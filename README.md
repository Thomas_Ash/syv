# SYV #
0.3.337

### Introduction

SYV is a rapid prototyping application for machine learning datasets. It is written in Python 3 and JavaScript, and it runs on Ubuntu 20.04. It allows you to define atomic transactions on n-dimensional arrays.

These transactions could be, among other things:

- an embedded system or pseudonymous user uploading a raw example
- a trusted user classifying or filtering an example
- a script downloading a dataset and training a model
- a script downloading examples, running a model, and uploading inferences

---

### Installation - Prerequisites

You will need an Ubuntu 20.04 server, with:

- at least 2GB of memory
- a non-root user with sudo privileges and the ability to log in via ssh
- an active firewall which allows ports 22 (ssh), 80 (http), and 443 (https)
- a DNS A record pointing at the public IP address (dynamic DNS is ok)

---

### Installation - Docker

Using an ssh client, login to your server as the non-root user.

Install Docker Compose.  
`sudo apt install docker-compose`

Clone this repository - the location isn't important, your home directory is fine.  
`git clone https://Thomas_Ash@bitbucket.org/Thomas_Ash/syv.git`

Go to the repository directory.  
`cd syv`

Run the script to install core plugins. If you want to use additional plugins, clone them into the `plugins` directory first.  
`python3 install.py`

Create an environment file from the example template.  
`cp .env.example .env`

Edit the environment file.  
`nano .env`

Change the values of `POSTGRES_PASSWORD` and `JWT_SECRET KEY` to two different strong passwords. You can use GUIDs, as in the example file, but make sure to generate new ones.  
Change the value of `SYV_API_HOST` to the domain pointing to your server.  
Change the value of `CERTBOT_EMAIL` to your email address. This address is only used to get a free TLS certificate from [Let's Encrypt](https://letsencrypt.org/). The SYV developers will not store or use your email address in any way.  
Save the file and exit the editor.

Run the setup script. This will build the containers, initialize the database, acquire a TLS certificate, and start the application. This will probably take a few minutes, depending on the specs of your server.  
`sudo ./setup.sh`

Display the contents of the password file created by the setup script, and copy the temporary password.  
`cat .syvpw`

Open a browser and navigate to your domain. You should see the SYV login screen.

Login using the username `super` and the temporary password you copied earlier.

Click `Change Password` in the navigation bar, and create a new password.

You are now ready to start using SYV!

To stop the application, go to the repository directory and run this command:  
`sudo docker-compose down`

And to restart it:  
`sudo docker-compose up -d`

---

### Terminology

A **User** contains credentials allowing a client to log in to SYV.

Users may be a member of one or more **Roles**.

A **Tank** is analagous to a traditional database table.

It contains a number of named **Array Specs**, which specify a data type and dimensional constraints. These can be compared to database columns.

An array spec will also have one of six predefined **Faces**, which determine how the array content is derived and presented.

Tanks may contain a number of **Drops** which are the equivalent of rows. A drop will contain a corresponding, conformant n-dimensional array for every array spec in the parent tank.

A **Pump** specifies a type of transaction which could read, write or delete drops in specified tanks, and the roles which have permission to execute it.

Pumps have one of eight predefined **Engines**, which determine the specific behaviour of the pump.

Engines define a number of **Pipes**, each of which connects a tank to the pump, either as an input or output.

Pipes can have any number of **Filters**, which each have a **Membrane**. These can be used to restrict the flow of drops through the pipe. On an input pipe, filtered drops are simply not selected, wheras on an output pipe they are discarded.

Authorized users can execute a pump to yield a **Cycle**, which represents a single transaction of drops.

A **Tome** is a simple list of strings, which may be referenced by faces and appended to by engines.

A **Record** is a file, which can either be uploaded at the start of a cycle, or downloaded after it completes.

---

### Faces

**Base** face implements no rendering or parsing, but has no constraints on what kind of array spec it is attached to.

**Binary** face requires a one- or two- dimensional array of booleans. A tome can optionally be attached to each dimension as a label. It supports input via checkboxes, and renders as a table of simple `True`/`False` strings.

**Text** face requires a one-dimensional array of unsigned 8- or 32- bit ints, which are interpreted as ASCII or UTF32 respectively. Input is via textarea and it renders as one or more paragraphs containing strings.

**Tome** face requires a one- or two- dimensional array of unsigned ints, which are interpreted as indexes in a tome. Further tomes can optionally be used as dimension labels. Input is via select and it renders as a table of strings.

**Image** face requires a three-dimensional array of ints or floats, which can be mapped to horizontal/vertical/channel in any order. It can render PNG or JPEG images, and does not support browser input.

**Audio** face requires a two-dimensional array of ints or floats, which can be mapped to sample position and channel in either order. It can render WAV, MP3 or OGG audio elements, and does not support browser input.

---

### Engines

**Base** engine defines no pipes and does nothing.

**Absorb** engine defines one input pipe and no output. It simply displays a number of drops.

**Create** engine defines one output pipe and no input. It generates new drops from the submitted arrays.

**Extend** engine defines one input pipe and one output. It displays the input drops, then combines their arrays with the submitted ones to generate new drops.

**Filter** engine defines one input pipe and two outputs. It displays the input drops and allows them to be directed into either output.

**Export** engine defines one input pipe and no output. It populates a zip archive with a file for every input drop, with the content and optionally the path inside the archive being determined by the array content.

**Import** is the opposite of export - it has one output pipe, no input and generates drops from the content/path of files in an upload zip archive.

**Scrape** is a web scraper. It generates drops from elements on a specified web page, which are located using CSS selectors. It can be configured to navigate to and scrape other linked pages, remember and skip previously scraped urls, and use a headless browser to scrape JavaScript-heavy pages.

---

### Membranes

**Base** membrane does nothing.

**Balance** membrane compares a specified array of the drops flowing through the pipe to an array of some tank. Drops are filtered if the total number of drops with the same array value exceed some specified value. It can be used to ensure uniqueness or balance a dataset with multiple classes.
