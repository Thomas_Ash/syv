from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from syv.config import Config

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
session_factory = sessionmaker(bind=engine, autoflush=False, autocommit=False)
BaseModel = declarative_base()


@contextmanager
def session_scope() -> Session:
    session = session_factory()
    try:
        yield session
    except (Exception,):
        session.rollback()
        raise
    finally:
        session.close()
