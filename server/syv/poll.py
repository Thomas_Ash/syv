import logging
from datetime import datetime

from uwsgi_tasks import timer_lazy

from syv.database import session_scope
from syv.services import CycleService, PumpService, UserService, ArrayService, RequestService
from syv.utils import get_timespan_minutes


def cleanup_cycles(session):
    cycle_service = CycleService(session)

    pre_commit_cycles = cycle_service.list_pre_commit()
    pre_commit_cycle_count = len(pre_commit_cycles)
    if pre_commit_cycle_count > 0:
        logging.info(f'poll: {pre_commit_cycle_count} cycles timed out pre-commit and will be cancelled.')

    for cycle in pre_commit_cycles:
        cycle_service.abort('Timed out pre-commit.', cycle)
        session.commit()

    expired_cycles = cycle_service.list_expired()

    expired_cycle_count = len(expired_cycles)
    if expired_cycle_count > 0:
        logging.info(f'poll: {expired_cycle_count} cycles have expired and will be deleted.')

        for cycle in expired_cycles:
            cycle_service.delete(cycle)

        session.commit()


def cleanup_arrays(session):
    array_service = ArrayService(session)

    orphaned_arrays = array_service.list_orphaned()

    orphaned_array_count = len(orphaned_arrays)
    if orphaned_array_count > 0:
        logging.info(f'poll: {orphaned_array_count} arrays are orphaned and will be deleted.')

        for array in orphaned_arrays:
            session.delete(array)

        session.commit()


def cleanup_requests(session):
    request_service = RequestService(session)

    expired_requests = request_service.list_expired()

    expired_request_count = len(expired_requests)
    if expired_request_count > 0:
        logging.info(f'poll: {expired_request_count} requests have expired and will be deleted.')

        for request in expired_requests:
            session.delete(request)

        session.commit()


def execute_scheduled_pumps(session):
    pump_service = PumpService(session)

    active_pumps = pump_service.list_active()
    epoch_minutes = int(datetime.now().timestamp() // 60)
    exec_pump_count = 0

    cycle_service = CycleService(session)
    user_service = UserService(session)

    for pump in active_pumps:
        if 'schedule_run' not in pump.engine_config or pump.engine_config.get('schedule_run') is not True:
            continue

        try:
            schedule_period = get_timespan_minutes(pump.engine_config.get('schedule_period'))
            schedule_offset = get_timespan_minutes(pump.engine_config.get('schedule_offset'))
            schedule_user_uid = int(pump.engine_config.get('schedule_user_uid'))
            schedule_user = user_service.get_by_uid(schedule_user_uid)
            assert schedule_user is not None

        except (Exception,) as e:
            logging.warning(f'Pump "{pump.name}" set for scheduled execution but config is invalid: {str(e)}')
            continue

        if epoch_minutes % schedule_period == schedule_offset:
            try:
                cycle = cycle_service.open(pump.uid, schedule_user.uid)
                session.commit()
                cycle_service.post_open(cycle)
                session.commit()
                cycle_service.commit(cycle)
                session.commit()
                cycle_service.post_commit(cycle)
                session.commit()

            except (Exception,) as e:
                logging.warning(f'Scheduled execution of pump "{pump.name}" failed: {str(e)}')

    if exec_pump_count > 0:
        logging.info(f'poll: {exec_pump_count} pumps executed on schedule.')


@timer_lazy(seconds=60)
def poll(sig):
    with session_scope() as session:
        cleanup_cycles(session)
        cleanup_arrays(session)
        cleanup_requests(session)
        execute_scheduled_pumps(session)
