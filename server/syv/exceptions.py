class EngineConfigError(Exception):

    def __init__(self):
        super().__init__('Pump operation failed due to invalid engine configuration.')


class EngineInternalError(Exception):

    def __init__(self):
        super().__init__('Engine processing failed.')


class FaceModeError(Exception):

    def __init__(self, expected, actual):
        super().__init__(f'Mode {actual} not supported, should be one of: {", ".join([x[0] for x in expected])}')


class FaceTypeError(Exception):

    def __init__(self, expected, actual):
        super().__init__(f'Expected {expected.__name__} but received {actual.__name__}')


class FaceDataError(Exception):

    def __init__(self, actual):
        super().__init__(f'Received {actual.__name__} but contents are invalid')


class FaceConfigError(Exception):

    def __init__(self):
        super().__init__('Array operation failed due to invalid face configuration.')


class FaceInternalError(Exception):

    def __init__(self):
        super().__init__('Face processing failed.')

