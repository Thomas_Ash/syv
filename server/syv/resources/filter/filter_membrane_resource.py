from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from syv.database import session_scope
from syv.schemas import filter_schema, pipe_schema
from syv.services import UserService


class FilterMembraneResource(Resource):
    def get(self):
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            from syv.membranes import get_membrane_types

            return {'membranes': [{
                'code': x.get_code()
            } for x in get_membrane_types()]}

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            try:
                filter_data = request.get_json().get('filter')
                _filter = filter_schema.load(filter_data, session=session, transient=True)
                pipe_data = request.get_json().get('pipe')
                pipe = pipe_schema.load(pipe_data, session=session, transient=True)
            except ValidationError:
                session.rollback()
                return {'config_template': {}}

            membrane_type = _filter.get_membrane_type()

            if membrane_type is None:
                config_template = {}
            else:
                membrane = membrane_type(_filter, pipe)
                config_template = membrane.get_config_template(session)

            session.rollback()
            return {'config_template': config_template}
