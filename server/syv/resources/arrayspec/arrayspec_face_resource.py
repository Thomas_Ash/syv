from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from syv.database import session_scope
from syv.schemas import arrayspec_schema
from syv.services import UserService


class ArraySpecFaceResource(Resource):
    def get(self):
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            from syv.faces import get_face_types

            return {'faces': [{
                'code': x.get_code()
            } for x in get_face_types()]}

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('arrayspec')
                arrayspec = arrayspec_schema.load(data, session=session, transient=True)
            except ValidationError:
                session.rollback()
                return {'config_template': {}}

            face_type = arrayspec.get_face_type()

            if face_type is None:
                config_template = {}
            else:
                face = face_type(arrayspec, session)
                config_template = face.get_config_template(session)

            session.rollback()
            return {'config_template': config_template}

