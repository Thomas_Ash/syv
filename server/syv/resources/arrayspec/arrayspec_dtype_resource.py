from typing import Dict

from flask import request
from flask_restful import Resource, abort

import numpy as np

from syv.database import session_scope
from syv.models import ArraySpec
from syv.services import UserService


class ArraySpecDtypeResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            dtype_list_data = []

            for code in ArraySpec.get_dtypes():
                dtype = np.dtype(code)
                dtype_data = {
                    'code': code,
                    'name': dtype.name
                }

                if code in ArraySpec.get_int_dtypes():
                    dtype_data.update({
                        'min': int(np.iinfo(dtype).min),
                        'max': int(np.iinfo(dtype).max)
                    })
                dtype_list_data.append(dtype_data)

            return {'dtypes': dtype_list_data}
