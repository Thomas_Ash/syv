from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.services import UserService, ArraySpecService


class ArraySpecBlankResource(Resource):
    def get(self, arrayspec_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            arrayspec_service = ArraySpecService(session)

            arrayspec = arrayspec_service.get_by_uid(arrayspec_uid)
            if arrayspec is None:
                abort(404, errors=['Not found.'])

            face_mode = request.args.get('face_mode')
            if face_mode not in ['web', 'raw']:
                abort(400, errors=['Invalid face mode.'])

            face_type = arrayspec.get_face_type()
            face = face_type(arrayspec, session)
            array = face.load_blank(session)

            return {
                'face_code': arrayspec.face_code,
                'data': face.dump(array, face_mode, session),
                'shape': list(array.shape)
            }
