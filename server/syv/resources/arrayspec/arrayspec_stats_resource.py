from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.services import UserService, DropService


class ArraySpecStatsResource(Resource):
    def get(self, arrayspec_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            drop_service = DropService(session)

            skip = request.args.get('skip') or 0
            take = request.args.get('take')

            stats, count = drop_service.page_count_by_arrayspec_and_hash(arrayspec_uid, skip, take)

            return {
                'stats': [{
                    'data_hash': x[0],
                    'drop_count': x[1]
                } for x in stats],
                'stat_count': count
            }
