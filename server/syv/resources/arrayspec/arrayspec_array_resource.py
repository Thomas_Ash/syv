from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.services import UserService, ArraySpecService, ArrayService


class ArraySpecArrayResource(Resource):
    def get(self, arrayspec_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            arrayspec_service = ArraySpecService(session)

            arrayspec = arrayspec_service.get_by_uid(arrayspec_uid)
            if arrayspec is None:
                abort(404, errors=['Not found.'])

            data_hash = request.args.get('data_hash')
            if data_hash is None:
                abort(400, errors=['Invalid or missing data hash.'])

            array_service = ArrayService(session)

            array = array_service.get_by_data_hash(data_hash)
            if array is None:
                abort(400, errors=['Invalid data hash.'])

            face_type = arrayspec.get_face_type()
            face = face_type(arrayspec, session)

            try:
                data = face.dump(array.data_array, 'web', session)
            except (Exception,):
                abort(400, errors=['Arrayspec face couldn\'t dump array.'])

            return {
                'array': {
                    'face_code': arrayspec.face_code,
                    'data': data,
                    'data_hash': data_hash,
                    'shape': list(array.data_array.shape)
                }
            }
