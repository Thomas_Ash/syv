import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import tome_schema
from syv.utils import any_errors
from syv.services import UserService, TomeService


class TomeResource(Resource):
    def get(self, tome_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            tome_service = TomeService(session)

            tome = tome_service.get_by_uid(tome_uid)
            if tome is None:
                abort(404, errors=['Not found.'])

            tome_data = tome_schema.dump(tome)
            tome_data = tome_schema.add_entries(tome, tome_data, session, False)
            tome_data = tome_schema.add_entry_count(tome, tome_data, session, False)
            tome_data = tome_schema.add_is_locked(tome, tome_data, session, False)

            return {'tome': tome_data}

    def put(self, tome_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            tome_service = TomeService(session)

            tome = tome_service.get_by_uid(tome_uid)
            if tome is None:
                abort(404, errors=['Not found.'])

            try:
                data = request.get_json().get('tome')
                entries = data.get('entries')
                data.pop('entries')
                tome_schema.load(data, session=session, instance=tome, partial=True, transient=True)

            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            if any_errors(tome_service.validate(tome)):
                abort(400, errors=['Tome validation failed.'])

            if tome_schema.tome_is_locked(tome, session):
                tome = tome_service.append_entries(tome_uid, entries)
            else:
                tome = tome_service.replace_entries(tome_uid, entries)

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            tome_data = tome_schema.dump(tome)
            tome_data = tome_schema.add_entries(tome, tome_data, session, False)
            tome_data = tome_schema.add_entry_count(tome, tome_data, session, False)
            tome_data = tome_schema.add_is_locked(tome, tome_data, session, False)

            return {'tome': tome_data}

    def delete(self, tome_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            tome_service = TomeService(session)

            tome = tome_service.get_by_uid(tome_uid)
            if tome is None:
                abort(404, errors=['Not found.'])

            if tome_schema.tome_is_locked(tome, session):
                abort(400, errors=['Tome is locked.'])

            try:
                session.delete(tome)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'tome_uid': tome.uid}
