import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import tome_schema, tome_list_schema
from syv.utils import any_errors
from syv.services import UserService, TomeService


class TomeListResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            skip = request.args.get('skip') or 0
            take = request.args.get('take')

            tome_service = TomeService(session)

            tome_list, count = tome_service.page(skip, take)

            tome_list_data = tome_list_schema.dump(tome_list)
            tome_list_data = tome_schema.add_entry_count(tome_list, tome_list_data, session, True)
            tome_list_data = tome_schema.add_is_locked(tome_list, tome_list_data, session, True)

            return {
                'tomes': tome_list_data,
                'tome_count': count
            }

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('tome')
                tome = tome_schema.load(data, session=session, transient=True)
            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            tome_service = TomeService(session)

            if any_errors(tome_service.validate(tome)):
                abort(400, errors=['Tome validation failed.'])

            try:
                session.add(tome)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            tome_data = tome_schema.dump(tome)
            tome_data = tome_schema.add_entries(tome, tome_data, session, False)
            tome_data = tome_schema.add_entry_count(tome, tome_data, session, False)
            tome_data = tome_schema.add_is_locked(tome, tome_data, session, False)

            return {'tome': tome_data}
