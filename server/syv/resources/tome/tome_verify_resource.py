from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from syv.database import session_scope
from syv.schemas import tome_schema
from syv.services import UserService, TomeService


class TomeVerifyResource(Resource):
    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('tome')
                tome = tome_schema.load(data, session=session, transient=True)
            except ValidationError:
                session.rollback()
                return {'verify_tome': {'report': {}}}

            tome_service = TomeService(session)

            report = tome_service.validate(tome)
            session.rollback()

            return {'verify_tome': {'report': report}}
