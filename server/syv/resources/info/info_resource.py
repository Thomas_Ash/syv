from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.config import Config
from syv.services import UserService


class InfoResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

        return {
            'info': {
                'byteorder': Config.BYTEORDER,
                'version': Config.VERSION
            }
        }