import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import tank_schema, tank_list_schema
from syv.utils import any_errors
from syv.services import UserService, TankService


class TankListResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            tank_service = TankService(session)

            skip = request.args.get('skip') or 0
            take = request.args.get('take')

            tank_list, count = tank_service.page(skip, take)

            tank_list_data = tank_list_schema.dump(tank_list)
            tank_list_data = tank_list_schema.add_drop_count(tank_list, tank_list_data, session, True)
            tank_list_data = tank_list_schema.add_is_locked(tank_list, tank_list_data, session, True)

            return {
                'tanks': tank_list_data,
                'tank_count': count
            }

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('tank')
                tank = tank_schema.load(data, session=session, transient=True)
            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            tank_service = TankService(session)

            if any_errors(tank_service.validate(tank)):
                abort(400, errors=['Tank validation failed.'])

            try:
                session.add(tank)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            tank_data = tank_schema.dump(tank)
            tank_data = tank_schema.add_drop_count(tank, tank_data, session, False)
            tank_data = tank_schema.add_is_locked(tank, tank_data, session, False)

            return {'tank': tank_data}
