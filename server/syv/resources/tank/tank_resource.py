import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import tank_schema
from syv.utils import any_errors
from syv.services import UserService, TankService


class TankResource(Resource):
    def get(self, tank_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            tank_service = TankService(session)

            tank = tank_service.get_by_uid(tank_uid)
            if tank is None:
                abort(404, errors=['Not found.'])

            tank_data = tank_schema.dump(tank)
            tank_data = tank_schema.add_drop_count(tank, tank_data, session, False)
            tank_data = tank_schema.add_is_locked(tank, tank_data, session, False)

            return {'tank': tank_data}

    def put(self, tank_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            tank_service = TankService(session)

            tank = tank_service.get_by_uid(tank_uid)
            if tank is None:
                abort(404, errors=['Not found.'])

            if tank_schema.tank_is_locked(tank, session):
                abort(400, errors=['Tank is locked.'])

            try:
                data = request.get_json().get('tank')
                tank_schema.load(data, session=session, instance=tank, partial=True, transient=True)
            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            if any_errors(tank_service.validate(tank)):
                abort(400, errors=['Tank validation failed.'])

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            tank_data = tank_schema.dump(tank)
            tank_data = tank_schema.add_drop_count(tank, tank_data, session, False)
            tank_data = tank_schema.add_is_locked(tank, tank_data, session, False)

            return {'tank': tank_data}

    def delete(self, tank_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            tank_service = TankService(session)

            tank = tank_service.get_by_uid(tank_uid)
            if tank is None:
                abort(404, errors=['Not found.'])

            if tank_schema.tank_is_locked(tank, session):
                abort(400, errors=['Tank is locked.'])

            try:
                session.delete(tank)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'tank_uid': tank.uid}
