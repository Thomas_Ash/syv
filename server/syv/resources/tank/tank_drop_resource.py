import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.services import UserService, TankService, DropService

# this is used by admin user browsing drops in combination with ArrayInstResource
# doesn't return array data, so unconcerned with faces
# accepts paging arguments and returns total count


class TankDropResource(Resource):
    def get(self, tank_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            tank_service = TankService(session)

            tank = tank_service.get_by_uid(tank_uid)
            if tank is None:
                abort(404, errors=['Not found.'])

            drop_service = DropService(session)

            skip = request.args.get('skip') or 0
            take = request.args.get('take')

            drop_list, count = drop_service.page_by_tank(tank_uid, skip, take)

            drop_list_data = []

            for drop in drop_list:
                try:
                    drop_data = {
                        'uid': drop.uid,
                        'arrays': {}
                    }
                    for arrayspec in tank.arrayspecs:
                        arrayinst = drop.get_arrayinst(arrayspec_uid=arrayspec.uid)
                        drop_data['arrays'][arrayspec.name] = {
                            'arrayinst_uid': arrayinst.uid,
                            'data_hash': arrayinst.array_data_hash
                        }
                    drop_list_data.append(drop_data)
                except AttributeError:
                    logging.warning(f'Drop {drop.uid} is missing at least one arrayinst')


            return {
                'drops': drop_list_data,
                'drop_count': count
            }

    def delete(self, tank_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            drop_service = DropService(session)
            drop_service.delete_by_tank(tank_uid)

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {
                'drops': [],
                'drop_count': 0
            }
