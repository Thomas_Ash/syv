from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from syv.database import session_scope
from syv.schemas import tank_schema
from syv.services import UserService, TankService


class TankVerifyResource(Resource):
    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('tank')
                tank = tank_schema.load(data, session=session, transient=True)
            except ValidationError:
                session.rollback()
                return {'verify_tank': {'report': {}}}

            tank_service = TankService(session)

            report = tank_service.validate(tank)
            session.rollback()

            return {'verify_tank': {'report': report}}
