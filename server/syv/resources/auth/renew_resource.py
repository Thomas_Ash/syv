import random
import time
from datetime import datetime, timedelta
from typing import Dict

from flask import request, current_app
from flask_restful import Resource, abort
import jwt

from syv.database import session_scope
from syv.schemas import user_identity_schema
from syv.services import UserService


class RenewResource(Resource):
    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)

            time.sleep(random.SystemRandom().uniform(0.05, 0.25))

            if client_user is None:
                abort(401, errors=['Forbidden.'])

            token = jwt.encode(
                {
                    'sub': client_user.name,
                    'iat': datetime.utcnow(),
                    'exp': datetime.utcnow() + timedelta(minutes=60)
                },
                current_app.config['SECRET_KEY'],
                algorithm='HS256'
            )

            user_data = user_identity_schema.dump(client_user)

            return {
                'token': token,
                'user': user_data
            }
