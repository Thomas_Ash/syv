from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from syv.database import session_scope
from syv.schemas import pump_schema
from syv.services import UserService, PumpService


class PumpEngineResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            from syv.engines import get_engine_types

            return {'engines': [{
                'code': x.get_code(),
                'pipes': {
                    'in': x.get_pipes()[0],
                    'out': x.get_pipes()[1]
                }
            } for x in get_engine_types()]}

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            try:
                data = request.get_json().get('pump')
                pump = pump_schema.load(data, session=session, transient=True)
                pump_service.load_roles(pump, data)

            except ValidationError:
                session.rollback()
                return {'config_template': {}}

            engine_type = pump.get_engine_type()

            if engine_type is None:
                config_template = {}
            else:
                engine = engine_type(pump)
                config_template = engine.get_config_template(session)

            session.rollback()
            return {
                'config_template': config_template,
                'debug': {'pump': pump_schema.dump(pump)}
            }
