from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.icons import get_icons
from syv.services import UserService


class PumpIconResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            return {'icons': get_icons()}
