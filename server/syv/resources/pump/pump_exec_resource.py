from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.schemas import pump_list_schema
from syv.services import PumpService, UserService


class PumpExecResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            pump_service = PumpService(session)

            pumps = pump_service.list_executable(client_user)

            return {'pumps': pump_list_schema.dump(pumps)}
