from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from syv.database import session_scope
from syv.schemas import pump_schema
from syv.services import PumpService, UserService


class PumpVerifyResource(Resource):
    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            try:
                data = request.get_json().get('pump')
                pump = pump_schema.load(data, session=session, transient=True)
                pump_service.load_roles(pump, data)

            except ValidationError:
                session.rollback()
                return {'verify_pump': {'report': {}}}

            report = pump_service.validate(pump)
            session.rollback()

            return {'verify_pump': {'report': report}}

