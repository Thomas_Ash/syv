import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.services import PumpService, UserService


class PumpClearResource(Resource):
    def put(self, pump_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            pump = pump_service.get_by_uid(pump_uid)
            if pump is None:
                abort(404, errors=['Not found.'])

            if not pump.is_disabled:
                abort(400, errors=['Pump is locked.'])

            pump.state = {}

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'success': True}
