import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.utils import any_errors
from syv.services import CycleService, PumpService, UserService, TankService

# pump is locked for normal editing via PumpResource.put if it's active
# this resource allows it to be deactivated


class PumpStatusResource(Resource):
    def put(self, pump_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            pump = pump_service.get_by_uid(pump_uid)
            if pump is None:
                abort(404, errors=['Not found.'])

            is_disabled = request.get_json().get('is_disabled')

            if is_disabled not in [True, False]:
                abort(400, errors=['Schema validation failed.'])

            cycle_service = CycleService(session)

            if is_disabled:
                cycles = cycle_service.list_processing_by_pump_uid(pump.uid)

                for cycle in cycles:
                    cycle_service.abort(f'Pump "{pump.name}" was deactivated.', cycle)
                    session.commit()

            else:

                tank_service = TankService(session)

                if any_errors(pump_service.validate(pump)):
                    abort(400, errors=['Pump validation failed.'])

                for pipe in pump.pipes:
                    if any_errors(tank_service.validate(pipe.tank)):
                        abort(400, errors=['Tank validation failed.'])

            pump.is_disabled = is_disabled

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'is_disabled': pump.is_disabled}
