import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import pump_schema, pump_list_schema
from syv.utils import any_errors
from syv.services import PumpService, UserService, ArrayService


class PumpListResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            skip = request.args.get('skip') or 0
            take = request.args.get('take')

            pump_list, count = pump_service.page(skip, take)

            pump_list_data = pump_list_schema.dump(pump_list)
            pump_list_data = pump_schema.add_details(pump_list, pump_list_data, session, True)

            return {
                'pumps': pump_list_data,
                'pump_count': count
            }

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            try:
                data = request.get_json().get('pump')
                pump = pump_schema.load(data, session=session, transient=True)
                pump.state = {}
                pump_service.load_roles(pump, data)

            except ValidationError as e:
                logging.warning(str(e))
                abort(400, errors=['Schema validation failed.'])

            if any_errors(pump_service.validate(pump)):
                abort(400, errors=['Pump validation failed.'])

            array_service = ArrayService(session)

            try:
                session.add(pump)
                session.commit()

                array_service.save_constant_arrays(pump)
                session.commit()

            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            pump_data = pump_schema.dump(pump)
            pump_data = pump_schema.add_details(pump, pump_data, session, False)

            return {'pump': pump_data}
