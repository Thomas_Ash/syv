import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import pump_schema
from syv.utils import any_errors
from syv.services import CycleService, PumpService, UserService, ArrayService


class PumpResource(Resource):
    def get(self, pump_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            pump = pump_service.get_by_uid(pump_uid)
            if pump is None:
                abort(404, errors=['Not found.'])

            pump_data = pump_schema.dump(pump)
            pump_data = pump_schema.add_details(pump, pump_data, session, False)

            return {'pump': pump_data}

    def put(self, pump_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            pump = pump_service.get_by_uid(pump_uid)
            if pump is None:
                abort(404, errors=['Not found.'])

            if not pump.is_disabled:
                abort(400, errors=['Pump is locked.'])

            try:
                data = request.get_json().get('pump')
                pump_schema.load(data, session=session, instance=pump, partial=True, transient=True)
                pump_service.load_roles(pump, data)

            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            if any_errors(pump_service.validate(pump)):
                abort(400, errors=['Pump validation failed.'])

            cycle_service = CycleService(session)
            if pump.is_disabled:
                cycles = cycle_service.list_processing_by_pump_uid(pump.uid)

                for cycle in cycles:
                    cycle_service.abort(f'Pump "{pump.name}" was deactivated.', cycle)
                    session.commit()

            array_service = ArrayService(session)

            array_service.save_constant_arrays(pump)
            array_service.unlink_pump(pump.uid, pump.get_array_hashes())

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            pump_data = pump_schema.dump(pump)
            pump_data = pump_schema.add_details(pump, pump_data, session, False)

            return {'pump': pump_data}

    def delete(self, pump_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            pump_service = PumpService(session)

            pump = pump_service.get_by_uid(pump_uid)
            if pump is None:
                abort(404, errors=['Not found.'])

            if not pump.is_disabled:
                abort(400, errors=['Pump is locked.'])

            array_service = ArrayService(session)

            array_service.unlink_pump(pump.uid)

            try:
                session.delete(pump)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'pump_uid': pump.uid}
