from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.services import UserService, ArrayService, ArraySpecService
from syv.exceptions import FaceDataError


class PumpArrayResource(Resource):
    def put(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            json = request.get_json()

            constant_array_data = json.get('constant_array')
            if constant_array_data is None:
                abort(400, errors=['Constant array is missing.'])

            arrayspec_service = ArraySpecService(session)

            try:
                arrayspec_uid = int(constant_array_data.get('arrayspec_uid'))
                arrayspec = arrayspec_service.get_by_uid(arrayspec_uid)
            except (KeyError, ValueError, TypeError):
                abort(400, errors=['Arrayspec uid is missing or invalid.'])

            if arrayspec is None:
                abort(404, errors=['Arrayspec not found.'])

            face = arrayspec.get_face_type()(arrayspec, session)

            if 'data' in constant_array_data:
                try:
                    data_array = face.load(constant_array_data.get('data'), 'web', session)
                except FaceDataError:
                    abort(400, errors=['Invalid array data.'])
            else:
                data_array = face.load_blank(session)

            array_service = ArrayService(session)

            data_hash = array_service.get_data_hash(data_array)

            return {
                'constant_array': {
                    'data': face.dump(data_array, 'web', session),
                    'data_hash': data_hash,
                    'arrayspec_uid': arrayspec.uid,
                    'face_code': arrayspec.face_code
                }
            }
