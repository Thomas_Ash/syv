import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import role_schema, role_list_schema
from syv.utils import any_errors
from syv.services import UserService, RoleService


class RoleListResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not (
                client_user.is_authorized(require_super=True) or
                client_user.is_authorized(require_admin=True)
            ):
                abort(403, errors=['Access denied.'])

            role_service = RoleService(session)

            skip = request.args.get('skip') or 0
            take = request.args.get('take')

            role_list, count = role_service.page(skip, take)

            return {
                'roles': role_list_schema.dump(role_list),
                'role_count': count
            }

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('role')
                role = role_schema.load(data, session=session, transient=True)
            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            role_service = RoleService(session)

            if any_errors(role_service.validate(role)):
                abort(400, errors=['Role validation failed.'])

            try:
                session.add(role)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'role': role_schema.dump(role)}
