import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import role_schema
from syv.utils import any_errors
from syv.services import UserService, RoleService


class RoleResource(Resource):
    def get(self, role_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            role_service = RoleService(session)

            role = role_service.get_by_uid(role_uid)
            if role is None:
                abort(404, errors=['Not found.'])

            return {'role': role_schema.dump(role)}

    def put(self, role_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            role_service = RoleService(session)

            role = role_service.get_by_uid(role_uid)
            if role is None:
                abort(404, errors=['Not found.'])

            try:
                data = request.get_json().get('role')
                role_schema.load(data, session=session, instance=role, partial=True, transient=True)
            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            if any_errors(role_service.validate(role)):
                abort(400, errors=['Role validation failed.'])

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'role': role_schema.dump(role)}

    def delete(self, role_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            role_service = RoleService(session)

            role = role_service.get_by_uid(role_uid)
            if role is None:
                abort(404, errors=['Not found.'])

            try:
                session.delete(role)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'role_uid': role.uid}
