from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from syv.database import session_scope
from syv.schemas import role_schema
from syv.services import UserService, RoleService


class RoleVerifyResource(Resource):
    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('role')
                role = role_schema.load(data, session=session, transient=True)
            except ValidationError:
                session.rollback()
                return {'verify_role': {'report': {}}}

            role_service = RoleService(session)

            report = role_service.validate(role)
            session.rollback()

            return {'verify_role': {'report': report}}
