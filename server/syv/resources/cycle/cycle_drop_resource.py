from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.services import CycleService, UserService, RequestService, DropService

# requires face mode and comma separated list of drop_uids as arguments
# returns actual array data


class CycleDropResource(Resource):
    def get(self, cycle_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            request_service = RequestService(session)

            if not request_service.make_request(client_user):
                abort(429, errors=['Request limit exceeded.'])

            session.commit()
            cycle_service = CycleService(session)
            cycle = cycle_service.get_by_uid(cycle_uid)

            if cycle is None:
                abort(404, errors=['Not found.'])

            if cycle.user_uid != client_user.uid and not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            if cycle.is_committed or cycle.error is not None:
                abort(400, errors=['Cycle is committed or failed.'])

            face_mode = request.args.get('face_mode')
            if face_mode not in ['web', 'raw']:
                abort(400, errors=['Invalid face mode.'])

            drop_uids = request.args.get('drop_uids') or ''
            try:
                drop_uid_list = [int(x) for x in drop_uids.split(',')]
            except ValueError:
                abort(400, errors=['Invalid drop uids.'])

            drop_service = DropService(session)

            drop_list = drop_service.list_by_cycle_filtered(cycle.uid, drop_uid_list)

            if not set(drop_uid_list) == set([x.uid for x in drop_list]):
                abort(400, errors=['Drop uids do not match cycle.'])

            drop_list_data = []
            faces = {}

            for drop in drop_list:
                if drop.tank.name not in faces:
                    faces[drop.tank.name] = {}

                drop_data = {
                    'uid': drop.uid,
                    'arrays': {}
                }

                for arrayinst in drop.arrayinsts:
                    if arrayinst.arrayspec.name not in faces[drop.tank.name]:
                        face_type = arrayinst.arrayspec.get_face_type()
                        faces[drop.tank.name][arrayinst.arrayspec.name] = face_type(arrayinst.arrayspec, session)

                    drop_data['arrays'][arrayinst.arrayspec.name] = {
                        'face_code': arrayinst.arrayspec.face_code,
                        'data_hash': arrayinst.array_data_hash,
                        'data': faces[drop.tank.name][arrayinst.arrayspec.name].dump(arrayinst.array.data_array,
                                                                                     face_mode, session),
                        'shape': list(arrayinst.array.data_array.shape)
                    }

                drop_list_data.append(drop_data)

            return {'drops': drop_list_data}
