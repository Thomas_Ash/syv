import copy
import io
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from werkzeug.utils import secure_filename

from syv.database import session_scope
from syv.schemas import cycle_schema
from syv.exceptions import FaceDataError
from syv.services import CycleService, UserService, ArrayService, RecordService, DropService, RequestService


class CycleResource(Resource):
    def get(self, cycle_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            request_service = RequestService(session)

            if not request_service.make_request(client_user):
                abort(429, errors=['Request limit exceeded.'])

            session.commit()
            cycle_service = CycleService(session)
            cycle = cycle_service.get_by_uid(cycle_uid)

            if cycle is None:
                abort(404, errors=['Not found.'])

            if cycle.user_uid != client_user.uid and not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            cycle_data = cycle_schema.dump(cycle)
            cycle_data = cycle_schema.add_details(cycle, cycle_data, session, False)

            return {'cycle': cycle_data}

    def put(self, cycle_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            request_service = RequestService(session)

            if not request_service.make_request(client_user):
                abort(429, errors=['Request limit exceeded.'])

            session.commit()
            cycle_service = CycleService(session)
            cycle = cycle_service.get_by_uid(cycle_uid)

            if cycle is None:
                abort(404, errors=['Not found.'])

            if cycle.user_uid != client_user.uid and not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            if cycle.is_committed or cycle.error is not None:
                abort(400, errors=['Cycle is committed or failed.'])

            if request.content_type.startswith('application/json'):

                drops = request.get_json().get('drops')
                if drops is not None:

                    array_service = ArrayService(session)

                    face_mode = request.args.get('face_mode')
                    if face_mode not in ['web', 'raw']:
                        abort(400, errors=['Invalid face mode.'])

                    client_drops = copy.deepcopy(cycle.client_drops)

                    drop_service = DropService(session)

                    for pipe_name in drops:
                        pipe = cycle.pump.get_output_pipe(pipe_name)
                        if pipe is None:
                            abort(400, errors=['Invalid pipe name.'])
                        for drop in drops[pipe_name]:
                            client_drop = {}
                            mapped_uid = drop.get('mapped_uid')
                            if mapped_uid is not None:
                                try:
                                    client_drop['mapped_uid'] = int(mapped_uid)
                                    mapped_drop = drop_service.get_by_uid(mapped_uid)
                                    assert mapped_drop.cycle_uid == cycle_uid
                                except ValueError:
                                    abort(400, errors=['Invalid drop uid.'])

                            # save arrays
                            arrays = drop.get('arrays')
                            if arrays is not None:
                                client_drop['arrays'] = {}
                                for array_name in arrays:
                                    arrayspec = pipe.tank.get_arrayspec(name=array_name)
                                    if arrayspec is None:
                                        abort(400, errors=['Invalid array name.'])

                                    face_type = arrayspec.get_face_type()
                                    face = face_type(arrayspec, session)
                                    try:
                                        array = face.load(
                                            arrays[array_name].get('data'),
                                            face_mode,
                                            session
                                        )
                                    except (FaceDataError,):
                                        abort(400, errors=['Invalid array data.'])

                                    if not arrayspec.shape_fits(array.shape) or arrayspec.dtype != array.dtype.char:
                                        abort(400, errors=['Invalid array data.'])

                                    data_hash = array_service.save(array, cycle_uid=cycle_uid)
                                    client_drop['arrays'][arrayspec.name] = data_hash

                            client_drops[pipe_name].append(client_drop)
                            cycle.client_drops = client_drops
                            session.commit()

                config = request.get_json().get('config')
                if config is not None:

                    client_config = copy.deepcopy(cycle.client_config)
                    engine = cycle.pump.get_engine_type()(cycle.pump)

                    if type(config) is not dict or any([
                        x for x in config.keys()
                        if x not in engine.get_client_config_template().keys()
                    ]):
                        abort(400, errors=['Invalid config data.'])

                    client_config.update(config)
                    cycle.client_config = client_config
                    session.commit()

                return {'success': True}

            elif request.content_type.startswith('multipart/form-data'):

                record_service = RecordService(session)
                input_record_names = cycle.pump.get_engine_type().get_records()[0]

                for field_name in request.files.keys():
                    if field_name not in input_record_names:
                        abort(400, errors=['Invalid record name.'])

                    try:
                        offset = int(request.form.get('offset'))

                    except (TypeError, ValueError):
                        abort(400, errors=['Offset is missing or invalid.'])
                    if offset == 0:
                        dupe = cycle.get_record(field_name)
                        if dupe is not None:
                            abort(400, errors=['Offset is zero but record already exists.'])

                        chunk = request.files[field_name]
                        stream = io.BytesIO()
                        chunk.save(stream)

                        record_service.save(stream,
                                            secure_filename(chunk.filename),
                                            field_name,
                                            cycle)

                    else:
                        record = cycle.get_record(field_name)
                        if record is None:
                            abort(400, errors=['Offset is nonzero but record does not exist.'])

                        chunk = request.files[field_name]
                        stream = io.BytesIO()
                        chunk.save(stream)

                        record_service.append(stream,
                                              record,
                                              offset)

                    session.commit()

                return {'success': True}

    def delete(self, cycle_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            request_service = RequestService(session)

            if not request_service.make_request(client_user):
                abort(429, errors=['Request limit exceeded.'])

            session.commit()
            cycle_service = CycleService(session)
            cycle = cycle_service.get_by_uid(cycle_uid)

            if cycle is None:
                abort(404, errors=['Not found.'])

            if cycle.user_uid != client_user.uid and not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            if cycle.error is None and cycle.progress < 100:
                abort(400, errors=['Cycle has not completed.'])

            cycle_service.delete(cycle)

            session.commit()

            return {'cycle_uid': cycle.uid}
