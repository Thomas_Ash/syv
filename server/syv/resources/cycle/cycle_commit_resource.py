from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.schemas import cycle_schema
from syv.services import CycleService, UserService, RequestService


class CycleCommitResource(Resource):
    def put(self, cycle_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            request_service = RequestService(session)

            if not request_service.make_request(client_user):
                abort(429, errors=['Request limit exceeded.'])

            session.commit()
            cycle_service = CycleService(session)
            cycle = cycle_service.get_by_uid(cycle_uid)

            if cycle is None:
                abort(404, errors=['Not found.'])

            if cycle.user_uid != client_user.uid and not client_user.is_authorized(require_admin=True):
                abort(403, errors=['Access denied.'])

            if not cycle.can_commit():
                abort(400, errors=['Missing prerequisites for commit.'])

            cycle_service.commit(cycle)
            session.commit()
            cycle_service.post_commit(cycle)
            session.commit()

            cycle_data = cycle_schema.dump(cycle)
            cycle_data = cycle_schema.add_details(cycle, cycle_data, session, False)

            return {'cycle': cycle_data}
