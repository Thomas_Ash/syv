from typing import Dict

from flask import request
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.schemas import cycle_schema, cycle_list_schema
from syv.services import CycleService, PumpService, UserService, RequestService


class CycleListResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            try:
                user_uid = int(request.args.get('user_uid'))
            except TypeError:
                user_uid = None

            try:
                pump_uid = int(request.args.get('pump_uid'))
            except TypeError:
                pump_uid = None

            if not client_user.is_authorized(require_admin=True):
                if user_uid is None:
                    abort(403, errors=['Access denied.'])
                elif not client_user.is_authorized(require_user_uids=[user_uid]):
                    abort(403, errors=['Access denied.'])

            skip = request.args.get('skip') or 0
            take = request.args.get('take')

            cycle_service = CycleService(session)

            cycle_list, count = cycle_service.page(user_uid, pump_uid, skip, take)

            return {
                'cycles': cycle_list_schema.dump(cycle_list),
                'cycle_count': count
            }

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            request_service = RequestService(session)

            if not request_service.make_request(client_user):
                abort(429, errors=['Request limit exceeded.'])

            session.commit()
            pump_service = PumpService(session)

            try:
                data = request.get_json()
                cycle_json = data.get('cycle')
                if 'pump_uid' in cycle_json:
                    pump = pump_service.get_by_uid(int(cycle_json.get('pump_uid')))
                elif 'pump_name' in cycle_json:
                    pump = pump_service.get_by_name(cycle_json.get('pump_name'))
                else:
                    abort(400, errors=['No pump identifier specified.'])
                assert pump is not None
            except (KeyError, ValueError, TypeError, AssertionError):
                abort(404, errors=['Not found.'])

            role_uids = [x.uid for x in client_user.roles if not x.is_disabled]

            if not any(x for x in pump.roles if x.uid in role_uids):
                abort(403, errors=['Access denied.'])

            cycle_service = CycleService(session)

            cycle = cycle_service.open(pump.uid, client_user.uid)
            session.commit()
            cycle_service.post_open(cycle)
            session.commit()

            cycle_data = cycle_schema.dump(cycle)
            cycle_data = cycle_schema.add_details(cycle, cycle_data, session, False)

            return {'cycle': cycle_data}
