from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from syv.database import session_scope
from syv.schemas import user_schema
from syv.services import UserService


class UserVerifyResource(Resource):
    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('user')
                user = user_schema.load(data, session=session, transient=True)
                user_service.load_roles(user, data)

            except ValidationError:
                session.rollback()
                return {'verify_user': {'report': {}}}

            report = user_service.validate(user)
            session.rollback()

            return {'verify_user': {'report': report}}
