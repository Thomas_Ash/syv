import logging
from typing import Dict

from werkzeug.security import generate_password_hash
from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.models import User
from syv.schemas import user_schema, user_list_schema
from syv.utils import any_errors
from syv.services import UserService


class UserListResource(Resource):
    def get(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not (
                client_user.is_authorized(require_super=True) or
                client_user.is_authorized(require_admin=True)
            ):
                abort(403, errors=['Access denied.'])

            skip = request.args.get('skip') or 0
            take = request.args.get('take')

            user_list, count = user_service.page(skip, take)

            return {
                'users': user_list_schema.dump(user_list),
                'user_count': count
            }

    def post(self) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            try:
                data = request.get_json().get('user')

                password = User.random_password()
                data['password'] = generate_password_hash(password)

                user = user_schema.load(data, session=session, transient=True)
                user_service.load_roles(user, data)

            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            if any_errors(user_service.validate(user)):
                abort(400, errors=['User validation failed.'])

            try:
                session.add(user)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'user': user_schema.dump(user)}
