import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from werkzeug.security import check_password_hash

from syv.database import session_scope
from syv.schemas import user_change_password_schema
from syv.services import UserService


class UserChangePasswordResource(Resource):
    def get(self, user_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if (
                not client_user.is_authorized(require_super=True) and
                not client_user.is_authorized(require_user_uids=[user_uid])
            ):
                abort(403, errors=['Access denied.'])

            user = user_service.get_by_uid(user_uid)
            if user is None:
                abort(404, errors=['Not found.'])

            return {'user': user_change_password_schema.dump(user)}

    def put(self, user_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if (
                not client_user.is_authorized(require_super=True) and
                not client_user.is_authorized(require_user_uids=[user_uid])
            ):
                abort(403, errors=['Access denied.'])

            user = user_service.get_by_uid(user_uid)
            if user is None:
                abort(404, errors=['Not found.'])

            data = request.get_json().get('user')

            # check old password if not super user

            if not client_user.is_authorized(require_super=True):
                try:
                    old_password = data.get('old_password')
                    assert check_password_hash(user.password, old_password)
                except (AttributeError, AssertionError):
                    abort(401, errors=['Authentication failed.'])

            try:
                user_change_password_schema.load(data, session=session, instance=user, partial=True, transient=True)
            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'user': user_change_password_schema.dump(user)}
