import logging
from typing import Dict

from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from syv.database import session_scope
from syv.schemas import user_schema
from syv.utils import any_errors
from syv.services import UserService


class UserResource(Resource):
    def get(self, user_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            user = user_service.get_by_uid(user_uid)
            if user is None:
                abort(404, errors=['Not found.'])

            return {'user': user_schema.dump(user)}

    def put(self, user_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            user = user_service.get_by_uid(user_uid)
            if user is None:
                abort(404, errors=['Not found.'])

            try:
                data = request.get_json().get('user')
                user_schema.load(data, session=session, instance=user, partial=True, transient=True)
                user_service.load_roles(user, data)
            except ValidationError:
                abort(400, errors=['Schema validation failed.'])

            if any_errors(user_service.validate(user)):
                abort(400, errors=['User validation failed.'])

            try:
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'user': user_schema.dump(user)}

    def delete(self, user_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            if not client_user.is_authorized(require_super=True):
                abort(403, errors=['Access denied.'])

            user = user_service.get_by_uid(user_uid)
            if user is None:
                abort(404, errors=['Not found.'])

            try:
                session.delete(user)
                session.commit()
            except IntegrityError as e:
                logging.warning(str(e))
                session.rollback()
                abort(400, errors=['Database transaction failed.'])

            return {'user_uid': user.uid}
