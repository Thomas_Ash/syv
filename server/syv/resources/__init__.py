from syv.resources.arrayspec.arrayspec_dtype_resource import ArraySpecDtypeResource
from syv.resources.arrayspec.arrayspec_face_resource import ArraySpecFaceResource
from syv.resources.arrayspec.arrayspec_blank_resource import ArraySpecBlankResource
from syv.resources.arrayspec.arrayspec_stats_resource import ArraySpecStatsResource
from syv.resources.arrayspec.arrayspec_array_resource import ArraySpecArrayResource

from syv.resources.auth.login_resource import LoginResource
from syv.resources.auth.renew_resource import RenewResource

from syv.resources.cycle.cycle_resource import CycleResource
from syv.resources.cycle.cycle_list_resource import CycleListResource
from syv.resources.cycle.cycle_commit_resource import CycleCommitResource
from syv.resources.cycle.cycle_cancel_resource import CycleCancelResource
from syv.resources.cycle.cycle_drop_resource import CycleDropResource
from syv.resources.cycle.cycle_freeze_resource import CycleFreezeResource

from syv.resources.filter.filter_membrane_resource import FilterMembraneResource

from syv.resources.info.info_resource import InfoResource

from syv.resources.pump.pump_resource import PumpResource
from syv.resources.pump.pump_list_resource import PumpListResource
from syv.resources.pump.pump_verify_resource import PumpVerifyResource
from syv.resources.pump.pump_icon_resource import PumpIconResource
from syv.resources.pump.pump_exec_resource import PumpExecResource
from syv.resources.pump.pump_engine_resource import PumpEngineResource
from syv.resources.pump.pump_status_resource import PumpStatusResource
from syv.resources.pump.pump_clear_resource import PumpClearResource
from syv.resources.pump.pump_array_resource import PumpArrayResource

from syv.resources.record.record_resource import RecordResource

from syv.resources.role.role_resource import RoleResource
from syv.resources.role.role_list_resource import RoleListResource
from syv.resources.role.role_verify_resource import RoleVerifyResource

from syv.resources.tank.tank_resource import TankResource
from syv.resources.tank.tank_list_resource import TankListResource
from syv.resources.tank.tank_verify_resource import TankVerifyResource
from syv.resources.tank.tank_drop_resource import TankDropResource

from syv.resources.tome.tome_resource import TomeResource
from syv.resources.tome.tome_list_resource import TomeListResource
from syv.resources.tome.tome_verify_resource import TomeVerifyResource

from syv.resources.user.user_resource import UserResource
from syv.resources.user.user_list_resource import UserListResource
from syv.resources.user.user_verify_resource import UserVerifyResource
from syv.resources.user.user_change_password_resource import UserChangePasswordResource
