from typing import Dict

from flask import request, make_response
from flask_restful import Resource, abort

from syv.database import session_scope
from syv.services import UserService, RequestService, RecordService


class RecordResource(Resource):
    def get(self, record_uid: int) -> Dict:
        with session_scope() as session:
            user_service = UserService(session)

            client_user = user_service.authenticated_user(request)
            if client_user is None:
                abort(401, errors=['Forbidden.'])

            request_service = RequestService(session)

            if not request_service.make_request(client_user):
                abort(429, errors=['Request limit exceeded.'])

            record_service = RecordService(session)

            record = record_service.get_by_uid(record_uid)

            if record is None:
                abort(404, errors=['Not found.'])

            if record.cycle.user_uid != client_user.uid:
                abort(403, errors=['Access denied.'])

            if record.is_input:
                abort(403, errors=['Access denied.'])

            try:
                offset = int(request.args.get('offset'))
            except (TypeError, ValueError):
                abort(400, errors=['Offset is missing or invalid.'])

            try:
                length = int(request.args.get('length'))
            except (TypeError, ValueError):
                abort(400, errors=['Length is missing or invalid.'])

            if offset + length > record.byte_count:
                abort(400, errors=['Offset plus length is greater than record size.'])

            with open(record.get_path(), 'rb') as stream:
                stream.seek(offset)
                chunk = stream.read(length)
                response = make_response(chunk)
                response.headers.set('Content-Type', 'application/octet-stream')

                return response
