from sqlalchemy.orm import Session


class BaseService:
    session = None

    def __init__(self, session: Session) -> None:
        self.session = session
