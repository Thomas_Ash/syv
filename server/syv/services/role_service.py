from typing import Union, Tuple, List, Dict

from syv.models import Role
from .base_service import BaseService


class RoleService(BaseService):

    def get_by_uid(self, role_uid: int) -> Union[Role, None]:
        return self.session.query(Role).filter(Role.uid == role_uid).first()

    def page(self, skip: int = 0, take: Union[int, None] = None) -> Tuple[List[Role], int]:
        q = self.session.query(Role)
        count = q.count()

        q = q.order_by(Role.uid.desc()).offset(skip)

        if take is not None:
            q = q.limit(take)

        return q.all(), count

    def validate(self, role: Role) -> Dict:
        report = {
            'name': {'errors': []}
        }

        if len(role.name) <= 0:
            report['name']['errors'].append('Name cannot be blank.')

        if len(role.name) > 32:
            report['name']['errors'].append('Name cannot be longer than 32 characters.')

        dupe = self.session.query(Role).filter(Role.name == role.name).first()
        if dupe is not None and role.uid != dupe.uid:
            report['name']['errors'].append('Name already in use.')

        return report
