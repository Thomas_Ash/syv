from typing import Union, List, Tuple, Dict

from sqlalchemy import and_

from syv.models import Pump, Tank, Role, User
from syv.icons import get_icons
from .base_service import BaseService


class PumpService(BaseService):

    def get_by_uid(self, pump_uid: int) -> Union[Pump, None]:
        return self.session.query(Pump).filter(Pump.uid == pump_uid).first()

    def get_by_name(self, pump_name: str) -> Union[Pump, None]:
        return self.session.query(Pump).filter(Pump.name == pump_name).first()

    def list_active(self) -> List[Pump]:
        return self.session.query(Pump).filter(Pump.is_disabled.is_(False)).all()

    def list_executable(self, user: User) -> List[Pump]:
        role_uids = [x.uid for x in user.roles if not x.is_disabled]

        return self.session.query(Pump).order_by(Pump.display_name).filter(and_(
            Pump.is_disabled.is_(False),
            Pump.roles.any(Role.uid.in_(role_uids))
        )).all()

    def page(self, skip: int = 0, take: Union[int, None] = None) -> Tuple[List[Pump], int]:
        q = self.session.query(Pump)
        count = q.count()

        q = q.order_by(Pump.uid.desc()).offset(skip)

        if take is not None:
            q = q.limit(take)

        return q.all(), count

    def load_roles(self, pump: Pump, data: Dict) -> None:
        roles = pump.roles or []
        role_uids = data.get('role_uids') or []
        remove_role_uids = [x.uid for x in roles if not any([y for y in role_uids if y == x.uid])]
        add_role_uids = [x for x in role_uids if not any([y for y in roles if y.uid == x])]
        pump.roles = [x for x in roles if x.uid not in remove_role_uids] or []
        pump.roles.extend(self.session.query(Role).filter(Role.uid.in_(add_role_uids)).all())

    def validate(self, pump: Pump) -> Dict:
        report = {
            'name': {'errors': []},
            'fa_code': {'errors': []},
            'engine_code': {'errors': []},
            'pipes': {
                'in': [],
                'out': []
            },
            'engine_config': {}
        }

        if len(pump.name) <= 0:
            report['name']['errors'].append('Name cannot be blank.')

        if len(pump.name) > 32:
            report['name']['errors'].append('Name cannot be longer than 32 characters.')

        dupe = self.session.query(Pump).filter(Pump.name == pump.name).first()
        if dupe is not None and pump.uid != dupe.uid:
            report['name']['errors'].append('Name already in use.')

        if pump.fa_code not in get_icons():
            report['fa_code']['errors'].append('Icon code is invalid.')

        engine_type = pump.get_engine_type()

        if engine_type is None:
            report['engine_code']['errors'].append('Engine code is invalid.')
        else:
            # instantiate engine, check pipes, get errors & splice into report
            engine = engine_type(pump)
            pipes_report = {
                'in': [],
                'out': []
            }
            for pipe in pump.pipes:
                pipe_report = {
                    'tank_uid': {'errors': []},
                    'filters': []
                }
                if self.session.query(Tank).filter(Tank.uid == pipe.tank_uid).count() != 1:
                    pipe_report['tank_uid']['errors'].append('Tank is invalid.')

                for _filter in pipe.filters:

                    filter_report = {
                        'name': {'errors': []},
                        'membrane_code': {'errors': []},
                        'membrane_config': {}
                    }

                    if len(_filter.name) <= 0:
                        filter_report['name']['errors'].append('Name cannot be blank.')

                    if len(_filter.name) > 32:
                        filter_report['name']['errors'].append('Name cannot be longer than 32 characters.')

                    name_count = len([x for x in pipe.filters if x.name == _filter.name])

                    if name_count > 1:
                        filter_report['name']['errors'].append(f'Name already in use.')

                    membrane_type = _filter.get_membrane_type()

                    if membrane_type is None:
                        filter_report['membrane_code']['errors'].append('Membrane code is invalid.')
                    else:
                        membrane = membrane_type(_filter, pipe)
                        membrane_report = membrane.validate(self.session)

                        filter_report['membrane_code'] = membrane_report['membrane_code']
                        filter_report['membrane_config'] = membrane_report['membrane_config']

                    pipe_report['filters'].append(filter_report)

                if pipe.is_input:
                    pipe_report['drop_limit'] = {'errors': []}
                    try:
                        drop_limit = int(pipe.drop_limit)
                        assert drop_limit > 0
                    except (ValueError, AssertionError):
                        pipe_report['drop_limit']['errors'].append('Drop limit should be a positive integer.')
                    pipes_report['in'].append(pipe_report)

                else:
                    pipes_report['out'].append(pipe_report)

            engine_report = engine.validate(self.session)

            report['pipes'] = pipes_report
            report['engine_code'] = engine_report['engine_code']
            report['engine_config'] = engine_report['engine_config']

            if set(engine.get_pipes()[0]) != set([x.name for x in pump.pipes if x.is_input]):
                report['engine_code']['errors'].append('Input pipe configuration is invalid.')

            if set(engine.get_pipes()[1]) != set([x.name for x in pump.pipes if not x.is_input]):
                report['engine_code']['errors'].append('Output pipe configuration is invalid.')

        return report
