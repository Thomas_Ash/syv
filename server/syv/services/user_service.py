from typing import Union, Tuple, List, Dict

from werkzeug.security import check_password_hash
from flask import current_app, Request
import jwt

from syv.models import User, Role
from .base_service import BaseService


class UserService(BaseService):

    def get_by_uid(self, user_uid: int) -> Union[User, None]:
        return self.session.query(User).filter(User.uid == user_uid).first()

    def page(self, skip: int = 0, take: Union[int, None] = None) -> Tuple[List[User], int]:
        q = self.session.query(User)
        count = q.count()

        q = q.order_by(User.uid.desc()).offset(skip)

        if take is not None:
            q = q.limit(take)

        return q.all(), count

    def load_roles(self, user: User, data: Dict) -> None:
        roles = user.roles or []
        role_uids = data.get('role_uids') or []
        remove_role_uids = [x.uid for x in roles if not any([y for y in role_uids if y == x.uid])]
        add_role_uids = [x for x in role_uids if not any([y for y in roles if y.uid == x])]
        user.roles = [x for x in roles if x.uid not in remove_role_uids] or []
        user.roles.extend(self.session.query(Role).filter(Role.uid.in_(add_role_uids)).all())

    def validate(self, user: User) -> Dict:
        report = {
            'name': {'errors': []},
            'request_limit': {'errors': []},
            'request_timeout': {'errors': []}
        }

        if len(user.name) <= 0:
            report['name']['errors'].append('Name cannot be blank.')

        if len(user.name) > 32:
            report['name']['errors'].append('Name cannot be longer than 32 characters.')

        dupe = self.session.query(User).filter(User.name == user.name).first()
        if dupe is not None and user.uid != dupe.uid:
            report['name']['errors'].append('Name already in use.')

        if user.request_limit is not None:
            try:
                request_limit = int(user.request_limit)
                assert request_limit > 0
            except (TypeError, ValueError, AssertionError):
                report['request_limit']['errors'].append('Request limit should be a positive integer or empty.')

        if user.request_timeout is not None:
            try:
                request_timeout = int(user.request_timeout)
                assert request_timeout > 0
            except (TypeError, ValueError, AssertionError):
                report['request_timeout']['errors'].append('Request timeout should be a positive integer or empty.')

        return report

    def authenticate(self, name: str, password: str) -> Union[User, None]:
        if not name or not password:
            return None

        user = self.session.query(User).filter(User.name == name).first()
        if not user or user.is_disabled or not check_password_hash(user.password, password):
            return None

        return user

    def authenticated_user(self, request: Request) -> Union[User, None]:
        auth_headers = request.headers.get('Authorization', '').split()

        if len(auth_headers) != 2:
            return None  # missing or corrupt headers

        try:
            token = auth_headers[1]
            data = jwt.decode(
                token,
                current_app.config['SECRET_KEY'],
                algorithms=['HS256']
            )
            user = self.session.query(User).filter(User.name == data['sub']).first()
            assert user is not None
            assert not user.is_disabled
        except (jwt.ExpiredSignatureError,
                jwt.InvalidTokenError,
                AssertionError,
                Exception):
            return None  # invalid/expired token or missing/disabled user

        return user
