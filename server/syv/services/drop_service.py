from typing import Union, List, Tuple

from sqlalchemy import and_, func
from sqlalchemy.sql import text

from syv.models import Drop, ArrayInst, ArraySpec, Array
from .base_service import BaseService


class DropService(BaseService):

    def get_by_uid(self, drop_uid: int) -> Union[Drop, None]:
        return self.session.query(Drop).filter(Drop.uid == drop_uid).first()

    def list_by_cycle_filtered(self, cycle_uid, drop_uid_list):
        return self.session.query(Drop).filter(and_(
            Drop.cycle_uid == cycle_uid,
            Drop.uid.in_(drop_uid_list)
        )).all()

    def list_uids_by_tank_and_cycle(self, tank_uid: int, cycle_uid: int) -> List[Drop]:
        return [x for (x,) in self.session.query(Drop.uid).filter(and_(
                    Drop.tank_uid == tank_uid,
                    Drop.cycle_uid == cycle_uid
                )).all()]

    def page_by_tank(self, tank_uid: int, skip: int = 0, take: Union[int, None] = None) -> Tuple[List[Drop], int]:
        q = self.session.query(Drop).filter(Drop.tank_uid == tank_uid)
        count = q.count()
        q = q.order_by(Drop.uid.desc()).offset(skip)

        if take is not None:
            q = q.limit(take)

        return q.all(), count

    def count_by_tank(self, tank_uid: int) -> int:
        return self.session.query(Drop).filter(Drop.tank_uid == tank_uid).count()

    def delete_by_tank(self, tank_uid: int) -> None:
        self.session.query(Drop).filter(Drop.tank_uid == tank_uid).delete()

    def count_by_arrayspec_and_hash(self, arrayspec_uid: int) -> List[Tuple[str, int]]:
        arrayspec = self.session.query(ArraySpec).filter(ArraySpec.uid == arrayspec_uid).first()

        res = self.session.query(
            Array.data_hash,
            func.count(Array.data_hash).label('drop_count')
        ).join(ArrayInst).join(Drop).group_by(Array.data_hash).filter(and_(
            Drop.tank_uid == arrayspec.tank_uid,
            ArrayInst.arrayspec_uid == arrayspec.uid
        )).order_by(text('drop_count DESC')).all()

        return [(x[0], x[1]) for x in res]

    def page_count_by_arrayspec_and_hash(
            self, arrayspec_uid: int, skip: int = 0, take: Union[int, None] = None
    ) -> Tuple[List[Tuple[str, int]], int]:

        arrayspec = self.session.query(ArraySpec).filter(ArraySpec.uid == arrayspec_uid).first()

        q = self.session.query(
            Array.data_hash,
            func.count(Array.data_hash).label('drop_count')
        ).join(ArrayInst).join(Drop).group_by(Array.data_hash).filter(and_(
            Drop.tank_uid == arrayspec.tank_uid,
            ArrayInst.arrayspec_uid == arrayspec.uid
        ))

        count = q.count()

        q = q.order_by(text('drop_count DESC')).offset(skip)

        if take is not None:
            q = q.limit(take)

        res = q.all()

        return [(x[0], x[1]) for x in res], count
