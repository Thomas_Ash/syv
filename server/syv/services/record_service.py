import os
import io
from typing import Union

from sqlalchemy import and_

from syv.root import server_root
from syv.models import Record, Cycle
from .base_service import BaseService


class RecordService(BaseService):

    def get_by_uid(self, record_uid: int) -> Union[Record, None]:
        return self.session.query(Record).filter(Record.uid == record_uid).first()

    def save(self, stream: io.BytesIO, filename: str, name: str, cycle: Cycle) -> Union[Record, None]:

        engine_records = cycle.pump.get_engine_type().get_records()
        if name in engine_records[0]:
            is_input = True
        elif name in engine_records[1]:
            is_input = False
        else:
            # invalid name
            return

        dupe = self.session.query(Record).filter(and_(
            Record.is_input == is_input,
            Record.cycle_uid == cycle.uid,
            Record.name == name
        )).first()

        if dupe is not None:
            # duplicate
            return

        path = os.path.join(server_root,
                            'records',
                            str(cycle.uid),
                            name,
                            filename)

        with open(path, 'wb') as file:
            byte_count = file.write(stream.getvalue())

        record = Record(
            name=name,
            is_input=is_input,
            filename=filename,
            cycle_uid=cycle.uid,
            byte_count=byte_count
        )

        cycle.records.append(record)

        return record

    def append(self, stream: io.BytesIO, record: Record, offset: int) -> Record:

        path = record.get_path()

        with open(path, 'ab') as file:
            file.seek(offset)
            file.write(stream.getvalue())

            record.byte_count = file.tell()

        return record
