from typing import Union, Tuple, List, Dict
import copy

from syv.models import Tome
from .base_service import BaseService


class TomeService(BaseService):

    def get_by_uid(self, tome_uid: int) -> Union[Tome, None]:
        return self.session.query(Tome).filter(Tome.uid == tome_uid).first()

    def get_by_name(self, tome_name: str) -> Union[Tome, None]:
        return self.session.query(Tome).filter(Tome.name == tome_name).first()

    def page(self, skip: int = 0, take: Union[int, None] = None) -> Tuple[List[Tome], int]:
        q = self.session.query(Tome)
        count = q.count()

        q = q.order_by(Tome.uid.desc()).offset(skip)

        if take is not None:
            q = q.limit(take)

        return q.all(), count

    def validate(self, tome: Tome) -> Dict:
        report = {
            'name': {'errors': []}
        }

        if len(tome.name) <= 0:
            report['name']['errors'].append('Name cannot be blank.')

        if len(tome.name) > 32:
            report['name']['errors'].append('Name cannot be longer than 32 characters.')

        dupe = self.session.query(Tome).filter(Tome.name == tome.name).first()
        if dupe is not None and tome.uid != dupe.uid:
            report['name']['errors'].append('Name already in use.')

        return report

    def append_entries(self, tome_uid: int, new_entries: List[str]) -> Tome:
        tome = self.session.query(Tome).with_for_update().filter(Tome.uid == tome_uid).first()
        entries = copy.deepcopy(tome.entries)
        unique_entries = [x for x in new_entries if x not in entries]
        entries.extend(unique_entries)
        tome.entries = entries

        return tome

    def replace_entries(self, tome_uid: int, new_entries: List[str]) -> Tome:
        tome = self.session.query(Tome).with_for_update().filter(Tome.uid == tome_uid).first()
        tome.entries = new_entries

        return tome
