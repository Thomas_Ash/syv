from typing import Union

from syv.models import ArraySpec
from .base_service import BaseService


class ArraySpecService(BaseService):

    def get_by_uid(self, arrayspec_uid: int) -> Union[ArraySpec, None]:
        return self.session.query(ArraySpec).filter(ArraySpec.uid == arrayspec_uid).first()
