from typing import List
from datetime import datetime, timedelta

from sqlalchemy import and_

from syv.models import Request, User
from .base_service import BaseService


class RequestService(BaseService):

    def list_expired(self) -> List[Request]:
        return self.session.query(Request).filter(and_(
            Request.expire_after.is_not(None),
            Request.expire_after <= datetime.now()
        )).all()

    def make_request(self, user: User) -> bool:
        if user.request_limit is None or user.request_timeout is None:
            return True

        request_count = self.session.query(Request).filter(Request.user_uid == user.uid).count()
        if request_count >= user.request_limit:
            return False

        request = Request(user_uid=user.uid,
                          expire_after=datetime.now() + timedelta(minutes=user.request_timeout))

        self.session.add(request)
        return True

