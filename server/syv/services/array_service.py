import copy
import pickle
from typing import Union, List

import numpy as np
from pyblake2 import blake2b
from sqlalchemy import cast, and_, func
from sqlalchemy.dialects.postgresql import JSONB

from syv.models import Array, ArrayInst, ArraySpec, Pump
from .base_service import BaseService
from syv.exceptions import FaceDataError


class ArrayService(BaseService):

    def get_by_data_hash(self, data_hash: str) -> Union[Array, None]:
        return self.session.query(Array).filter(
            Array.data_hash == data_hash
        ).first()

    def list_orphaned(self) -> List[Array]:
        return self.session.query(Array).outerjoin(
            ArrayInst,
            Array.data_hash == ArrayInst.array_data_hash
        ).filter(and_(
            func.jsonb_array_length(Array.cycle_uids) == 0,
            func.jsonb_array_length(Array.pump_uids) == 0,
            ArrayInst.uid.is_(None)
        )).all()

    def get_data_hash(self, data_array: np.ndarray) -> str:
        assert type(data_array) is np.ndarray

        p = pickle.dumps(data_array)
        b = blake2b(digest_size=16)
        b.update(p)
        return b.hexdigest()

    def save(self, data_array: np.ndarray, cycle_uid: Union[int, None] = None,
             pump_uid: Union[int, None] = None) -> str:

        assert type(data_array) is np.ndarray

        p = pickle.dumps(data_array)
        b = blake2b(digest_size=16)
        b.update(p)
        data_hash = b.hexdigest()

        dupe = self.session.query(Array).filter(Array.data_hash == data_hash).first()

        if dupe is not None:
            if cycle_uid is not None and cycle_uid not in dupe.cycle_uids:
                cycle_uids = copy.deepcopy(dupe.cycle_uids)
                cycle_uids.append(cycle_uid)
                dupe.cycle_uids = cycle_uids

            if pump_uid is not None and pump_uid not in dupe.pump_uids:
                pump_uids = copy.deepcopy(dupe.pump_uids)
                pump_uids.append(pump_uid)
                dupe.pump_uids = pump_uids

            return data_hash

        array = Array(
            data_hash=data_hash,
            data_pickle=p,
            cycle_uids=[] if cycle_uid is None else [cycle_uid],
            pump_uids=[] if pump_uid is None else [pump_uid]
        )

        self.session.add(array)

        return data_hash

    def save_constant_arrays(self, pump: Pump) -> None:
        for field_key in pump.constant_arrays:
            try:
                arrayspec_uid = int(pump.constant_arrays[field_key].get('arrayspec_uid'))
                arrayspec = self.session.query(ArraySpec).filter(ArraySpec.uid == arrayspec_uid).first()
                face = arrayspec.get_face_type()(arrayspec, self.session)
                data_array = face.load(pump.constant_arrays[field_key].get('data'), 'web', self.session)
                self.save(data_array, pump_uid=pump.uid)
            except(KeyError, ValueError, FaceDataError):
                continue

    def unlink_cycle(self, cycle_uid: int, exclude_hashes: Union[List[str], None] = None) -> None:
        if exclude_hashes is None:
            exclude_hashes = []

        # can be optimized: really don't want full data here
        arrays = self.session.query(Array).filter(and_(
            Array.cycle_uids.contains(cast(cycle_uid, JSONB)),
            Array.data_hash.notin_(exclude_hashes)
        )).all()

        for array in arrays:
            array.cycle_uids = [x for x in array.cycle_uids if x != cycle_uid]

    def unlink_pump(self, pump_uid: int, exclude_hashes: Union[List[str], None] = None) -> None:
        if exclude_hashes is None:
            exclude_hashes = []

        # can be optimized: really don't want full data here
        arrays = self.session.query(Array).filter(and_(
            Array.pump_uids.contains(cast(pump_uid, JSONB)),
            Array.data_hash.notin_(exclude_hashes)
        )).all()

        for array in arrays:
            array.pump_uids = [x for x in array.pump_uids if x != pump_uid]
