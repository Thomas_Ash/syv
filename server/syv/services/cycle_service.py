import os
import shutil
from datetime import datetime, timedelta
import copy
import logging
from typing import Union, List, Tuple

import numpy as np
from uwsgi_tasks import task
from sqlalchemy import and_, func

from syv.models import Cycle, Pump, Array, Drop, ArrayInst
from syv.database import session_scope
from syv.root import server_root
from syv.exceptions import EngineConfigError
from syv.utils import get_timespan_minutes
from .base_service import BaseService
from .array_service import ArrayService



@task
def process(cycle_uid: int) -> None:
    with session_scope() as session:
        cycle_service = CycleService(session)
        cycle = cycle_service.get_by_uid(cycle_uid)

        try:
            assert isinstance(cycle, Cycle)
            assert cycle.error is None

            engine_type = cycle.pump.get_engine_type()
            engine = engine_type(cycle.pump)
            engine.process(cycle, session)
            cycle_service.complete(cycle)
            session.commit()
            return

        except (Exception,) as e:
            cycle_service.abort(f'Error in process: {str(e)}', cycle)
            session.commit()
            return


class CycleService(BaseService):

    def get_by_uid(self, cycle_uid: int) -> Union[Cycle, None]:
        return self.session.query(Cycle).filter(Cycle.uid == cycle_uid).first()

    def list_pre_commit(self) -> List[Cycle]:
        return self.session.query(Cycle).filter(
            and_(
                Cycle.is_committed.is_(False),
                Cycle.error.is_(None),
                Cycle.commit_before.is_not(None),
                Cycle.commit_before <= datetime.now()
            )
        ).all()

    def list_expired(self) -> List[Cycle]:
        return self.session.query(Cycle).filter(
            Cycle.expire_after.is_not(None),
            Cycle.expire_after <= datetime.now()
        ).all()

    def list_processing_by_pump_uid(self, pump_uid: int) -> List[Cycle]:
        return self.session.query(Cycle).filter(
            and_(
                Cycle.pump_uid == pump_uid,
                Cycle.error.is_(None),
                Cycle.progress < 100
            )
        ).all()

    def page(self, user_uid: int = None, pump_uid: int = None,
             skip: int = 0, take: Union[int, None] = None) -> Tuple[List[Cycle], int]:
        q = self.session.query(Cycle)

        if user_uid is not None:
            q = q.filter(Cycle.user_uid == user_uid)

        if pump_uid is not None:
            q = q.filter(Cycle.pump_uid == pump_uid)

        count = q.count()
        q = q.order_by(Cycle.uid.desc()).offset(skip)
        if take is not None:
            q = q.limit(take)

        return q.all(), count

    def count_by_pump_uid(self, pump_uid: int) -> int:
        return self.session.query(Cycle).filter(Cycle.pump_uid == pump_uid).count()

    def open(self, pump_uid: int, user_uid: int) -> Union[Cycle, None]:
        pump = self.session.query(Pump).filter(Pump.uid == pump_uid).first()
        if pump is None:
            return None

        if pump.engine_config.get('commit_timeout'):
            try:
                commit_before = datetime.now() + timedelta(
                    minutes=get_timespan_minutes(pump.engine_config.get('commit_timeout_duration'))
                )
            except (Exception,):
                raise EngineConfigError
        else:
            commit_before = None

        cycle = Cycle(
            pump_uid=pump.uid,
            user_uid=user_uid,
            client_drops={x.name: [] for x in pump.pipes if not x.is_input},
            client_config={},
            output_drops={x.name: [] for x in pump.pipes if not x.is_input},
            input_drops=[],
            progress=0,
            is_committed=False,
            error=None,
            commit_before=commit_before,
            expire_after=None,
            state={}
        )

        for pipe in pump.pipes:
            if pipe.is_input:
                query = self.session.query(Drop).with_for_update().filter(and_(
                    Drop.cycle_uid.is_(None),
                    Drop.tank_uid == pipe.tank_uid
                ))

                membranes = [
                    x.get_membrane_type()(x, pipe)
                    for x in pipe.filters
                    if not x.is_disabled
                ]

                for membrane in membranes:
                    query = membrane.process_input_query(query, self.session)

                drops = query.order_by(func.random()).limit(pipe.drop_limit).all()

                for membrane in membranes:
                    drops = membrane.process_input_drops(drops, self.session)

                cycle.input_drops.extend(drops)

        self.session.add(cycle)
        
        return cycle

    def post_open(self, cycle: Cycle):
        engine = cycle.pump.get_engine_type()(cycle.pump)

        if engine.open_success(cycle):

            record_names = engine.get_records()
            record_names = record_names[0] + record_names[1]

            if len(record_names) > 0:
                records_path = os.path.join(server_root, 'records', str(cycle.uid))

                if os.path.isdir(records_path):
                    shutil.rmtree(records_path)

                os.mkdir(records_path)

                for record_name in record_names:
                    os.mkdir(os.path.join(records_path, record_name))
            logging.info(
                f'cycle {cycle.uid} of pump "{cycle.pump.name}" and user ' +
                f'"{cycle.user.name}" opened and locked {len(cycle.input_drops)} drops.'
            )
        else:
            self.abort('Prerequisites for cycle open were not met.', cycle)

    def commit(self, cycle: Cycle) -> None:
        cycle.is_committed = True

    def post_commit(self, cycle: Cycle) -> None:
        logging.info(
            f'cycle {cycle.uid} of pump "{cycle.pump.name}" and user "{cycle.user.name}" ' +
            'committed and started processing.'
        )
        process(cycle.uid)

    def abort(self, error: str, cycle: Cycle) -> None:
        cycle.error = error
        cycle.input_drops = []
        self.finalize(cycle)
        logging.info(
            f'cycle {cycle.uid} of pump "{cycle.pump.name}" and user "{cycle.user.name}" failed: {cycle.error}'
        )

    def complete(self, cycle):
        if not cycle.is_committed or cycle.error is not None:
            return

        delete_from_tank_uids = [x.tank_uid for x in cycle.pump.pipes if x.is_input and x.delete_drops]
        delete_drop_uids = [x.uid for x in cycle.input_drops if x.tank_uid in delete_from_tank_uids]
        output_drops = []

        for pipe_name in cycle.output_drops:
            pipe = cycle.pump.get_output_pipe(pipe_name)

            if pipe is None:
                self.abort(f'Engine "{cycle.pump.engine_code}" generated an invalid pipe name.', cycle)
                return

            drops = cycle.output_drops[pipe_name]
            membranes = [
                x.get_membrane_type()(x, pipe)
                for x in pipe.filters
                if not x.is_disabled
            ]

            for membrane in membranes:
                drops = membrane.process_output_drops(drops, self.session)

            for drop in drops:
                for arrayspec in pipe.tank.arrayspecs:
                    array = self.session.query(Array).filter(Array.data_hash == drop.get(arrayspec.name)).first()

                    if array is None:
                        self.abort(f'Engine "{cycle.pump.engine_code}" generated a null array ' +
                                   f'for arrayspec "{arrayspec.name}".', cycle)
                        return

                    elif type(array.data_array) is not np.ndarray:
                        self.abort(f'Engine "{cycle.pump.engine_code}" generated ' +
                                   f'type "{type(array.data_array)}" ' +
                                   f'for arrayspec "{arrayspec.name}".', cycle)
                        return

                    elif not arrayspec.shape_fits(array.data_array.shape):
                        self.abort(f'Engine "{cycle.pump.engine_code}" generated ' +
                                   f'misshaped array {array.data_array.shape} ' +
                                   f'for arrayspec "{arrayspec.name}".', cycle)
                        return

                    elif arrayspec.dtype != array.data_array.dtype.char:
                        self.abort(f'Engine "{cycle.pump.engine_code}" generated ' +
                                   f'mistyped array {array.data_array.dtype.char} ' +
                                   f'for arrayspec "{arrayspec.name}".', cycle)
                        return

                output_drops.append(Drop(
                    tank_uid=pipe.tank_uid,
                    arrayinsts=[ArrayInst(
                        arrayspec_uid=x.uid,
                        array_data_hash=drop.get(x.name)
                    ) for x in pipe.tank.arrayspecs],
                    user_uid=cycle.user_uid,
                    cycle_uid=None
                ))

        cycle.progress = 100
        cycle.input_drops = []

        try:
            # bulk_save_objects is faster but does not add arrayinsts via the relationship
            # maybe can be made to work with two calls? is FK populated after the first one?
            self.session.add_all(output_drops)
            self.session.query(Drop).filter(Drop.uid.in_(delete_drop_uids)).delete()
            self.finalize(cycle)

            if len(cycle.state) > 0:
                state = copy.deepcopy(cycle.pump.state)
                state.update({
                    cycle.uid: cycle.state
                })
                cycle.pump.state = state

            logging.info(
                f'cycle {cycle.uid} of pump "{cycle.pump.name}" and user "{cycle.user.name}" completed ' +
                f'and generated {len(output_drops)} drops.'
            )

        except (Exception,) as e:
            self.session.rollback()
            self.abort(f'Error in complete: {str(e)}', cycle)

    def finalize(self, cycle: Cycle) -> None:
        array_service = ArrayService(self.session)

        array_service.unlink_cycle(cycle.uid)

        if cycle.pump.engine_config.get('expiry_timeout'):
            try:
                expire_after = datetime.now() + timedelta(
                    minutes=get_timespan_minutes(cycle.pump.engine_config.get('expiry_timeout_duration'))
                )
            except (Exception,):
                raise EngineConfigError
        else:
            expire_after = None

        cycle.expire_after = expire_after

    def freeze(self, cycle: Cycle) -> None:
        cycle.expire_after = None

    def delete(self, cycle: Cycle) -> None:
        records_path = os.path.join(server_root,
                                    'records',
                                    str(cycle.uid))

        if os.path.isdir(records_path):
            shutil.rmtree(records_path)

        self.session.delete(cycle)
