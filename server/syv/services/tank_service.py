from typing import Union, Tuple, List, Dict

from sqlalchemy import and_

from syv.models import Tank, ArraySpec, Drop, Pipe, Pump
from .base_service import BaseService


class TankService(BaseService):

    def get_by_uid(self, tank_uid: int) -> Union[Tank, None]:
        return self.session.query(Tank).filter(Tank.uid == tank_uid).first()

    def has_drops(self, tank_uid: int) -> bool:
        return self.session.query(Drop).filter(Drop.tank_uid == tank_uid).count() > 0

    def has_active_pumps(self, tank_uid: int) -> bool:
        return self.session.query(Pipe).join(Pipe.pump).filter(and_(
            Pipe.tank_uid == tank_uid,
            Pump.is_disabled.is_(False)
        )).count() > 0

    def page(self, skip: int = 0, take: Union[int, None] = None) -> Tuple[List[Tank], int]:
        q = self.session.query(Tank)
        count = q.count()

        q = q.order_by(Tank.uid.desc()).offset(skip)

        if take is not None:
            q = q.limit(take)

        return q.all(), count

    def validate(self, tank: Tank) -> Dict:
        report = {
            'name': {'errors': []},
            'arrayspecs': []
        }

        if len(tank.name) <= 0:
            report['name']['errors'].append('Name cannot be blank.')

        if len(tank.name) > 32:
            report['name']['errors'].append('Name cannot be longer than 32 characters.')

        dupe = self.session.query(Tank).filter(Tank.name == tank.name).first()
        if dupe is not None and tank.uid != dupe.uid:
            report['name']['errors'].append('Name already in use.')

        for arrayspec in tank.arrayspecs:
            arrayspec_report = {
                'name': {'errors': []},
                'dtype': {'errors': []},
                'dims': [],
                'face_code': {'errors': []},
                'face_config': {}
            }

            if len(arrayspec.name) <= 0:
                arrayspec_report['name']['errors'].append('Name cannot be blank.')

            if len(arrayspec.name) > 32:
                arrayspec_report['name']['errors'].append('Name cannot be longer than 32 characters.')

            if len([x for x in tank.arrayspecs if x.name == arrayspec.name]) != 1:
                arrayspec_report['name']['errors'].append('Name already in use.')

            if arrayspec.dtype not in ArraySpec.get_dtypes():
                arrayspec_report['dtype']['errors'].append('Data type is invalid.')

            for dim in arrayspec.dims:
                dim_report = {'errors': []}

                try:
                    length = int(dim['length'])
                    assert length > 0
                except (ValueError, TypeError, AssertionError):
                    dim_report['errors'].append('Length should be a positive integer.')

                arrayspec_report['dims'].append(dim_report)

            face_type = arrayspec.get_face_type()

            if face_type is None:
                arrayspec_report['face_code']['errors'].append('Face code is invalid.')
            else:
                # instantiate face, get errors & splice into report
                face = face_type(arrayspec, self.session)
                face_report = face.validate(self.session)
                arrayspec_report['face_code'] = face_report['face_code']
                arrayspec_report['face_config'] = face_report['face_config']

            report['arrayspecs'].append(arrayspec_report)

        return report
