from typing import Dict


def has_errors(field: Dict) -> bool:
    if 'errors' in field:
        return len(field['errors']) > 0
    else:
        return any_errors(field)


def any_errors(fields: Dict) -> bool:
    for key in fields:
        if type(fields[key]) is list:
            for field in fields[key]:
                if has_errors(field):
                    return True
        else:
            if has_errors(fields[key]):
                return True
    return False


def get_timespan_minutes(timespan: str) -> int:
    timespan_parts = timespan.split('|')
    number = int(timespan_parts[0])
    unit = timespan_parts[1]

    if unit == 'm':
        return number
    elif unit == 'h':
        return number * 60
    elif unit == 'd':
        return number * 1440
    else:
        raise ValueError
