from flask import Flask, Blueprint
from flask_cors import CORS
from flask_restful import Api
import logging

from syv.resources import *
from .poll import poll


def create_app(app_name='SYV'):
    app = Flask(app_name)
    app.config.from_object('syv.config.Config')

    logging.basicConfig(filename='log/syv.log', level=logging.INFO)
    logging.getLogger('uwsgi_tasks').setLevel(logging.WARNING)
    logging.info('Logging initialized.')

    _ = CORS(
        app,
        expose_headers='*',
        resources={r"/api/*": {"origins": "*"}}
    )

    app.url_map.strict_slashes = False

    api_bp = Blueprint('api', app_name)
    api = Api(api_bp)

    api.add_resource(ArraySpecDtypeResource, '/arrayspec/dtype')
    api.add_resource(ArraySpecFaceResource, '/arrayspec/face')
    api.add_resource(ArraySpecBlankResource, '/arrayspec/<int:arrayspec_uid>/blank')
    api.add_resource(ArraySpecStatsResource, '/arrayspec/<int:arrayspec_uid>/stats')
    api.add_resource(ArraySpecArrayResource, '/arrayspec/<int:arrayspec_uid>/array')

    api.add_resource(LoginResource, '/login')
    api.add_resource(RenewResource, '/renew')

    api.add_resource(CycleResource, '/cycle/<int:cycle_uid>')
    api.add_resource(CycleListResource, '/cycle')
    api.add_resource(CycleCommitResource, '/cycle/<int:cycle_uid>/commit')
    api.add_resource(CycleCancelResource, '/cycle/<int:cycle_uid>/cancel')
    api.add_resource(CycleDropResource, '/cycle/<int:cycle_uid>/drop')
    api.add_resource(CycleFreezeResource, '/cycle/<int:cycle_uid>/freeze')

    api.add_resource(FilterMembraneResource, '/filter/membrane')

    api.add_resource(InfoResource, '/info')

    api.add_resource(PumpResource, '/pump/<int:pump_uid>')
    api.add_resource(PumpListResource, '/pump')
    api.add_resource(PumpVerifyResource, '/pump/verify')
    api.add_resource(PumpIconResource, '/pump/icon')
    api.add_resource(PumpExecResource, '/pump/exec')
    api.add_resource(PumpEngineResource, '/pump/engine')
    api.add_resource(PumpArrayResource, '/pump/array')
    api.add_resource(PumpStatusResource, '/pump/<int:pump_uid>/status')
    api.add_resource(PumpClearResource, '/pump/<int:pump_uid>/clear')

    api.add_resource(RecordResource, '/record/<int:record_uid>')

    api.add_resource(RoleResource, '/role/<int:role_uid>')
    api.add_resource(RoleListResource, '/role')
    api.add_resource(RoleVerifyResource, '/role/verify')

    api.add_resource(TankResource, '/tank/<int:tank_uid>')
    api.add_resource(TankListResource, '/tank')
    api.add_resource(TankVerifyResource, '/tank/verify')
    api.add_resource(TankDropResource, '/tank/<int:tank_uid>/drop')

    api.add_resource(TomeResource, '/tome/<int:tome_uid>')
    api.add_resource(TomeListResource, '/tome')
    api.add_resource(TomeVerifyResource, '/tome/verify')

    api.add_resource(UserResource, '/user/<int:user_uid>')
    api.add_resource(UserListResource, '/user')
    api.add_resource(UserVerifyResource, '/user/verify')
    api.add_resource(UserChangePasswordResource, '/user/<int:user_uid>/change_password')

    app.register_blueprint(api_bp, url_prefix='/api')

    poll()

    return app
