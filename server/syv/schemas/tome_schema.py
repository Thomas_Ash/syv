import typing

from sqlalchemy.orm import Session
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow import (
    INCLUDE, fields
)

from syv.models import Tome
from .tank_schema import TankSchema
from syv.services import TankService


class TomeSchema(SQLAlchemySchema):
    class Meta:
        model = Tome
        load_instance = True
        unknown = INCLUDE

    uid = auto_field()
    name = auto_field()
    entries = fields.List(fields.String, dump_only=True)
    entry_count = fields.Integer(dump_only=True)

    is_locked = fields.Boolean(dump_only=True)

    @staticmethod
    def add_entries(tome: Tome, data: typing.Dict, session: Session, many: bool = False) -> typing.Dict:
        if many:
            assert len(tome) == len(data)
            for idx in range(len(tome)):
                data[idx] = TomeSchema.add_entries(tome[idx], data[idx], session, False)
        else:
            data['entries'] = tome.entries

        return data

    @staticmethod
    def add_entry_count(tome: Tome, data: typing.Dict, session: Session, many: bool = False) -> typing.Dict:
        if many:
            assert len(tome) == len(data)
            for idx in range(len(tome)):
                data[idx] = TomeSchema.add_entry_count(tome[idx], data[idx], session, False)
        else:
            data['entry_count'] = len(tome.entries)

        return data

    @staticmethod
    def add_is_locked(tome: Tome, data: typing.Dict, session: Session, many: bool = False) -> typing.Dict:
        if many:
            assert len(tome) == len(data)
            for idx in range(len(tome)):
                data[idx] = TomeSchema.add_is_locked(tome[idx], data[idx], session, False)
        else:
            data['is_locked'] = TomeSchema.tome_is_locked(tome, session)

        return data

    @staticmethod
    def tome_is_locked(tome: Tome, session: Session) -> bool:
        tank_service = TankService(session)

        tanks, count = tank_service.page(0, None)
        for tank in tanks:
            if TankSchema.tank_is_locked(tank, session) and tome.uid in tank.get_tome_uids(session):
                return True

        return False


tome_schema = TomeSchema(many=False)
tome_list_schema = TomeSchema(many=True)
