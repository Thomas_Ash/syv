import typing

from werkzeug.security import generate_password_hash
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow_sqlalchemy.fields import Nested
from marshmallow import INCLUDE, pre_load, post_dump
from marshmallow.fields import String, Pluck

from syv.models import User
from .role_schema import RoleSchema


# for user CRUD actions by super user

class UserSchema(SQLAlchemySchema):
    class Meta:
        model = User
        load_instance = True
        unknown = INCLUDE

    uid = auto_field()
    name = auto_field()
    is_disabled = auto_field()
    role_uids = Pluck(RoleSchema, 'uid', attribute='roles', many=True, dump_only=True)
    request_limit = auto_field()
    request_timeout = auto_field()


# for login

class UserIdentitySchema(SQLAlchemySchema):
    class Meta:
        model = User
        load_instance = True

    uid = auto_field()
    name = auto_field()
    roles = Nested(RoleSchema, many=True)


# for password change

class UserChangePasswordSchema(SQLAlchemySchema):
    class Meta:
        model = User
        load_instance = True
        unknown = INCLUDE

    uid = auto_field(dump_only=True)
    name = auto_field(dump_only=True)
    old_password = String(load_only=True)
    new_password = String(load_only=True, attribute='password')

    # hash new password

    @pre_load
    def pre_load_process(self, data: typing.Dict, **kwargs) -> typing.Dict:
        data['new_password'] = generate_password_hash(data.get('new_password'))

        return data

    # add old_password & new_password as empty strings

    @post_dump
    def post_dump_process(self, data: typing.Dict, **kwargs) -> typing.Dict:
        data['old_password'] = ''
        data['new_password'] = ''

        return data


user_schema = UserSchema(many=False)
user_list_schema = UserSchema(many=True)
user_identity_schema = UserIdentitySchema(many=False)
user_change_password_schema = UserChangePasswordSchema(many=False)
