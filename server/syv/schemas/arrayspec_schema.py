from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow import INCLUDE

from syv.models import ArraySpec


class ArraySpecSchema(SQLAlchemySchema):
    class Meta:
        model = ArraySpec
        load_instance = True
        unknown = INCLUDE

    uid = auto_field()
    name = auto_field()

    dtype = auto_field()
    dims = auto_field()

    face_code = auto_field()
    face_config = auto_field()


arrayspec_schema = ArraySpecSchema(many=False)
