from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from syv.models import Role


class RoleSchema(SQLAlchemySchema):
    class Meta:
        model = Role
        load_instance = True

    uid = auto_field()
    name = auto_field()
    is_admin = auto_field()
    is_super = auto_field()
    is_disabled = auto_field()


role_schema = RoleSchema(many=False)
role_list_schema = RoleSchema(many=True)
