import typing

from sqlalchemy.orm import Session
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow_sqlalchemy.fields import Nested
from marshmallow import INCLUDE
from marshmallow.fields import Pluck, Dict

from syv.models import Cycle
from .pump_schema import PumpSchema
from .user_schema import UserSchema
from .record_schema import RecordSchema
from syv.services import DropService


class CycleSchema(SQLAlchemySchema):
    class Meta:
        model = Cycle
        load_instance = True
        unknown = INCLUDE

    uid = auto_field()
    pump_uid = auto_field()
    user_uid = auto_field()

    expire_after = auto_field()

    pump_name = Pluck(PumpSchema, 'name', attribute='pump', dump_only=True)
    user_name = Pluck(UserSchema, 'name', attribute='user', dump_only=True)

    pump_engine_code = Pluck(PumpSchema, 'engine_code', attribute='pump', dump_only=True)
    pump_display_name = Pluck(PumpSchema, 'display_name', attribute='pump', dump_only=True)
    pump_fa_code = Pluck(PumpSchema, 'fa_code', attribute='pump', dump_only=True)

    client_config = auto_field()
    progress = auto_field()
    is_committed = auto_field()
    error = auto_field()

    drop_uids = Dict(dump_only=True)
    ask_arrays = Dict(dump_only=True)
    client_config_template = Dict(dump_only=True)
    public_config = Dict(dump_only=True)

    records = Nested(RecordSchema, many=True, dump_only=True)

    @staticmethod
    def add_drop_uids(cycle: Cycle, data: typing.Dict, session: Session) -> typing.Dict:
        data['drop_uids'] = {}

        drop_service = DropService(session)

        for pipe in cycle.pump.pipes:
            if pipe.is_input:
                data['drop_uids'][pipe.name] = drop_service.list_uids_by_tank_and_cycle(pipe.tank_uid, cycle.uid)

        return data

    @staticmethod
    def add_ask_arrays(cycle: Cycle, data: typing.Dict) -> typing.Dict:
        data['ask_arrays'] = {}

        for pipe in cycle.pump.pipes:
            if not pipe.is_input:
                data['ask_arrays'][pipe.name] = {}
                prefix = f'mapping_{pipe.name}_'
                for arrayspec_name in ['_'.join(x.split('_')[2:]) for x in cycle.pump.engine_config if
                                       x.startswith(prefix) and cycle.pump.engine_config[x] == 'ask']:
                    arrayspec = pipe.tank.get_arrayspec(name=arrayspec_name)
                    if arrayspec is not None:
                        data['ask_arrays'][pipe.name][arrayspec_name] = {
                            'arrayspec_uid': arrayspec.uid,
                            'face_code': arrayspec.face_code
                        }

        return data

    @staticmethod
    def add_client_config_template(cycle: Cycle, data: typing.Dict) -> typing.Dict:
        engine = cycle.pump.get_engine_type()(cycle.pump)
        data['client_config_template'] = engine.get_client_config_template()

        return data

    @staticmethod
    def add_public_config(cycle: Cycle, data: typing.Dict) -> typing.Dict:
        engine_type = cycle.pump.get_engine_type()
        engine = engine_type(cycle.pump)
        data['public_config'] = engine.get_public_config()

        return data

    @staticmethod
    def add_details(cycle: Cycle, data: typing.Dict, session: Session, many: bool = False) -> typing.Dict:
        if many:
            assert len(cycle) == len(data)
            for idx in range(len(cycle)):
                data[idx] = CycleSchema.add_drop_uids(cycle[idx], data[idx], session)
                data[idx] = CycleSchema.add_ask_arrays(cycle[idx], data[idx])
                data[idx] = CycleSchema.add_client_config_template(cycle[idx], data[idx])
                data[idx] = CycleSchema.add_public_config(cycle[idx], data[idx])
        else:
            data = CycleSchema.add_drop_uids(cycle, data, session)
            data = CycleSchema.add_ask_arrays(cycle, data)
            data = CycleSchema.add_client_config_template(cycle, data)
            data = CycleSchema.add_public_config(cycle, data)

        return data


cycle_schema = CycleSchema(many=False)
cycle_list_schema = CycleSchema(many=True)
