from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow import INCLUDE

from syv.models import Filter


class FilterSchema(SQLAlchemySchema):
    class Meta:
        model = Filter
        load_instance = True
        unknown = INCLUDE

    uid = auto_field()
    name = auto_field()

    is_disabled = auto_field()

    membrane_code = auto_field()
    membrane_config = auto_field()


filter_schema = FilterSchema(many=False)
