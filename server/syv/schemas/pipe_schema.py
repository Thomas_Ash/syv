from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow_sqlalchemy.fields import Nested
from marshmallow import INCLUDE

from syv.models import Pipe
from .filter_schema import FilterSchema


class PipeSchema(SQLAlchemySchema):
    class Meta:
        model = Pipe
        load_instance = True
        unknown = INCLUDE

    uid = auto_field()
    name = auto_field()

    tank_uid = auto_field()

    is_input = auto_field()
    delete_drops = auto_field()
    drop_limit = auto_field()

    filters = Nested(FilterSchema, many=True)


pipe_schema = PipeSchema(many=False)
