import typing

from sqlalchemy.orm import Session
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow_sqlalchemy.fields import Nested
from marshmallow import INCLUDE
from marshmallow.fields import Integer, Boolean, Dict, Pluck

from syv.models import Pump
from syv.services import CycleService, ArrayService
from .role_schema import RoleSchema
from .pipe_schema import PipeSchema


class PumpSchema(SQLAlchemySchema):
    class Meta:
        model = Pump
        load_instance = True
        unknown = INCLUDE

    uid = auto_field()
    name = auto_field()
    display_name = auto_field()
    fa_code = auto_field()
    is_disabled = auto_field()
    role_uids = Pluck(RoleSchema, 'uid', attribute='roles', many=True, dump_only=True)

    pipes = Nested(PipeSchema, many=True)

    engine_code = auto_field()
    engine_config = auto_field()

    is_locked = Boolean(dump_only=True)
    cycle_count = Integer(dump_only=True)
    state_cycle_count = Integer(dump_only=True)
    constant_arrays = Dict(attribute='constant_arrays')

    @staticmethod
    def add_is_locked(pump, data: typing.Dict) -> typing.Dict:
        data['is_locked'] = not pump.is_disabled

        return data

    @staticmethod
    def add_cycle_count(pump: Pump, data: typing.Dict, session: Session) -> typing.Dict:

        cycle_service = CycleService(session)
        data['cycle_count'] = cycle_service.count_by_pump_uid(pump.uid)

        return data

    @staticmethod
    def add_state_cycle_count(pump: Pump, data: typing.Dict) -> typing.Dict:
        data['state_cycle_count'] = len(pump.state.keys())

        return data

    @staticmethod
    def add_constant_arrays(pump: Pump, data: typing.Dict, session: Session) -> typing.Dict:
        constant_mappings = [
            x for x in pump.engine_config
            if x.startswith('mapping_') and pump.engine_config[x] == 'constant'
        ]

        array_service = ArrayService(session)

        for mapping in constant_mappings:
            mapping_parts = mapping.split('_')
            pipe_name = mapping_parts[1]
            arrayspec_name = '_'.join(mapping_parts[2:])
            pipe = pump.get_pipe(pipe_name)
            if pipe is None:
                continue
            arrayspec = pipe.tank.get_arrayspec(name=arrayspec_name)
            if arrayspec is None:
                continue

            field_key = f'constant_{pipe_name}_{arrayspec_name}'
            data_hash = pump.engine_config.get(field_key)
            array = array_service.get_by_data_hash(data_hash)

            if array is None:
                continue

            face = arrayspec.get_face_type()(arrayspec, session)

            data['constant_arrays'][field_key] = {
                'data': face.dump(array.data_array, 'web', session),
                'data_hash': data_hash,
                'arrayspec_uid': arrayspec.uid,
                'face_code': arrayspec.face_code
            }

        return data

    @staticmethod
    def add_details(pump: Pump, data: typing.Dict, session: Session, many: bool = False) -> typing.Dict:
        if many:
            assert len(pump) == len(data)
            for idx in range(len(pump)):
                data[idx] = PumpSchema.add_details(pump[idx], data[idx], session, False)
        else:
            data = PumpSchema.add_is_locked(pump, data)
            data = PumpSchema.add_cycle_count(pump, data, session)
            data = PumpSchema.add_state_cycle_count(pump, data)
            data = PumpSchema.add_constant_arrays(pump, data, session)

        return data


pump_schema = PumpSchema(many=False)
pump_list_schema = PumpSchema(many=True)
