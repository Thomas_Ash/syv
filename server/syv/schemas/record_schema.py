from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

from syv.models import Record


class RecordSchema(SQLAlchemySchema):
    class Meta:
        model = Record

    uid = auto_field()
    name = auto_field()
    filename = auto_field()
    is_input = auto_field()
    byte_count = auto_field()
