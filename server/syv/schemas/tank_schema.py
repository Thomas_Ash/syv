import typing

from sqlalchemy.orm import Session
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow_sqlalchemy.fields import Nested
from marshmallow import INCLUDE
from marshmallow.fields import Integer, Boolean

from syv.models import Tank
from syv.services import DropService, TankService
from .arrayspec_schema import ArraySpecSchema


class TankSchema(SQLAlchemySchema):
    class Meta:
        model = Tank
        load_instance = True
        unknown = INCLUDE

    uid = auto_field()
    name = auto_field()

    arrayspecs = Nested(ArraySpecSchema, many=True)

    drop_count = Integer(dump_only=True)
    is_locked = Boolean(dump_only=True)

    @staticmethod
    def add_drop_count(tank: Tank, data: typing.Dict, session: Session, many: bool = False) -> typing.Dict:
        if many:
            assert len(tank) == len(data)
            for idx in range(len(tank)):
                data[idx] = TankSchema.add_drop_count(tank[idx], data[idx], session, False)
        else:
            drop_service = DropService(session)

            data['drop_count'] = drop_service.count_by_tank(tank.uid)

        return data

    @staticmethod
    def add_is_locked(tank: Tank, data: typing.Dict, session: Session, many: bool = False) -> typing.Dict:
        if many:
            assert len(tank) == len(data)
            for idx in range(len(tank)):
                data[idx] = TankSchema.add_is_locked(tank[idx], data[idx], session, False)
        else:
            data['is_locked'] = TankSchema.tank_is_locked(tank, session)

        return data

    @staticmethod
    def tank_is_locked(tank: Tank, session: Session) -> bool:
        tank_service = TankService(session)

        has_drops = tank_service.has_drops(tank.uid)
        has_active_pumps = tank_service.has_active_pumps(tank.uid)

        return has_drops or has_active_pumps


tank_schema = TankSchema(many=False)
tank_list_schema = TankSchema(many=True)
