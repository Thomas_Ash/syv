from typing import Union
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from .arrayinst import ArrayInst
from syv.database import BaseModel


class Drop(BaseModel):
    __tablename__ = 'drops'
    uid = Column(Integer, primary_key=True)

    tank_uid = Column(Integer, ForeignKey('tanks.uid'), index=True, nullable=False)
    tank = relationship('Tank')

    arrayinsts = relationship('ArrayInst', back_populates='drop', cascade='all, delete-orphan',
                              order_by='ArrayInst.arrayspec_uid', passive_deletes=True)

    user_uid = Column(Integer, ForeignKey('users.uid'), nullable=False)     # creator
    user = relationship('User')

    cycle_uid = Column(Integer, ForeignKey('cycles.uid'), nullable=True)   # locked by
    cycle = relationship('Cycle', back_populates='input_drops')

    def get_arrayinst(self, arrayspec_uid: int = None, arrayspec_name: str = None) -> Union[ArrayInst, None]:
        if arrayspec_uid is None and arrayspec_name is None:
            return None
        elif arrayspec_uid is not None:
            return next(iter([
                x for x in self.arrayinsts if x.arrayspec_uid == arrayspec_uid
            ]), None)
        else:
            return next(iter([
                x for x in self.arrayinsts if x.arrayspec.name == arrayspec_name
            ]), None)
