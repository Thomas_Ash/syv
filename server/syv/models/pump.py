from typing import Union, Type, List, TYPE_CHECKING
if TYPE_CHECKING:
    from syv.engines import BaseEngine

from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship

from .pipe import Pipe
from syv.database import BaseModel


class Pump(BaseModel):
    __tablename__ = 'pumps'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), unique=True, nullable=False)

    display_name = Column(String(64), nullable=False)
    fa_code = Column(String(32), nullable=False)

    is_disabled = Column(Boolean, default=True, nullable=False)

    pipes = relationship('Pipe', cascade='all, delete-orphan')
    cycles = relationship('Cycle', cascade='all, delete-orphan', passive_deletes=True)
    roles = relationship('Role', secondary='pump_roles', back_populates='pumps')

    engine_code = Column(String(32), nullable=False)
    engine_config = Column(JSONB, nullable=False)

    state = Column(JSONB, nullable=False)

    constant_arrays = {}

    def get_engine_type(self) -> Union[Type['BaseEngine'], None]:
        from syv.engines import get_engine_types

        return next(iter([x for x in get_engine_types() if self.engine_code == x.get_code()]), None)

    def get_pipe(self, pipe_name: str) -> Union[Pipe, None]:
        return next(iter([x for x in self.pipes if pipe_name == x.name]), None)

    def get_input_pipe(self, pipe_name: str) -> Union[Pipe, None]:
        return next(iter([x for x in self.pipes if pipe_name == x.name and x.is_input]), None)

    def get_output_pipe(self, pipe_name: str) -> Union[Pipe, None]:
        return next(iter([x for x in self.pipes if pipe_name == x.name and not x.is_input]), None)

    def get_array_hashes(self) -> List[str]:
        return [self.engine_config[x] for x in self.engine_config if x.startswith('constant_')]
