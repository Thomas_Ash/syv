import os

from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, BigInteger
from sqlalchemy.orm import relationship

from syv.root import server_root
from syv.database import BaseModel


class Record(BaseModel):
    __tablename__ = 'records'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)   # not globally unique, only within cycle
    is_input = Column(Boolean, nullable=False)

    filename = Column(String(256), nullable=False)
    byte_count = Column(BigInteger, nullable=False)

    cycle_uid = Column(Integer, ForeignKey('cycles.uid', ondelete='CASCADE'), nullable=False)
    cycle = relationship('Cycle', back_populates='records')

    def get_path(self) -> str:
        return os.path.join(server_root,
                            'records',
                            str(self.cycle_uid),
                            self.name,
                            self.filename)
