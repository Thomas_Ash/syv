from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from syv.database import BaseModel


class ArrayInst(BaseModel):
    __tablename__ = 'arrayinsts'
    uid = Column(Integer, primary_key=True)

    drop_uid = Column(Integer, ForeignKey('drops.uid', ondelete='CASCADE'), index=True, nullable=False)
    drop = relationship('Drop', back_populates='arrayinsts')

    arrayspec_uid = Column(Integer, ForeignKey('arrayspecs.uid'), nullable=False)
    arrayspec = relationship('ArraySpec')

    array_data_hash = Column(String(32), ForeignKey('arrays.data_hash'), index=True, nullable=False)
    array = relationship('Array', foreign_keys=[array_data_hash], lazy='select')

    def array_data_hash_short(self) -> str:
        return '..' + self.array_data_hash[-4:]
