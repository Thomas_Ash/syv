import pickle
from typing import Tuple

from sqlalchemy import Column, String, LargeBinary
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import reconstructor

from syv.database import BaseModel


class Array(BaseModel):
    __tablename__ = 'arrays'

    data_hash = Column(String(32), primary_key=True)
    data_pickle = Column(LargeBinary, nullable=False)
    data_array = None

    cycle_uids = Column(JSONB, nullable=False)
    pump_uids = Column(JSONB, nullable=False)

    @reconstructor
    def init_on_load(self) -> None:
        self.data_array = pickle.loads(self.data_pickle)

    @staticmethod
    def reverse_axes(axes: Tuple) -> Tuple:
        assert set(axes) == set(range(len(axes)))
        reverse = [0] * len(axes)
        for i in range(len(axes)):
            reverse[axes[i]] = i

        return tuple(reverse)
