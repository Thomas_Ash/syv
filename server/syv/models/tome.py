from typing import Union
from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects.postgresql import JSONB

from syv.database import BaseModel


class Tome(BaseModel):
    __tablename__ = 'tomes'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), unique=True, nullable=False)

    entries = Column(JSONB, nullable=False)

    def get_entry(self, idx: int) -> str:
        if idx < 1 or idx > len(self.entries):
            return ''
        return self.entries[idx-1]

    def get_index(self, entry: str) -> Union[str, None]:
        if entry == '':
            return 0
        if entry not in self.entries:
            return None
        return self.entries.index(entry)+1
