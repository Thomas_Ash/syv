from typing import Tuple, Type, Union, TYPE_CHECKING
if TYPE_CHECKING:
    from syv.faces import BaseFace

from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
import numpy as np

from syv.database import BaseModel


class ArraySpec(BaseModel):
    __tablename__ = 'arrayspecs'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)       # not globally unique, only within tank

    tank_uid = Column(Integer, ForeignKey('tanks.uid', ondelete='CASCADE'), nullable=False)
    tank = relationship('Tank', back_populates='arrayspecs')

    dtype = Column(String, nullable=False)      # numpy dtype code
    dims = Column(JSONB, nullable=False)        # json list of {'length':...,'is_max':...} dicts

    face_code = Column(String(32), nullable=False)
    face_config = Column(JSONB, nullable=False)

    def get_max_shape(self) -> Tuple:
        return tuple([int(x.get('length')) for x in self.dims])

    def get_random_shape(self) -> Tuple:
        return tuple([
            (1 + np.random.randint(int(x.get('length'))))
            if x.get('is_max') else int(x.get('length'))
            for x in self.dims
        ])

    def shape_fits(self, shape: Tuple) -> bool:
        if len(shape) != len(self.dims):
            return False

        for idx in range(len(self.dims)):
            if (self.dims[idx].get('is_max') and shape[idx] > int(self.dims[idx].get('length'))) or \
                    (not self.dims[idx].get('is_max') and shape[idx] != int(self.dims[idx].get('length'))):
                return False

        return True

    def get_face_type(self) -> Union[Type['BaseFace'], None]:
        from syv.faces import get_face_types

        return next(iter([x for x in get_face_types() if self.face_code == x.get_code()]), None)

    # all the numpy dtype codes supported by syv

    @staticmethod
    def get_dtypes() -> str:
        return '?bhilBHILfdgFDG'

    @staticmethod
    def get_int_dtypes() -> str:
        return 'bhilBHIL'

    @staticmethod
    def get_float_dtypes() -> str:
        return 'fdgFDG'
