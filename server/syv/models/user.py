from typing import Union, List
import random
import string

from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash

from syv.database import BaseModel


class User(BaseModel):
    __tablename__ = 'users'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), unique=True, nullable=False)

    password = Column(String(255), nullable=False)
    is_disabled = Column(Boolean, default=True, nullable=False)

    request_limit = Column(Integer, nullable=True)
    request_timeout = Column(Integer, nullable=True)

    roles = relationship('Role', secondary='user_roles', back_populates='users')

    cycles = relationship('Cycle', cascade='all, delete-orphan', passive_deletes=True)

    def set_password(self, password: str) -> None:
        self.password = generate_password_hash(password, method="sha256")

    @staticmethod
    def random_password() -> str:

        return ''.join(random.SystemRandom().choice(string.ascii_lowercase +
                                                    string.ascii_uppercase +
                                                    string.digits) for _ in range(16))

    def is_authorized(self, require_admin: bool = False, require_super: bool = False,
                      require_user_uids: Union[List[int], None] = None,
                      require_role_uids: Union[List[int], None] = None) -> bool:

        if require_admin and not any([x for x in self.roles if x.is_admin and not x.is_disabled]):
            return False  # required admin role missing

        if require_super and not any([x for x in self.roles if x.is_super and not x.is_disabled]):
            return False  # required super role missing

        if require_user_uids is not None and self.uid not in require_user_uids:
            return False  # required user_uid missing

        if require_role_uids is not None and not any([x.uid for x in self.roles
                                                      if x.uid in require_role_uids and not x.is_disabled]):
            return False  # required role_uid missing

        return True
