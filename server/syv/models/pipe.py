from sqlalchemy import Column, Integer, ForeignKey, String, Boolean
from sqlalchemy.orm import relationship

from syv.database import BaseModel


class Pipe(BaseModel):
    __tablename__ = 'pipes'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)   # not globally unique, only within pump

    pump_uid = Column(Integer, ForeignKey('pumps.uid'), nullable=False)
    pump = relationship('Pump', back_populates='pipes')

    tank_uid = Column(Integer, ForeignKey('tanks.uid'), nullable=False)
    tank = relationship('Tank', back_populates='pipes')

    is_input = Column(Boolean, nullable=False)
    delete_drops = Column(Boolean, nullable=True)
    drop_limit = Column(Integer, nullable=True)

    filters = relationship('Filter', cascade='all, delete-orphan')
