from sqlalchemy import Column, Integer, DateTime, ForeignKey

from syv.database import BaseModel


class Request(BaseModel):
    __tablename__ = 'requests'
    uid = Column(Integer, primary_key=True)
    user_uid = Column(Integer, ForeignKey('users.uid'), nullable=False)
    expire_after = Column(DateTime, nullable=False)
