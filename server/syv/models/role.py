from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from syv.database import BaseModel


class Role(BaseModel):
    __tablename__ = 'roles'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), unique=True, nullable=False)
    
    is_admin = Column(Boolean, nullable=False)
    is_super = Column(Boolean, nullable=False)
    is_disabled = Column(Boolean, default=True, nullable=False)

    users = relationship('User', secondary='user_roles', back_populates='roles')
    pumps = relationship('Pump', secondary='pump_roles', back_populates='roles')
