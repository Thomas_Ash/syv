from typing import Union, Type, TYPE_CHECKING
if TYPE_CHECKING:
    from syv.membranes import BaseMembrane

from sqlalchemy import Column, Integer, ForeignKey, String, Boolean
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship

from syv.database import BaseModel


class Filter(BaseModel):
    __tablename__ = 'filters'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)   # not globally unique, only within pipe

    pipe_uid = Column(Integer, ForeignKey('pipes.uid'), nullable=False)
    pipe = relationship('Pipe', back_populates='filters')

    is_disabled = Column(Boolean, default=True, nullable=False)

    membrane_code = Column(String(32), nullable=False)
    membrane_config = Column(JSONB, nullable=False)

    def get_membrane_type(self) -> Union[Type['BaseMembrane'], None]:
        from syv.membranes import get_membrane_types

        return next(iter([x for x in get_membrane_types() if self.membrane_code == x.get_code()]), None)
