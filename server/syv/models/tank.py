from typing import List
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship, Session

from .arrayspec import ArraySpec
from syv.database import BaseModel


class Tank(BaseModel):
    __tablename__ = 'tanks'
    uid = Column(Integer, primary_key=True)
    name = Column(String(32), unique=True, nullable=False)

    arrayspecs = relationship('ArraySpec', back_populates='tank', cascade='all, delete-orphan',
                              order_by='ArraySpec.uid', passive_deletes=True)

    pipes = relationship('Pipe')

    def get_arrayspec(self, uid: int = None, name: str = None) -> ArraySpec:
        if uid is None and name is None:
            return None
        elif uid is not None:
            return next(iter([
                x for x in self.arrayspecs if x.uid == uid
            ]), None)
        else:
            return next(iter([
                x for x in self.arrayspecs if x.name == name
            ]), None)

    def get_tome_uids(self, session: Session) -> List[int]:
        tome_uids = []
        for arrayspec in self.arrayspecs:
            face_type = arrayspec.get_face_type()
            face = face_type(arrayspec, session)
            tome_uids.extend(face.get_tome_uids())

        return list(set(tome_uids))
