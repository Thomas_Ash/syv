from typing import Union

from sqlalchemy import Column, Integer, ForeignKey, Boolean, DateTime, String
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship

from .record import Record
from syv.database import BaseModel


class Cycle(BaseModel):
    __tablename__ = 'cycles'
    uid = Column(Integer, primary_key=True)

    pump_uid = Column(Integer, ForeignKey('pumps.uid', ondelete='CASCADE'), nullable=False)
    pump = relationship('Pump', back_populates='cycles')

    user_uid = Column(Integer, ForeignKey('users.uid', ondelete='CASCADE'), nullable=False)
    user = relationship('User', back_populates='cycles')

    commit_before = Column(DateTime, nullable=True)
    expire_after = Column(DateTime, nullable=True)

    records = relationship('Record', cascade='all, delete-orphan', passive_deletes=True)

    client_drops = Column(JSONB, nullable=False)
    client_config = Column(JSONB, nullable=False)  # added pre-commit by client PUT ops

    input_drops = relationship('Drop', back_populates='cycle')
    output_drops = Column(JSONB, nullable=False)

    progress = Column(Integer, nullable=False)  # percentage
    is_committed = Column(Boolean, nullable=False)

    error = Column(String, nullable=True)
    state = Column(JSONB, nullable=False)


    def can_commit(self) -> bool:
        if self.is_committed or self.error is not None or self.progress >= 100:
            return False

        engine_type = self.pump.get_engine_type()
        if engine_type is None:
            return False

        engine = engine_type(self.pump)

        if not engine.needs_client_post():
            return True

        return engine.can_commit(self)

    def get_record(self, name: str) -> Union[Record, None]:
        return next(iter([x for x in self.records if x.name == name]), None)
