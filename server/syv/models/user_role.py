from sqlalchemy import Column, Integer, ForeignKey

from syv.database import BaseModel


class UserRole(BaseModel):
    __tablename__ = 'user_roles'
    uid = Column(Integer, primary_key=True)

    user_uid = Column(Integer, ForeignKey('users.uid'), nullable=False)
    role_uid = Column(Integer, ForeignKey('roles.uid'), nullable=False)

