from sqlalchemy import Column, Integer, ForeignKey

from syv.database import BaseModel


class PumpRole(BaseModel):
    __tablename__ = 'pump_roles'
    uid = Column(Integer, primary_key=True)

    pump_uid = Column(Integer, ForeignKey('pumps.uid'), nullable=False)
    role_uid = Column(Integer, ForeignKey('roles.uid'), nullable=False)

