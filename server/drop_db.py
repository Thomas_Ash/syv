try:
    import syv.models  # this import is required to build metadata despite not being directly referenced
    from syv.database import session_scope, engine, BaseModel
    from syv.app import create_app

    app = create_app()

    with app.app_context():

        with session_scope() as session:

            BaseModel.metadata.drop_all(bind=engine)
            session.commit()

            print('Dropped database.')

except (Exception,) as e:
    print('Failed to drop database:')
    print(str(e))
    print('Try running:')
    print('    `sudo -u postgres dropdb syv`')