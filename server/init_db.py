import syv.models
from syv.database import session_scope, BaseModel, engine

from syv.app import create_app

app = create_app()

with app.app_context():

    try:
        BaseModel.metadata.create_all(bind=engine)

        with session_scope() as session:

            password = syv.models.User.random_password()

            role = syv.models.Role(name='super', is_admin=True,
                                   is_super=True, is_disabled=False)
            session.add(role)

            user = syv.models.User(name='super', is_disabled=False,
                                   roles=[role], request_limit=None,
                                   request_timeout=None)
            user.set_password(password)

            session.add(user)
            session.commit()

            print('Initialized database.')
            print('Password for `super` user:')
            print('    ' + password)

    except (Exception,) as e:
        print('Failed to initialize database:')
        print(str(e))
        print('This script should be run immediately after:')
        print('    `venv/bin/python drop_db.py`')
        print('or:')
        print('    `sudo -u postgres createdb -O syv syv`')
