import pathlib
import sys

server_dir = str(pathlib.Path(__file__).parent.parent.resolve())
sys.path.insert(0, server_dir)

from syv.models import ArraySpec
from syv.database import session_scope
from syv.exceptions import FaceModeError

face_types = ArraySpec.get_face_types()

with session_scope() as session:

    for face_type in face_types:
        print(f'face code: "{face_type.get_code()}"')

        modes = face_type.get_modes()
        try:
            assert type(modes) is list
        except (AssertionError,):
            print('ERROR: get_modes() did not return a list.')
            continue

        for mode in modes:
            try:
                assert type(mode) is tuple
                # assert len(mode) == 2
                assert len(mode) == 4
                assert type(mode[0]) is str
                assert type(mode[1]) is bool
                assert type(mode[2]) is bool
                assert type(mode[3]) is bool
            except (AssertionError,):
                print('  ERROR: format of a list item returned by get_modes() is incorrect.')
                continue

            print(f'  mode name: "{mode[0]}"')
            print(f'  serializable: {"yes" if mode[1] else "no"}')
            print(f'  dumpable: {"yes" if mode[2] else "no"}')
            print(f'  loadable: {"yes" if mode[3] else "no"}')

            arrayspec = ArraySpec(dims=[],face_config={})
            face = face_type(arrayspec, session)

            if mode[2]:
                try:
                    data = face.dump(None, mode[0])
                except (FaceModeError,):
                    print('  ERROR: dump not supported')
                except (Exception,):
                    pass

            if mode[3]:
                try:
                    array = face.load(None, mode[0])
                except (FaceModeError,):
                    print('  ERROR: load not supported')
                except (Exception,):
                    pass

    session.rollback()
