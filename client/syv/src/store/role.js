import {
  loadRoles, loadRole,
  addRole, updateRole, verifyRole, deleteRole
} from '@/api/role'

export default {
  state: {
    roles: [],
    role_count: 0,
    role: {},
    verify_role: {}
  },
  mutations: {
    setRoles (state, payload) {
      state.roles = payload.roles
      state.role_count = payload.role_count
    },
    setRole (state, payload) {
      state.role = payload.role
    },
    setVerifyRole (state, payload) {
      state.verify_role = payload.verify_role
    }
  },
  actions: {
    loadRoles (context, { skip, take }) {
      if(context.getters.isSuper || context.getters.isAdmin) {
        return loadRoles(context.getters.token, skip, take)
          .then(response => context.commit('setRoles', response.data))
      }
    },
    loadRole (context, { uid }) {
      if(context.getters.isSuper) {
        return loadRole(context.getters.token, uid)
          .then(response => context.commit('setRole', response.data))

      }
    },
    newRole (context) {
      if(context.getters.isSuper) {
        context.commit('setRole', { 'role': {
          name: '',
          is_disabled: true,
          is_admin: false,
          is_super: false
        }})
      }
    },
    saveRole (context, { role }) {
      if(context.getters.isSuper) {
        if(role.uid) {
          return updateRole(context.getters.token, role)
            .then(response => context.commit('setRole', response.data))
        } else {
          return addRole(context.getters.token, role)
            .then(response => context.commit('setRole', response.data))
        }
      }
    },
    verifyRole (context,  { role }) {
      if(context.getters.isSuper) {
          return verifyRole(context.getters.token, role)
            .then(response => context.commit('setVerifyRole', response.data))
      }
    },
    deleteRole (context, { uid }) {
      if(context.getters.isSuper) {
        return deleteRole(context.getters.token, uid)
      }
    },
  }
}

