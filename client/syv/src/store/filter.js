import { loadMembranes, loadMembraneConfigTemplate } from '@/api/filter'

export default {
  state: {
    membranes: [],
    membrane_config_templates: {}
  },
  mutations: {
    setMembranes (state, payload) {
      state.membranes = payload.membranes
    },
    setMembraneConfigTemplate (state, payload) {
      if(!(payload.pipe_name in state.membrane_config_templates)) {
        state.membrane_config_templates[payload.pipe_name] = []
      }

      state.membrane_config_templates[payload.pipe_name][payload.filter_idx] = payload.config_template
    },
    clearMembraneConfigTemplates (state) {
      state.membrane_config_templates = {}
    },    
  },
  actions: {
    loadMembranes (context) {
      if(context.getters.isAdmin) {
        return loadMembranes(context.getters.token)
          .then(response => context.commit('setMembranes', response.data))
      }
    },
    loadMembraneConfigTemplate (context, { pipe, filter, filter_idx }) {
      if(context.getters.isAdmin) {
        return loadMembraneConfigTemplate(context.getters.token, pipe, filter)
          .then(response => {
            let payload = response.data
            payload.pipe_name = pipe.name
            payload.filter_idx = filter_idx
            context.commit('setMembraneConfigTemplate', payload)
          })
      }
    },
    clearMembraneConfigTemplates (context) {
      context.commit('clearMembraneConfigTemplates')
    },
  }
}