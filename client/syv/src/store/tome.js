import {
  loadTomes, loadTome, addTome,
  updateTome, verifyTome, deleteTome
} from '@/api/tome'

export default {
  state: {
    tomes: [],
    tome_count: 0,
    tome: {},
    verify_tome: {}
  },
  mutations: {
    setTomes (state, payload) {
      state.tomes = payload.tomes
      state.tome_count = payload.tome_count
    },
    setTome (state, payload) {
      state.tome = payload.tome
    },
    setVerifyTome (state, payload) {
      state.verify_tome = payload.verify_tome
    }
  },
  actions: {
    loadTomes (context, { skip, take }) {
      if(context.getters.isAdmin) {
        return loadTomes(context.getters.token, skip, take)
          .then(response => context.commit('setTomes', response.data))
      }
    },
    loadTome (context, { uid }) {
      if(context.getters.isAdmin) {
        return loadTome(context.getters.token, uid)
          .then(response => context.commit('setTome', response.data))
      }
    },
    newTome (context) {
      if(context.getters.isAdmin) {
        context.commit('setTome', { 'tome': {
          name: '',
          entries: [],
          entry_count: 0
        }})
      }
    },
    saveTome (context, { tome }) {
      if(context.getters.isAdmin) {
        if(tome.uid) {
          return updateTome(context.getters.token, tome)
            .then(response => context.commit('setTome', response.data))
        } else {
          return addTome(context.getters.token, tome)
            .then(response => context.commit('setTome', response.data))
        }
      }
    },
    verifyTome (context, { tome }) {
      if(context.getters.isAdmin) {
        return verifyTome(context.getters.token, tome)
          .then(response => context.commit('setVerifyTome', response.data))
      }
    },
    deleteTome (context, { uid }) {
      if(context.getters.isAdmin) {
        return deleteTome(context.getters.token, uid)
      }
    }
  }
}
