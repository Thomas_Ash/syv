import Vue from 'vue'

import {
  loadCycles, loadCycle, addCycle,
  commitCycle, cancelCycle, loadCycleDrops,
  addCycleDrops, addCycleRecordChunk, 
  addCycleConfig, freezeCycle, expireCycle
} from '@/api/cycle'

import { loadTome } from '@/api/tome'

export default {
  state: {
    cycle: {},
    cycles: [],
    cycle_count: 0,
    drops: {},
    tomes: {}
  },
  mutations: {
    setCycle (state, payload) {
      state.cycle = payload.cycle
    },
    setCycles (state, payload) {
      state.cycles = payload.cycles
      state.cycle_count = payload.cycle_count
    },
    clearCycleDrops(state) {
      state.drops = {}
    },
    setCycleDrops (state, payload) {
      Vue.set(state.drops, payload.pipe_name, payload.drops)
    },
    clearCycleTomes (state) {
      state.tomes = {}
    },
    setCycleTome (state, payload) {
      Vue.set(state.tomes, payload.tome.uid, payload.tome)
    }
  },
  actions: {
    loadCycles (context, { skip, take, pump_uid, user_uid }) {
      return loadCycles(context.getters.token, skip, take, pump_uid, user_uid)
        .then(response => context.commit('setCycles', response.data))
    },
    clearCycles (context) {
      context.commit('setCycles', {
        cycles: [],
        cycle_count: 0
      })
    },
    loadCycle (context, { uid }) {
      return loadCycle(context.getters.token, uid)
        .then(response => context.commit('setCycle', response.data))
    },
    openCycle (context, { cycle }) {
      return addCycle(context.getters.token, cycle)
        .then(response => context.commit('setCycle', response.data))
    },
    commitCycle (context, { uid }) {
      return commitCycle(context.getters.token, uid)
        .then(response => context.commit('setCycle', response.data))
    },
    cancelCycle (context, { uid }) {
      return cancelCycle(context.getters.token, uid)
        .then(response => context.commit('setCycle', response.data))
    },
    clearCycleDrops(context) {
      context.commit('clearCycleDrops')
    },
    loadCycleDrops (context, { cycle, pipe_name }) {
      return loadCycleDrops(context.getters.token, cycle.uid, cycle.drop_uids[pipe_name])
        .then(response => context.commit('setCycleDrops', {
              pipe_name: pipe_name,
              drops: response.data.drops
        }))             
    },
    addCycleDrops (context, { uid, drops }) {
      return addCycleDrops(context.getters.token, uid, drops)
    },
    addCycleRecordChunk (context, { uid, record_name, chunk, fileName, chunkStart, chunkEnd, fileSize }) {
      return addCycleRecordChunk(context.getters.token, uid, record_name, chunk, fileName, chunkStart, chunkEnd, fileSize)
    },
    addCycleConfig (context, { uid, config }) {
      return addCycleConfig(context.getters.token, uid, config)
    },    
    clearCycleTomes (context) {
      context.commit('clearCycleTomes')
    },
    loadCycleTome (context, { uid }) {
      return loadTome(context.getters.token, uid)
        .then(response => context.commit('setCycleTome', response.data))
    },
    freezeCycle (context, { uid }) {
      return freezeCycle(context.getters.token, uid)
    },
    expireCycle (context, { uid }) {
      return expireCycle(context.getters.token, uid)
    }
  }
}
