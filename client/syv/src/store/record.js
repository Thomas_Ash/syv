import { downloadRecordChunk } from '@/api/record'

export default {
  state: {
    download: null
  },
  mutations: {
    setDownload(state, payload) {
      if(state.download) {
        state.download = new Blob([state.download, payload.download])
      } else {
        state.download = new Blob([payload.download])
      }
    },
    clearDownload(state) {
      state.download = null
    }
  },
  actions: {
    downloadRecordChunk(context, { uid, offset, length }) {
      return downloadRecordChunk(context.getters.token, uid, offset, length)
        .then(response => context.commit('setDownload', { download: response.data }))
    },
    clearDownload(context) {
      context.commit('clearDownload')
    }
  }
}