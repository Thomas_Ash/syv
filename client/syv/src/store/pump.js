import {
  loadPumps, loadPump, addPump,
  updatePump, verifyPump, deletePump, loadIcons,
  loadExecutablePumps, loadEngines, 
  loadEngineConfigTemplate, activatePump,
  deactivatePump, clearPump, applyConstantArray
} from '@/api/pump'

import Vue from 'vue'

export default {
  state: {
    pumps: [],
    pump_count: 0,
    pump: {},
    verify_pump: {},
    icons: [],
    executable_pumps: [],
    engines: [],
    engine_config_template: {}
  },
  mutations: {
    setPumps (state, payload) {
      state.pumps = payload.pumps
      state.pump_count = payload.pump_count
    },
    setPump (state, payload) {
      state.pump = payload.pump
    },
    setVerifyPump (state, payload) {
      state.verify_pump = payload.verify_pump
    },
    setIcons (state, payload) {
      state.icons = payload.icons
    },
    setExecutablePumps (state, payload) {
      state.executable_pumps = payload.pumps
    },
    setEngines (state, payload) {
      state.engines = payload.engines
    },
    setEngineConfigTemplate (state, payload) {
      state.engine_config_template = payload.config_template
    },
    setConstantArray (state, payload) {
      Vue.set(state.pump.constant_arrays, payload.field_key, payload.constant_array)
    }
  },
  actions: {
    loadPumps (context, { skip, take }) {
      if(context.getters.isAdmin) {
        return loadPumps(context.getters.token, skip, take)
          .then(response => context.commit('setPumps', response.data))
      }
    },
    loadPump (context, { uid }) {
      if(context.getters.isAdmin) {
        return loadPump(context.getters.token, uid)
          .then(response => context.commit('setPump', response.data))
      }
    },
    newPump (context) {
      if(context.getters.isAdmin) {
        context.commit('setPump', { 'pump': {
          name: '',
          display_name: '',
          fa_code: '',
          is_disabled: true,
          role_uids: [],
          pipes: [],
          engine_code: '',
          engine_config: {},
          constant_arrays: {}
        }})
      }
    },
    savePump (context, { pump }) {
      if(context.getters.isAdmin) {
        if(pump.uid) {
          return updatePump(context.getters.token, pump)
            .then(response => context.commit('setPump', response.data))
        } else {
          return addPump(context.getters.token, pump)
            .then(response => context.commit('setPump', response.data))
        }
      }
    },
    verifyPump (context, { pump }) {
      if(context.getters.isAdmin) {
        return verifyPump(context.getters.token, pump)
          .then(response => context.commit('setVerifyPump', response.data))
      }
    },
    deletePump (context, { uid }) {
      if(context.getters.isAdmin) {
        return deletePump(context.getters.token, uid)
      }
    },
    loadIcons (context) {
      if(context.getters.isAdmin) {
        return loadIcons(context.getters.token)
          .then(response => context.commit('setIcons', response.data))
      }
    },
    loadExecutablePumps (context) {
      return loadExecutablePumps(context.getters.token)
        .then(response => context.commit('setExecutablePumps', response.data))
    },
    loadEngines (context) {
      if(context.getters.isAdmin) {
        return loadEngines(context.getters.token)
          .then(response => context.commit('setEngines', response.data))
      }
    },
    loadEngineConfigTemplate (context, { pump }) {
      if(context.getters.isAdmin) {
        return loadEngineConfigTemplate(context.getters.token, pump)
          .then(response => context.commit('setEngineConfigTemplate', response.data))
      }
    },
    activatePump (context, { uid }) {
      if(context.getters.isAdmin) {
        return activatePump(context.getters.token, uid)
      }
    },
    deactivatePump (context, { uid }) {
      if(context.getters.isAdmin) {
        return deactivatePump(context.getters.token, uid)
      }
    },
    clearPump (context, { uid }) {
      if(context.getters.isAdmin) {
        return clearPump(context.getters.token, uid)
      }
    },
    applyConstantArray (context, { field_key, constant_array }) {
      if(context.getters.isAdmin) {
        return applyConstantArray(context.getters.token, constant_array)
          .then(response => context.commit('setConstantArray', { field_key: field_key, constant_array: response.data.constant_array}))
      }
    }
  }
}
