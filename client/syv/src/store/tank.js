import {
  loadTanks, loadTank, addTank,
  updateTank, verifyTank, deleteTank,
  loadTankDrops, deleteTankDrops
} from '@/api/tank'

export default {
  state: {
    tanks: [],
    tank_count: 0,
    tank: {},
    verify_tank: {},
    drops: [],
    drop_count: 0
  },
  mutations: {
    setTanks (state, payload) {
      state.tanks = payload.tanks
      state.tank_count = payload.tank_count
    },
    setTank (state, payload) {
      state.tank = payload.tank
    },
    setVerifyTank (state, payload) {
      state.verify_tank = payload.verify_tank
    },
    setTankDrops (state, payload) {
      state.drops = payload.drops
      state.drop_count = payload.drop_count
    },    
  },
  actions: {
    loadTanks (context, { skip, take }) {
      if(context.getters.isAdmin) {
        return loadTanks(context.getters.token, skip, take)
          .then(response => context.commit('setTanks', response.data))
      }
    },
    loadTank (context, { uid }) {
      if(context.getters.isAdmin) {
        return loadTank(context.getters.token, uid)
          .then(response => context.commit('setTank', response.data))
      }
    },
    newTank (context) {
      if(context.getters.isAdmin) {
        context.commit('setTank', { 'tank': {
          name: '',
          arrayspecs: [],
          drop_count: 0
        }})
      }
    },
    saveTank (context, { tank }) {
      if(context.getters.isAdmin) {
        if(tank.uid) {
          return updateTank(context.getters.token, tank)
            .then(response => context.commit('setTank', response.data))
        } else {
          return addTank(context.getters.token, tank)
            .then(response => context.commit('setTank', response.data))
        }
      }
    },
    verifyTank (context, { tank }) {
      if(context.getters.isAdmin) {
        return verifyTank(context.getters.token, tank)
          .then(response => context.commit('setVerifyTank', response.data))
      }
    },
    deleteTank (context, { uid }) {
      if(context.getters.isAdmin) {
        return deleteTank(context.getters.token, uid)
      }
    },
    loadTankDrops(context, { uid, skip, take }) {
      if(context.getters.isAdmin) {
        return loadTankDrops(context.getters.token, uid, skip, take)
          .then(response => context.commit('setTankDrops', response.data))
      }      
    },
    clearTankDrops(context) {
      context.commit('setTankDrops', {
        'drops': [],
        'drop_count': 0
      })
    },
    deleteTankDrops (context, { uid }) {
      if(context.getters.isAdmin) {
        return deleteTankDrops(context.getters.token, uid)
          .then(response => context.commit('setTankDrops', response.data))
      }
    }    
  }
}
