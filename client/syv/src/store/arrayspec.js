import { 
  loadDtypes, loadFaces, loadFaceConfigTemplate, 
  loadBlank, loadStats, loadArray 
} from '@/api/arrayspec'

export default {
  state: {
    dtypes: [],
    faces: [],
    face_config_templates: [],
    blanks: {},
    stats: [],
    stat_count: 0,
    array: null
  },
  mutations: {
    setDtypes (state, payload) {
      state.dtypes = payload.dtypes
    },
    setFaces (state, payload) {
      state.faces = payload.faces
    },
    setFaceConfigTemplate (state, payload) {
      state.face_config_templates[payload.arrayspec_idx] = payload.config_template
    },
    clearFaceConfigTemplates (state) {
      state.face_config_templates = []
    },
    setBlank (state, payload) {
      state.blanks[payload.name] = payload.data
    },
    clearBlanks(state) {
      state.blanks = {}
    },
    setStats(state, payload) {
      state.stats = payload.stats
      state.stat_count = payload.stat_count
    },
    setArray(state, payload) {
      state.array = payload.array
    }
  },
  actions: {
    loadDtypes (context) {
      if(context.getters.isAdmin) {
        return loadDtypes(context.getters.token)
          .then(response => context.commit('setDtypes', response.data))
      }
    },
    loadFaces (context) {
      if(context.getters.isAdmin) {
        return loadFaces(context.getters.token)
          .then(response => context.commit('setFaces', response.data))
      }
    },
    loadFaceConfigTemplate (context, { arrayspec, arrayspec_idx }) {
      if(context.getters.isAdmin) {
        return loadFaceConfigTemplate(context.getters.token, arrayspec)
          .then(response => {
            let payload = response.data
            payload.arrayspec_idx = arrayspec_idx
            context.commit('setFaceConfigTemplate', payload)
          })
      }
    },
    clearFaceConfigTemplates (context) {
      context.commit('clearFaceConfigTemplates')
    },
    loadBlank (context, { name, uid }) {
      return loadBlank(context.getters.token, uid)
        .then(response => {
          let payload = {
            name: name,
            data: response.data
          }
          context.commit('setBlank', payload)
        })
    },
    clearBlanks (context) {
      context.commit('clearBlanks')
    },
    loadStats (context, { uid, skip, take }) {
      return loadStats(context.getters.token, uid, skip, take)
        .then(response => context.commit('setStats', response.data))
    },
    loadArray (context, { uid, hash }) {
      return loadArray(context.getters.token, uid, hash)
        .then(response => context.commit('setArray', response.data))
    }
  }
}
