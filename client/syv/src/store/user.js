import {
  loadUsers, loadUser, addUser,
  updateUser, verifyUser, deleteUser, 
  loadUserChangePassword, updateUserChangePassword
} from '@/api/user'

export default {
  state: {
    users: [],
    user_count: 0,
    user: {},
    user_change_password: {},
    verify_user: {}
  },
  mutations: {
    setUsers (state, payload) {
      state.users = payload.users
      state.user_count = payload.user_count
    },
    setUser (state, payload) {
      state.user = payload.user
    },
    setUserChangePassword (state, payload) {
      state.user_change_password = payload.user
    },
    setVerifyUser (state, payload) {
      state.verify_user = payload.verify_user
    }
  },
  actions: {
    loadUsers (context, { skip, take }) {
      if(context.getters.isSuper || context.getters.isAdmin) {
        return loadUsers(context.getters.token, skip, take)
          .then(response => context.commit('setUsers', response.data))

      }
    },
    loadUser (context, { uid }) {
      if(context.getters.isSuper) {
        return loadUser(context.getters.token, uid)
          .then(response => context.commit('setUser', response.data))
      }
    },
    newUser (context) {
      if(context.getters.isSuper) {
        context.commit('setUser', { 'user': {
          name: '',
          role_uids: [],
          is_disabled: true,
          request_limit: null,
          request_timeout: null
        }})
      }
    },
    saveUser (context, { user }) {
      if(context.getters.isSuper) {
        if(user.uid) {
          return updateUser(context.getters.token, user)
            .then(response => context.commit('setUser', response.data))
        } else {
          return addUser(context.getters.token, user)
            .then(response => context.commit('setUser', response.data))
        }
      }
    },
    verifyUser (context, { user }) {
      if(context.getters.isSuper) {
        return verifyUser(context.getters.token, user)
          .then(response => context.commit('setVerifyUser', response.data))
      }
    },
    deleteUser (context, { uid }) {
      if(context.getters.isSuper) {
        return deleteUser(context.getters.token, uid)
      }
    },
    loadUserChangePassword (context, { uid }) {
      if(context.getters.isSuper || (context.getters.userUid === uid)) {
        return loadUserChangePassword(context.getters.token, uid)
          .then(response => context.commit('setUserChangePassword', response.data))
      }
    },
    saveUserChangePassword (context, { user }) {
      if(context.getters.isSuper || (context.getters.userUid === user.uid)) {
        return updateUserChangePassword(context.getters.token, user)
          .then(response => context.commit('setUserChangePassword', response.data))
      }
    },
    clearUserChangePassword (context) {
      context.commit('setUserChangePassword', {
        user: {
          old_password: '',
          new_password: ''
        }
      })
    }
  }
}
