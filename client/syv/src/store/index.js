import Vue from 'vue'
import Vuex from 'vuex'

import { isValidJwt } from '@/utils'
import { authenticate, renew } from '@/api'
import { loadInfo } from '@/api/info'
import userStore from '@/store/user'
import roleStore from '@/store/role'
import tomeStore from '@/store/tome'
import tankStore from '@/store/tank'
import arraySpecStore from '@/store/arrayspec'
import pumpStore from '@/store/pump'
import cycleStore from '@/store/cycle'
import recordStore from '@/store/record'
import filterStore from '@/store/filter'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    identity: {},
    messages: [],
    next_message_uid: 0,
    pending_messages: [],
    dirty: false,
    tab: null,
    info: {}
  },
  mutations: {
    setIdentity (state, payload) {
      if(payload.identity && payload.identity.token) {
        state.identity = payload.identity
        localStorage.setItem('token', payload.identity.token)
      } else {
        state.identity = {}
        localStorage.removeItem('token')
      }
    },
    setMessages (state, payload) {
      state.messages = payload.messages
      state.next_message_uid = payload.next_message_uid
    },
    setPendingMessages (state, payload) {
      state.pending_messages = payload.messages
    },
    setDirty (state, payload) {
      state.dirty = payload.dirty
    },
    setTab (state, payload) {
      state.tab = payload.tab
    },
    setInfo (state, payload) {
      state.info = payload.info
    }
  },
  actions: {
    // TODO: fix method signature
    authenticate (context, credentials) {
      return authenticate(credentials)
        .then(response => context.commit('setIdentity', {
          identity: {
            user: response.data.user,
            token: response.data.token,
          }
        }))
        .catch(error => {
          console.log(error)
        })
    },
    renew(context) {
      return renew(context.getters.token)
        .then(response => context.commit('setIdentity', {
          identity: {
            user: response.data.user,
            token: response.data.token,
          }
        }))
        .catch(error => {
          console.log(error)
        })
    },
    logout (context) {
      context.commit('setIdentity', {})
    },
    restore (context, { token }) {
      if(token && isValidJwt(token)) {
        return renew(token)
          .then(response => context.commit('setIdentity', {
            identity: {
              user: response.data.user,
              token: response.data.token,
            }
          }))
          .catch(error => {
            console.log(error)
          })
      } else {
        return new Promise((resolve) => resolve())
      }
    },
    addMessages (context, { messages }) {
      let payload = {
        messages: context.state.messages,
        next_message_uid: context.state.next_message_uid
      }
      messages.forEach((message) => {
        payload.messages.push({
          uid: payload.next_message_uid,
          message: message
        })
        payload.next_message_uid++
      })
      context.commit('setMessages', payload)
    },
    addPendingMessages(context, { messages }) {
      let payload = {
        messages: context.state.pending_messages.concat(messages)
      }
      context.commit('setPendingMessages', payload)
    },
    showPendingMessages(context) {
      let payload = {
        messages: context.state.messages,
        next_message_uid: context.state.next_message_uid
      }
      context.state.pending_messages.forEach((message) => {
        payload.messages.push({
          uid: payload.next_message_uid,
          message: message
        })
        payload.next_message_uid++
      })
      context.commit('setMessages', payload)   
      context.commit('setPendingMessages', { messages: [] })   
    },
    clearMessage (context, { uid }) {
      let payload = {
        messages: context.state.messages.filter(m => m.uid != uid),
        next_message_uid: context.state.next_message_uid
      }
      context.commit('setMessages', payload)
    },
    clearMessages (context) {
      context.commit('setMessages', {
        messages: [],
        next_message_uid: 0
      })
    },
    touch (context) {
      context.commit('setDirty', {
        dirty: true
      })
    },
    reset (context) {
      context.commit('setDirty', {
        dirty: false
      })
    },
    switchTab (context, { tab }) {
      context.commit('setTab', {
        tab: tab
      })
    },
    loadInfo (context) {
      return loadInfo(context.getters.token)
        .then((response) => context.commit('setInfo', response.data))
    }
  },
  getters: {
    isLoggedIn: state => {
      if(!(state.identity))
        return false
      if(!(state.identity.token))
        return false
      return isValidJwt(state.identity.token)
    },
    token: state => {
      if(!(state.identity))
        return null
      if(!(state.identity.token))
        return null
      return state.identity.token
    },
    isAdmin: (state, getters) => {
      if(!getters.isLoggedIn)
        return false
      return state.identity.user.roles.some(
        role => role.is_admin && !role.is_disabled
      )
    },
    isSuper: (state, getters) => {
      if(!getters.isLoggedIn)
        return false
      return state.identity.user.roles.some(
        role => role.is_super && !role.is_disabled
      )
    },
    userUid: (state, getters) => {
      if(!getters.isLoggedIn)
        return false
      return state.identity.user.uid
    },
    userName: (state, getters) => {
      if(!getters.isLoggedIn)
        return false
      return state.identity.user.name
    },
    isDirty: state => {
      return state.dirty
    },
    currentTab: state => {
      return state.tab
    }
  },
  modules: {
    user: userStore,
    role: roleStore,
    tome: tomeStore,
    tank: tankStore,
    arrayspec: arraySpecStore,
    pump: pumpStore,
    cycle: cycleStore,
    record: recordStore,
    filter: filterStore
  }
})
