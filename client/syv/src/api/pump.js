import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadPumps (token, skip, take) {
  return axios.get(
    `${API_URL}/pump`, {
      headers: authHeaders(token),
      params: { skip: skip, take: take }
    }
  )
}

export function loadPump (token, uid) {
  return axios.get(`${API_URL}/pump/${uid}`, { headers: authHeaders(token) })
}

export function addPump (token, pump) {
  return axios.post(`${API_URL}/pump`, { pump: pump }, { headers: authHeaders(token) })
}

export function verifyPump (token, pump) {
  return axios.post(`${API_URL}/pump/verify`, { pump: pump }, { headers: authHeaders(token) })
}

export function updatePump (token, pump) {
  return axios.put(`${API_URL}/pump/${pump.uid}`, { pump: pump }, { headers: authHeaders(token) })
}

export function deletePump (token, uid) {
  return axios.delete(`${API_URL}/pump/${uid}`, { headers: authHeaders(token) })
}

export function loadIcons (token) {
  return axios.get(`${API_URL}/pump/icon`, { headers: authHeaders(token) })
}

export function loadExecutablePumps (token) {
  return axios.get(`${API_URL}/pump/exec`, { headers: authHeaders(token) })
}

export function loadEngines (token) {
  return axios.get(`${API_URL}/pump/engine`, { headers: authHeaders(token) })
}

export function loadEngineConfigTemplate (token, pump) {
  return axios.post(`${API_URL}/pump/engine`, { pump: pump }, { headers: authHeaders(token) }) 
}

export function activatePump (token, uid) {
  return axios.put(`${API_URL}/pump/${uid}/status`, { is_disabled: false }, { headers: authHeaders(token) })
}

export function deactivatePump (token, uid) {
  return axios.put(`${API_URL}/pump/${uid}/status`, { is_disabled: true }, { headers: authHeaders(token) })
}

export function clearPump (token, uid) {
  return axios.put(`${API_URL}/pump/${uid}/clear`, {}, { headers: authHeaders(token) })
}

export function applyConstantArray (token, constant_array) {
  return axios.put(`${API_URL}/pump/array`, { constant_array: constant_array }, { headers: authHeaders(token) })
}