import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadTanks (token, skip, take) {
  return axios.get(
    `${API_URL}/tank`, {
      headers: authHeaders(token),
      params: { skip: skip, take: take }
    }
  )
}

export function loadTank (token, uid) {
  return axios.get(`${API_URL}/tank/${uid}`, { headers: authHeaders(token) })
}

export function addTank (token, tank) {
  return axios.post(`${API_URL}/tank`, { tank: tank }, { headers: authHeaders(token) })
}

export function verifyTank (token, tank) {
  return axios.post(`${API_URL}/tank/verify`, { tank: tank }, { headers: authHeaders(token) })
}

export function updateTank (token, tank) {
  return axios.put(`${API_URL}/tank/${tank.uid}`, { tank: tank }, { headers: authHeaders(token) })
}

export function deleteTank (token, uid) {
  return axios.delete(`${API_URL}/tank/${uid}`, { headers: authHeaders(token) })
}

export function loadTankDrops (token, uid, skip, take) {
  return axios.get(
    `${API_URL}/tank/${uid}/drop`, {
      headers: authHeaders(token),
      params: { skip: skip, take: take }
    }
  )
}


export function deleteTankDrops (token, uid) {
  return axios.delete(`${API_URL}/tank/${uid}/drop`, { headers: authHeaders(token) })
}
