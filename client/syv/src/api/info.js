import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadInfo (token) {
  return axios.get(`${API_URL}/info`, { headers: authHeaders(token) })
}
