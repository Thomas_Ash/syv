import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadRoles (token, skip, take) {
  return axios.get(
    `${API_URL}/role`,
    {
      headers: authHeaders(token),
      params: { skip: skip, take: take }
    }
  )
}

export function loadRole (token, uid) {
  return axios.get(`${API_URL}/role/${uid}`, { headers: authHeaders(token) })
}

export function addRole (token, role) {
  return axios.post(`${API_URL}/role`, { role: role }, { headers: authHeaders(token) })
}

export function verifyRole (token, role) {
  return axios.post(`${API_URL}/role/verify`, { role: role }, { headers: authHeaders(token) })
}

export function updateRole (token, role) {
  return axios.put(`${API_URL}/role/${role.uid}`, { role: role }, { headers: authHeaders(token) })
}

export function deleteRole (token, uid) {
  return axios.delete(`${API_URL}/role/${uid}`, { headers: authHeaders(token) })
}
