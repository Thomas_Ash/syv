import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadDtypes (token) {
  return axios.get(`${API_URL}/arrayspec/dtype`, { headers: authHeaders(token) })
}

export function loadFaces (token) {
  return axios.get(`${API_URL}/arrayspec/face`, { headers: authHeaders(token) })
}

export function loadFaceConfigTemplate (token, arrayspec) {
  return axios.post(`${API_URL}/arrayspec/face`, { arrayspec: arrayspec }, { headers: authHeaders(token) }) 
}

export function loadBlank (token, uid) { 
  return axios.get(`${API_URL}/arrayspec/${uid}/blank`, { 
    headers: authHeaders(token), 
    params: { face_mode: 'web' } 
  }) 
}

export function loadStats (token, uid, skip, take) {
  return axios.get(`${API_URL}/arrayspec/${uid}/stats`, { 
    headers: authHeaders(token),
    params: { skip: skip, take: take } 
  })
}

export function loadArray (token, uid, hash) {
  return axios.get(`${API_URL}/arrayspec/${uid}/array?data_hash=${hash}`, { headers: authHeaders(token) })
}
