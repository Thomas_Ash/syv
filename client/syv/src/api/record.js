import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function downloadRecordChunk (token, uid, offset, length) {
  const params = { offset: offset, length: length }
  return axios({
    url: `${API_URL}/record/${uid}`, 
    method: 'get',
    responseType: 'blob',
    headers: authHeaders(token),
    params: params
  })
}
