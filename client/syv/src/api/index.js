import axios from 'axios'

const API_URL = process.env.VUE_APP_API_URL

export function authHeaders(token) {
  return {
    'Authorization': `Bearer ${token}`
  }
}

export function authenticate (credentials) {
  return axios.post(`${API_URL}/login`, credentials)
}

export function renew (token) {
  return axios.post(`${API_URL}/renew`, {}, { headers: authHeaders(token) })
}
