import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadTomes (token, skip, take) {
  return axios.get(
    `${API_URL}/tome`, {
      headers: authHeaders(token),
      params: { skip: skip, take: take }
    }
  )
}

export function loadTome (token, uid) {
  return axios.get(`${API_URL}/tome/${uid}`, { headers: authHeaders(token) })
}

export function addTome (token, tome) {
  return axios.post(`${API_URL}/tome`, { tome: tome }, { headers: authHeaders(token) })
}

export function verifyTome (token, tome) {
  return axios.post(`${API_URL}/tome/verify`, { tome: tome }, { headers: authHeaders(token) })
}

export function updateTome (token, tome) {
  return axios.put(`${API_URL}/tome/${tome.uid}`, { tome: tome }, { headers: authHeaders(token) })
}

export function deleteTome (token, uid) {
  return axios.delete(`${API_URL}/tome/${uid}`, { headers: authHeaders(token) })
}
