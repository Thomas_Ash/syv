import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadMembranes (token) {
  return axios.get(`${API_URL}/filter/membrane`, { headers: authHeaders(token) })
}

export function loadMembraneConfigTemplate (token, pipe, filter) {
  return axios.post(`${API_URL}/filter/membrane`, { pipe: pipe, filter: filter }, { headers: authHeaders(token) }) 
}
