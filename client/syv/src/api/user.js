import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadUsers (token, skip, take) {
  return axios.get(
    `${API_URL}/user`, {
      headers: authHeaders(token),
      params: { skip: skip, take: take }
    }
  )
}

export function loadUser (token, uid) {
  return axios.get(`${API_URL}/user/${uid}`, { headers: authHeaders(token) })
}

export function addUser (token, user) {
  return axios.post(`${API_URL}/user`, { user: user }, { headers: authHeaders(token) })
}

export function verifyUser (token, user) {
  return axios.post(`${API_URL}/user/verify`, { user: user }, { headers: authHeaders(token) })
}

export function updateUser (token, user) {
  return axios.put(`${API_URL}/user/${user.uid}`, { user: user }, { headers: authHeaders(token) })
}

export function deleteUser (token, uid) {
  return axios.delete(`${API_URL}/user/${uid}`, { headers: authHeaders(token) })
}

export function loadUserChangePassword (token, uid) {
  return axios.get(`${API_URL}/user/${uid}/change_password`, { headers: authHeaders(token) })
}

export function updateUserChangePassword (token, user) {
  return axios.put(`${API_URL}/user/${user.uid}/change_password`, { user: user }, { headers: authHeaders(token) })
}

