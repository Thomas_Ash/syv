import axios from 'axios'
import { authHeaders } from '@/api'

const API_URL = process.env.VUE_APP_API_URL

export function loadCycles (token, skip, take, pump_uid, user_uid) {
  let params = { skip: skip, take: take }
  if(pump_uid) {
    params.pump_uid = pump_uid
  } else {
    params.user_uid = user_uid
  }
  return axios.get(`${API_URL}/cycle`, { headers: authHeaders(token), params: params })
}

export function loadCycle (token, uid) {
  return axios.get(`${API_URL}/cycle/${uid}`, { headers: authHeaders(token) })
}

export function addCycle (token, cycle) {
  return axios.post(`${API_URL}/cycle`, { cycle: cycle }, { headers: authHeaders(token) })
}

export function commitCycle (token, uid) {
  return axios.put(`${API_URL}/cycle/${uid}/commit`, {}, { headers: authHeaders(token) })
}

export function cancelCycle (token, uid) {
  return axios.put(`${API_URL}/cycle/${uid}/cancel`, {}, { headers: authHeaders(token) })
}

export function loadCycleDrops (token, uid, drop_uids) {
  const params = {
    face_mode: 'web',
    drop_uids: drop_uids.join(',')
  }
  return axios.get(`${API_URL}/cycle/${uid}/drop`, { headers: authHeaders(token), params: params })
}

export function addCycleDrops (token, uid, drops) {
  return axios.put(`${API_URL}/cycle/${uid}`, { drops: drops }, { headers: authHeaders(token), params: { face_mode: 'web' } })
}

export function addCycleRecordChunk (token, uid, record_name, chunk, fileName, chunkStart, chunkEnd, fileSize) {
  let form = new FormData()
  form.append(record_name, chunk, fileName)
  form.append('offset', chunkStart)
  let headers = authHeaders(token)
  headers['Content-Type'] = 'multipart/form-data'
  headers['Content-Range'] = `bytes ${chunkStart}-${chunkEnd-1}/${fileSize}`
  return axios.put(`${API_URL}/cycle/${uid}`, form, { headers: headers })
}

export function addCycleConfig (token, uid, config) {
  return axios.put(`${API_URL}/cycle/${uid}`, { config: config }, { headers: authHeaders(token) }) 
}

export function freezeCycle (token, uid) {
  return axios.put(`${API_URL}/cycle/${uid}/freeze`, {}, { headers: authHeaders(token) })
}

export function expireCycle (token, uid) {
  return axios.delete(`${API_URL}/cycle/${uid}`, { headers: authHeaders(token) })
}
