import Roles from '@/views/role/Roles.vue'
import Role from '@/views/role/Role.vue'

export default [
  {
    path: '/role/:which',
    name: 'role',
    component: Role,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: true,
      title: 'Role',
      faCode: 'hat-cowboy',
      showDirty: true,
      tabs: []
    }
  }, {
    path: '/role/',
    name: 'roles',
    component: Roles,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: true,
      title: 'Roles',
      faCode: 'hat-cowboy',
      showDirty: false,
      tabs: []
    }
  }
]
