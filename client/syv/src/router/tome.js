import Tomes from '@/views/tome/Tomes.vue'
import Tome from '@/views/tome/Tome.vue'

export default [
  {
    path: '/tome/:which',
    name: 'tome',
    component: Tome,
    meta: {
      requireLoggedIn: true,
      requireAdmin: true,
      requireSuper: false,
      title: 'Tome',
      faCode: 'book',
      showDirty: true,
      tabs: [
        {
          name: 'tome',
          title: 'Tome',
          faCode: 'book'
        },
        {
          name: 'entries',
          title: 'Entries',
          faCode: 'scroll'
        }
      ]
    }
  }, {
    path: '/tome/',
    name: 'tomes',
    component: Tomes,
    meta: {
      requireLoggedIn: true,
      requireAdmin: true,
      requireSuper: false,
      title: 'Tomes',
      faCode: 'book',
      showDirty: false,
      tabs: []
    }
  }
]
