import Tanks from '@/views/tank/Tanks.vue'
import Tank from '@/views/tank/Tank.vue'

export default [
  {
    path: '/tank/:which',
    name: 'tank',
    component: Tank,
    meta: {
      requireLoggedIn: true,
      requireAdmin: true,
      requireSuper: false,
      title: 'Tank',
      faCode: 'database',
      showDirty: true,
      tabs: [
        {
          name: 'tank',
          title: 'Tank',
          faCode: 'database'
        },
        {
          name: 'drops',
          title: 'Drops',
          faCode: 'tint'
        },
        {
          name: 'stats',
          title: 'Stats',
          faCode: 'th-list'
        }
      ]
    }
  }, {
    path: '/tank/',
    name: 'tanks',
    component: Tanks,
    meta: {
      requireLoggedIn: true,
      requireAdmin: true,
      requireSuper: false,
      title: 'Tanks',
      faCode: 'database',
      showDirty: false,
      tabs: []
    }
  }
]
