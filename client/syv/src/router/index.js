import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import Home from '@/views/Home.vue'
import Login from '@/views/Login.vue'
import Ghost from '@/components/Ghost.vue'
import userRoutes from '@/router/user'
import roleRoutes from '@/router/role'
import tankRoutes from '@/router/tank'
import pumpRoutes from '@/router/pump'
import tomeRoutes from '@/router/tome'
import cycleRoutes from '@/router/cycle'
import aboutRoutes from '@/router/about'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: false,
      title: 'Home',
      faCode: 'home',
      showDirty: false,
      tabs: []
    }
  }, {
    path: '/login/',
    name: 'login',
    component: Login,
    meta: {
      requireLoggedIn: false,
      requireAdmin: false,
      requireSuper: false,
      title: 'Login',
      faCode: 'key',
      showDirty: false,
      tabs: []
    }
  }
].concat(
  userRoutes,
  roleRoutes,
  tankRoutes,
  pumpRoutes,
  tomeRoutes,
  cycleRoutes, 
  aboutRoutes, [
    {
      path: '/not-found/',
      name: 'not-found',
      component: Ghost,
      meta: {
        requireLoggedIn: false,
        requireAdmin: false,
        requireSuper: false,
        title: 'Not Found',
        faCode: 'exclamation-triangle',
        showDirty: false,
        tabs: []
      }
    }, {
      path: '/*',
      redirect: 'not-found'
    }

  ]
)

const router = new VueRouter({ routes })

router.beforeEach((to, from, next) => {
  store.dispatch('restore', {
    token: from.name ? null : localStorage.getItem('token')
  })
   .then(() => {

    if(from.meta.showDirty && store.getters.isDirty) {
      if(!confirm('Are you sure you want to discard your unsaved changes?')) {
        next(false)
        return
      }
    }
    store.dispatch('clearMessages', {})
      .then(() => { store.dispatch('showPendingMessages', {}) })
      .then(() => {
        if(store.getters.isLoggedIn && to.name === 'login') {
          next({name:'home'})
        } else if(!store.getters.isLoggedIn && to.meta.requireLoggedIn) {
          next({name:'login'})
        } else if(!store.getters.isAdmin && to.meta.requireAdmin) {
          next({name:'not-found'})
        } else if(!store.getters.isSuper && to.meta.requireSuper) {
          next({name:'not-found'})
        } else {
          if(to.meta &&
            to.meta.tabs &&
            to.meta.tabs.length > 0) {

            if(!store.getters.currentTab ||
              !to.meta.tabs.some(x => x.name === store.getters.currentTab)) {

              store.dispatch('switchTab', {tab:to.meta.tabs[0].name})
            }
          }
          next()
        }
      })
  })
})

export default router
