import Cycles from '@/views/cycle/Cycles.vue'
import Cycle from '@/views/cycle/Cycle.vue'

export default [
  {
    path: '/cycle/:which',
    name: 'cycle',
    component: Cycle,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: false,
      title: 'Cycle',
      faCode: 'sync',
      showDirty: false,
      tabs: []
    }
  }, {
    path: '/cycle/',
    name: 'cycles',
    component: Cycles,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: false,
      title: 'Cycles',
      faCode: 'sync',
      showDirty: false,
      tabs: []
    }
  }
]
