import Users from '@/views/user/Users.vue'
import User from '@/views/user/User.vue'
import ChangePassword from '@/views/user/ChangePassword.vue'

export default [
  {
    path: '/user/:which/change-password',
    name: 'change-password',
    component: ChangePassword,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: false,
      title: 'Change Password',
      faCode: 'unlock-alt',
      showDirty: false,
      tabs: []
    }
  }, {
    path: '/user/:which',
    name: 'user',
    component: User,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: true,
      title: 'User',
      faCode: 'user',
      showDirty: true,
      tabs: []
    }
  }, {
    path: '/user/',
    name: 'users',
    component: Users,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: true,
      title: 'Users',
      faCode: 'user',
      showDirty: false,
      tabs: []
    }
  }
]
