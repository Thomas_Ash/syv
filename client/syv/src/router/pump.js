import Pumps from '@/views/pump/Pumps.vue'
import Pump from '@/views/pump/Pump.vue'

export default [
  {
    path: '/pump/:which',
    name: 'pump',
    component: Pump,
    meta: {
      requireLoggedIn: true,
      requireAdmin: true,
      requireSuper: false,
      title: 'Pump',
      faCode: 'server',
      showDirty: true,
      tabs: [        
        {
          name: 'pump',
          title: 'Pump',
          faCode: 'server'
        },
        {
          name: 'cycles',
          title: 'Cycles',
          faCode: 'sync'
        }
      ]
    }
  }, {
    path: '/pump/',
    name: 'pumps',
    component: Pumps,
    meta: {
      requireLoggedIn: true,
      requireAdmin: true,
      requireSuper: false,
      title: 'Pumps',
      faCode: 'server',
      showDirty: false,
      tabs: []
    }
  }
]
