import About from '@/views/About.vue'
export default [
  {
    path: '/about/',
    name: 'about',
    component: About,
    meta: {
      requireLoggedIn: true,
      requireAdmin: false,
      requireSuper: false,
      title: 'About',
      faCode: 'info-circle',
      showDirty: false,
      tabs: []
    }
  }
]
