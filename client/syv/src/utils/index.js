export function isValidJwt (jwt) {
  if (!jwt ||
      typeof(jwt) != 'string' ||
      jwt.split('.').length < 3) {
    return false
  }
  const data = JSON.parse(atob(jwt.split('.')[1]))
  const exp = new Date(data.exp * 1000)
  const now = new Date()
  return now < exp
}

const prefixes = ['', 'K', 'M', 'G', 'T']

export function formatByteCount (byteCount) {
  if(!Number.isInteger(byteCount)) {
    return ''
  } else if (byteCount === 0) {
    return '0 B'
  } else {
   var i = parseInt(Math.floor(Math.log(byteCount) / Math.log(1000)), 10);
   return (byteCount / Math.pow(1000, i)).toPrecision(4) + ' ' + prefixes[i] + 'B';  
 }   
}

const chunkSize = 1048576

export function getChunkSize() {
  return chunkSize
}