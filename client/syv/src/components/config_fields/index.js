import BooleanRadioConfigField from '@/components/config_fields/BooleanRadioConfigField'
import ConstantArrayConfigField from '@/components/config_fields/ConstantArrayConfigField'
import CustomSelectConfigField from '@/components/config_fields/CustomSelectConfigField'
import DimSelectConfigField from '@/components/config_fields/DimSelectConfigField'
import MappingSelectConfigField from '@/components/config_fields/MappingSelectConfigField'
import NumberConfigField from '@/components/config_fields/NumberConfigField'
import TextConfigField from '@/components/config_fields/TextConfigField'
import TimespanConfigField from '@/components/config_fields/TimespanConfigField'
import TomeSelectConfigField from '@/components/config_fields/TomeSelectConfigField'
import UserSelectConfigField from '@/components/config_fields/UserSelectConfigField'

export function getConfigField(field_code) {
  if(field_code === 'boolean_radio') {
    return BooleanRadioConfigField
  } else if(field_code === 'custom_select') {
    return CustomSelectConfigField
  } else if(field_code === 'dim_select') {
    return DimSelectConfigField
  } else if(field_code === 'mapping_select') {
    return MappingSelectConfigField
  } else if(field_code === 'number') {
    return NumberConfigField
  } else if(field_code === 'text') {
    return TextConfigField
  } else if(field_code === 'timespan') {
    return TimespanConfigField
  } else if(field_code === 'tome_select') {
    return TomeSelectConfigField
  } else if(field_code === 'user_select') {
    return UserSelectConfigField
  } else if(field_code === 'constant') {
    return ConstantArrayConfigField
  }
}

export function getDefaultValue(field_code) {
  if(field_code === 'boolean_radio') {
    return false
  } else if(field_code === 'custom_select') {
    return ''
  } else if(field_code === 'dim_select') {
    return 0
  } else if(field_code === 'mapping_select') {
    return ''
  } else if(field_code === 'number') {
    return 0
  } else if(field_code === 'text') {
    return ''
  } else if(field_code === 'timespan') {
    return '1|m'
  } else if(field_code === 'tome_select') {
    return ''
  } else if(field_code === 'user_select') {
    return ''
  } else if(field_code === 'constant') {
    return ''
  }
}