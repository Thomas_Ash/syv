/*eslint no-unused-vars: ['error', { 'args': 'none' }]*/

import {
  Validator
} from '@/validators'

export class RoleValidator extends Validator {
  constructor(comp) {
    super(
      comp, {
        name: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.role.name.length > 0
            },
            error: 'Name cannot be blank.'
          }, {
            passes: (comp, idx) => {
              return comp.role.name.length <= 32
            },
            error: 'Name cannot be longer than 32 characters.'
          }]
        }
      },
      'verifyRole',
      (comp) => {
        return {
          role: comp.role
        }
      },
      (comp) => {
        return comp.verify_role.report
      }
    )
  }
}