/*eslint no-unused-vars: ['error', { 'args': 'none' }]*/

import {
  Validator
} from '@/validators'

export class TankValidator extends Validator {

  constructor(comp) {
    super(
      comp, {
        // simple case: rules for single field

        name: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.tank.name.length > 0
            },
            error: 'Name cannot be blank.'
          }, {
            passes: (comp, idx) => {
              return comp.tank.name.length <= 32
            },
            error: 'Name cannot be longer than 32 characters.'
          }]
        },

        // advanced case: many, each with subfields

        arrayspecs: {
          length: (comp, idx) => {
            return comp.tank.arrayspecs.length
          },
          fields: {
            name: {
              length: null,
              rules: [{
                passes: (comp, idx) => {
                  return comp.tank.arrayspecs[idx.arrayspecs].name.length > 0
                },
                error: 'Name cannot be blank.'
              }, {
                passes: (comp, idx) => {
                  return comp.tank.arrayspecs[idx.arrayspecs].name.length <= 32
                },
                error: 'Name cannot be longer than 32 characters.'
              }, {
                passes: (comp, idx) => {
                  return comp.tank.arrayspecs.filter(x => {
                    return x.name === comp.tank.arrayspecs[idx.arrayspecs].name
                  }).length === 1
                },
                error: 'Name is already in use.'
              }]
            },
            dtype: {
              length: null,
              rules: [{
                passes: (comp, idx) => {
                  return comp.dtypes.some(x => { return x.code === comp.tank.arrayspecs[idx.arrayspecs].dtype })
                },
                error: 'Data type is invalid.'
              }]
            },

            // intermediate case: many, no subfields

            dims: {
              length: (comp, idx) => {
                return comp.tank.arrayspecs[idx.arrayspecs].dims.length
              },
              rules: [{
                passes: (comp, idx) => {
                  return comp.tank.arrayspecs[idx.arrayspecs].dims[idx.dims].length > 0
                },
                error: 'Length should be a positive integer.'
              }]
            },

            face_code: {
              length: null,
              rules: [{
                passes: (comp, idx) => {
                  return comp.faces.some(x => x.code === comp.tank.arrayspecs[idx.arrayspecs].face_code)
                },
                error: 'Face code is invalid.'
              }]
            },

            // extra-advanced case: dynamic fields

            face_config: {
              length: null,
              dynamic: (comp, idx) => {

                let fields = {}

                // could add client-side rules here
                // based on type/options in config template

                if(comp.face_config_templates[idx.arrayspecs]) {
                  Object.keys(comp.face_config_templates[idx.arrayspecs]).forEach(key => {
                    fields[key] = {
                      length: null,
                      rules: []
                    }
                  })
                }
                return fields
              }
            }

          }
        }
      },

      // server-side same as before


      'verifyTank',
      (comp) => {
        return {
          tank: comp.tank
        }
      },
      (comp) => {
        return comp.verify_tank.report
      }

      // no more getChildren() needed
    )
  }
}
