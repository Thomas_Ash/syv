/*eslint no-unused-vars: ['error', { 'args': 'none' }]*/

import {
  Validator
} from '@/validators'

export class UserValidator extends Validator {
  constructor(comp) {
    super(
      comp, {
        name: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.user.name.length > 0
            },
            error: 'Name cannot be blank.'
          }, {
            passes: (comp, idx) => {
              return comp.user.name.length <= 32
            },
            error: 'Name cannot be longer than 32 characters.'
          }]
        },
        request_limit: {
          length: null,
          rules: []
        },
        request_timeout: {
          length: null,
          rules: []
        }
      },
      'verifyUser',
      (comp) => {
        return {
          user: comp.user
        }
      },
      (comp) => {
        return comp.verify_user.report
      }
    )
  }
}