/*eslint no-unused-vars: ['error', { 'args': 'none' }]*/

import {
  Validator
} from '@/validators'

export class LoginValidator extends Validator {
  constructor(comp) {
    super(
      comp, {
        name: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.name.length > 0
            },
            error: 'Name cannot be blank.'
          }]
        },
        password: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.password.length > 0
            },
            error: 'Password cannot be blank.'
          }]
        }        
      }
    )
  }
}