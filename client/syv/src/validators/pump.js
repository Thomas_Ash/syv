/*eslint no-unused-vars: ['error', { 'args': 'none' }]*/

import {
  Validator
} from '@/validators'

export class PumpValidator extends Validator {
  constructor(comp) {
    super(
      comp, {
        name: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.pump.name.length > 0
            },
            error: 'Name cannot be blank.'
          }, {
            passes: (comp, idx) => {
              return comp.pump.name.length <= 32
            },
            error: 'Name cannot be longer than 32 characters.'
          }]
        },
        fa_code: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.icons.includes(comp.pump.fa_code)
            },
            error: 'Icon code is invalid.'
          }]
        },

        engine_code: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.engines.some(x => x.code === comp.pump.engine_code)
            },
            error: 'Engine code is invalid.'
          }]
        },

        pipes: {
          length: null,
          fields: {
            in: {
              length: (comp, idx) => {
                return comp.inputPipes.length
              },
              fields: {
                tank_uid: {
                  length: null,
                  rules: [{
                    passes: (comp, idx) => {
                      return comp.tanks.some(x => x.uid === comp.inputPipes[idx.in].tank_uid)
                    },
                    error: 'Tank is invalid.'
                  }]
                },
                drop_limit: {
                  length: null,
                  rules: [{
                    passes: (comp, idx) => {
                      return comp.inputPipes[idx.in].drop_limit > 0
                    },
                    error: 'Drop limit should be a positive integer.'
                  }]
                },
                filters: {
                  length: (comp, idx) => {
                    return comp.inputPipes[idx.in].filters.length
                  },
                  fields: {
                    name: {
                      length: null,
                      rules: [{
                        passes: (comp, idx) => {
                          return comp.inputPipes[idx.in].filters[idx.filters].name.length > 0
                        },
                        error: 'Name cannot be blank.'
                      }, {
                        passes: (comp, idx) => {
                          return comp.inputPipes[idx.in].filters[idx.filters].name.length <= 32
                        },
                        error: 'Name cannot be longer than 32 characters.'
                      }]
                    },
                    membrane_code: {
                      length: null,
                      rules: [{
                        passes: (comp, idx) => {
                          return comp.membranes.some(x => x.code === comp.inputPipes[idx.in].filters[idx.filters].membrane_code)
                        },
                        error: 'Membrane code is invalid.'
                      }]                      
                    },
                    membrane_config: {
                      length: null,
                      dynamic: (comp, idx) => {
                        const pipe_name = comp.inputPipes[idx.in].name
                        let fields = {}

                        if(
                          comp.membrane_config_templates[pipe_name] && 
                          comp.membrane_config_templates[pipe_name][idx.filters]
                        ) {
                          Object.keys(comp.membrane_config_templates[pipe_name][idx.filters]).forEach(key => {
                            fields[key] = {
                              length: null,
                              rules: []
                            }
                          })                          
                        }
                        return fields
                      }
                    }                   
                  }
                }
              }
            },
            out: {
              length: (comp, idx) => {
                return comp.outputPipes.length
              },
              fields: {
                tank_uid: {
                  length: null,
                  rules: [{
                    passes: (comp, idx) => {
                      return comp.tanks.some(x => x.uid === comp.outputPipes[idx.out].tank_uid)
                    },
                    error: 'Tank is invalid.'
                  }]
                },
                filters: {
                  length: (comp, idx) => {
                    return comp.outputPipes[idx.out].filters.length
                  },
                  fields: {
                    name: {
                      length: null,
                      rules: [{
                        passes: (comp, idx) => {
                          return comp.outputPipes[idx.out].filters[idx.filters].name.length > 0
                        },
                        error: 'Name cannot be blank.'
                      }, {
                        passes: (comp, idx) => {
                          return comp.outputPipes[idx.out].filters[idx.filters].name.length <= 32
                        },
                        error: 'Name cannot be longer than 32 characters.'
                      }]
                    },
                    membrane_code: {
                      length: null,
                      rules: [{
                        passes: (comp, idx) => {
                          return comp.membranes.some(x => x.code === comp.outputPipes[idx.out].filters[idx.filters].membrane_code)
                        },
                        error: 'Membrane code is invalid.'
                      }]                      
                    },
                    membrane_config: {
                      length: null,
                      dynamic: (comp, idx) => {
                        const pipe_name = comp.outputPipes[idx.out].name
                        let fields = {}

                        if(
                          comp.membrane_config_templates[pipe_name] &&
                          comp.membrane_config_templates[pipe_name][idx.filters]
                        ) {
                          Object.keys(comp.membrane_config_templates[pipe_name][idx.filters]).forEach(key => {
                            fields[key] = {
                              length: null,
                              rules: []
                            }
                          })                          
                        }
                        return fields
                      }
                    }                   
                  }
                }
              }
            }
          }
        },

        engine_config: {
          length: null,
          dynamic: (comp, idx) => {
            let fields = {}

            // could add client-side rules here
            // based on type/options in config template

            if(comp.engine_config_template) {
              Object.keys(comp.engine_config_template).forEach(key => {
                fields[key] = {
                  length: null,
                  rules: []
                }
              })
            }
            return fields            
          }
        }

      },
      'verifyPump',
      (comp) => {
        return {
          pump: comp.pump
        }
      },
      (comp) => {
        return comp.verify_pump.report
      }
    )
  }
}