/*eslint no-unused-vars: ['error', { 'args': 'none' }]*/

import {
  Validator
} from '@/validators'

export class TomeValidator extends Validator {
  constructor(comp) {
    super(
      comp, {
        name: {
          length: null,
          rules: [{
            passes: (comp, idx) => {
              return comp.tome.name.length > 0
            },
            error: 'Name cannot be blank.'
          }, {
            passes: (comp, idx) => {
              return comp.tome.name.length <= 32
            },
            error: 'Name cannot be longer than 32 characters.'
          }]
        }
      },
      'verifyTome',
      (comp) => {
        return {
          tome: comp.tome
        }
      },
      (comp) => {
        return comp.verify_tome.report
      }
    )
  }
}