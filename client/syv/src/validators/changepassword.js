/*eslint no-unused-vars: ['error', { 'args': 'none' }]*/

import {
  Validator
} from '@/validators'

export class ChangePasswordValidator extends Validator {
  constructor(comp) {
    super(
      comp, {
        old_password: {
          length: null,
          rules: [{
            passes: (comp) => { 
              return comp.isSuper || comp.user.old_password.length > 0 
            },
            error: 'Old password cannot be blank.'
          }]
        },
        new_password: {
          length: null,
          rules: [{
            passes: (comp) => {
              return comp.user.new_password.length >= 8
            },
            error: 'New password should be at least 8 characters in length.'
          }]
        },
        repeat_new_password: {
          length: null,
          rules: [{
            passes: (comp) => {
              return comp.repeat_new_password === comp.user.new_password
            },
            error: 'New passwords do not match.'
          }]
        }
      }
    )
  }
}