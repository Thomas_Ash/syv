import Vue from 'vue'

function validateField(comp, field, idx) {
  if ('rules' in field) {
    return {
      errors: field.rules
        .reduce((errors, rule) => {
          if(rule.passes(comp, idx)) {
            return errors
          } else {
            errors.push(rule.error)
            return errors
          }
        }, [])
    }
  } else if ('fields' in field) {
    return validateFields(comp, field.fields, idx)
  } else if ('dynamic' in field) {
    let fields = field.dynamic(comp, idx)
    return validateFields(comp, fields, idx)
  }
}

function validateFields(comp, fields, idx) {
  let report = {}
  Object.keys(fields).forEach(key => {
    let field = fields[key]
    if (field.length) {
      let length = field.length(comp, idx)
      report[key] = [...Array(length)].map((_, j) => {
        let innerIdx = Object.assign({}, idx)
        innerIdx[key] = j
        return validateField(comp, field, innerIdx)
      })
    } else {
      Vue.set(report, key, validateField(comp, field, idx))
    }
  })
  return report
}

function hasErrors(field) {
  if('errors' in field) {
    return field.errors.length > 0
  } else {
    return anyErrors(field)
  }
}

function anyErrors(fields) {
  let keys = Object.keys(fields)

  for(const i in Object.keys(fields)) {
    let key = keys[i]
    let field = fields[key]
    if(Array.isArray(field)) {
      for(const j in field) {
        if(hasErrors(field[j])) {
          return true
        }
      }
    } else {
      if(hasErrors(field)) {
        return true
      }
    }
  }
  return false
}

export class Validator {

  constructor(comp, fields, action = null, getActionArg = null, getState = null) {
    this.comp = comp
    this.fields = fields
    this.action = action
    this.getActionArg = getActionArg
    this.getState = getState
  }

  refresh() {
    let report = validateFields(this.comp, this.fields, {})

    if(!anyErrors(report) && this.action && this.getActionArg && this.getState) {
      this.comp.$store.dispatch(this.action, this.getActionArg(this.comp))
        .then(() => Vue.set(this, 'report', Object.assign(report, this.getState(this.comp))))
    } else {
      Vue.set(this, 'report', report)
    }
  }

  anyErrors() {
    if(!this.report) {
      return true
    }

    return anyErrors(this.report)
  }
}
