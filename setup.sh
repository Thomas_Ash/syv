#!/bin/bash
source .env
docker-compose build
docker-compose up -d postgres
docker-compose run --rm -u syv --entrypoint "\
    venv/bin/python init_db.py" uwsgi > .syvpw
docker-compose run --rm --no-deps --entrypoint "\
    sh -c 'mkdir -p /etc/letsencrypt/live/${SYV_API_HOST} && \
    openssl req -x509 -nodes -newkey rsa:4096 -days 1 \
    -keyout \"/etc/letsencrypt/live/${SYV_API_HOST}/privkey.pem\" \
    -out \"/etc/letsencrypt/live/${SYV_API_HOST}/fullchain.pem\" \
    -subj \"/CN=localhost\"'" certbot
docker-compose up -d nginx
docker-compose run --rm --entrypoint "\
    sh -c 'rm -Rf /etc/letsencrypt/live/${SYV_API_HOST} && \
    rm -Rf /etc/letsencrypt/archive/${SYV_API_HOST} && \
    rm -Rf /etc/letsencrypt/renewal/${SYV_API_HOST}.conf && \
    certbot certonly --webroot -w /var/www/certbot \
    --email ${CERTBOT_EMAIL} -d ${SYV_API_HOST} \
    --rsa-key-size 4096 --agree-tos --non-interactive --force-renewal'" certbot
docker-compose down
docker-compose up -d
