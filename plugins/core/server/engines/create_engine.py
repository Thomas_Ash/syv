import copy
from typing import Dict, Tuple

from sqlalchemy.orm import Session

from syv.models import Cycle
from syv.services import ArrayService
from syv.exceptions import EngineInternalError
from .base_engine import BaseEngine


class CreateEngine(BaseEngine):

    def get_config_template(self, session: Session) -> Dict:
        config_template = super().get_config_template(session)

        choices = {
            'ask': 'Ask',
            'constant': 'Constant'
        }

        config_template.update(self.get_mappings(session, choices))

        config_template.update({
            'drop_count': {
                'label': 'Drop Count',
                'type': 'number',
                'options': {}
            },
            'drops_per_page': {
                'label': 'Drops Per Page',
                'type': 'number',
                'options': {}
            }
        })

        return config_template

    def validate(self, session: Session) -> Dict:
        report = super().validate(session)

        try:
            drop_count = int(self.pump.engine_config['drop_count'])
            assert drop_count > 0
        except (ValueError, TypeError, AssertionError, KeyError):
            report['engine_config']['drop_count']['errors'].append('Drop count should be a positive integer.')

        try:
            drops_per_page = int(self.pump.engine_config.get('drops_per_page'))
            assert drops_per_page > 0
        except (TypeError, ValueError, AssertionError):
            report['engine_config']['drops_per_page']['errors'].append('Drops per page should be a positive integer.')

        return report

    def needs_client_post(self) -> bool:
        for field in self.pump.engine_config:
            if field.startswith('mapping_') and self.pump.engine_config[field] == 'ask':
                return True

        return False

    def can_commit(self, cycle: Cycle) -> bool:
        if not self.needs_client_post():
            return True

        if 'output' not in cycle.client_drops:
            return False

        ask_arrayspec_names = ['_'.join(x.split('_')[2:]) for x in self.pump.engine_config
                               if x.startswith('mapping_output_') and self.pump.engine_config.get(x) == 'ask']

        for drop in cycle.client_drops['output']:
            if 'arrays' not in drop:
                return False

            if any([x for x in ask_arrayspec_names if x not in drop['arrays']]):
                return False

        return True

    def process(self, cycle: Cycle, session: Session) -> None:

        output_pipe = self.pump.get_pipe('output')
        # can't 'append' to JSON field as with a simple list - so copy, append & reassign
        output_drops = copy.deepcopy(cycle.output_drops)

        array_service = ArrayService(session)

        if self.needs_client_post():
            # derive output_drops from client_drops
            client_drops = cycle.client_drops.get('output')
            drop_count = len(client_drops)

            if type(client_drops) is not list or len(client_drops) != int(self.pump.engine_config['drop_count']):
                raise EngineInternalError

            for drop_idx in range(drop_count):
                client_drop = client_drops[drop_idx]
                output_drop = {}
                for arrayspec in output_pipe.tank.arrayspecs:
                    mapping = self.pump.engine_config[f'mapping_{output_pipe.name}_{arrayspec.name}']
                    if mapping == 'ask':
                        output_drop[arrayspec.name] = client_drop['arrays'][arrayspec.name]
                    elif mapping == 'constant':
                        data_hash = self.pump.engine_config[f'constant_{output_pipe.name}_{arrayspec.name}']
                        output_drop[arrayspec.name] = data_hash
                    elif mapping == 'blank':
                        face_type = arrayspec.get_face_type()
                        face = face_type(arrayspec, session)
                        data_hash = array_service.save(face.load_blank(session), cycle_uid=cycle.uid)
                        output_drop[arrayspec.name] = data_hash
                    elif mapping == 'random':
                        face_type = arrayspec.get_face_type()
                        face = face_type(arrayspec, session)
                        data_hash = array_service.save(face.load_random(session), cycle_uid=cycle.uid)
                        output_drop[arrayspec.name] = data_hash

                output_drops['output'].append(output_drop)
                cycle.progress = min((100 * drop_idx) // drop_count, 99)
                session.commit()
        else:
            # derive output drops from drop_count and constant/blank/random
            drop_count = int(self.pump.engine_config['drop_count'])
            for drop_idx in range(drop_count):
                output_drop = {}
                for arrayspec in output_pipe.tank.arrayspecs:
                    mapping = self.pump.engine_config[f'mapping_{output_pipe.name}_{arrayspec.name}']
                    if mapping == 'constant':
                        data_hash = self.pump.engine_config[f'constant_{output_pipe.name}_{arrayspec.name}']
                        output_drop[arrayspec.name] = data_hash
                    elif mapping == 'blank':
                        face_type = arrayspec.get_face_type()
                        face = face_type(arrayspec, session)
                        data_hash = array_service.save(face.load_blank(session), cycle_uid=cycle.uid)
                        output_drop[arrayspec.name] = data_hash
                    elif mapping == 'random':
                        face_type = arrayspec.get_face_type()
                        face = face_type(arrayspec, session)
                        data_hash = array_service.save(face.load_random(session), cycle_uid=cycle.uid)
                        output_drop[arrayspec.name] = data_hash

                output_drops['output'].append(output_drop)
                cycle.progress = min((100 * drop_idx) // drop_count, 99)
                session.commit()

        cycle.output_drops = output_drops
        session.commit()

    def get_public_config(self) -> Dict:
        drop_count = int(self.pump.engine_config.get('drop_count'))

        try:
            drops_per_page = int(self.pump.engine_config.get('drops_per_page'))
        except ValueError:
            drops_per_page = drop_count

        return {
            'drop_count': drop_count,
            'drops_per_page': drops_per_page
        }

    @staticmethod
    def get_code() -> str:
        return 'create'

    @staticmethod
    def get_pipes() -> Tuple:
        return [], ['output']
