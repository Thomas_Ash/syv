import copy
import time
from typing import Dict, Tuple, List

import requests
import logging
from urllib import parse

from sqlalchemy.orm import Session
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException

from syv.models import Cycle
from syv.config import Config
from syv.services import ArrayService, TankService
from syv.exceptions import FaceDataError, EngineConfigError
from .base_engine import BaseEngine


class ScrapeEngine(BaseEngine):

    def get_config_template(self, session: Session) -> Dict:
        config_template = super().get_config_template(session)
        choices = {
            'scrape': 'Scrape',
            'constant': 'Constant'
        }

        config_template.update(self.get_mappings(session, choices=choices))

        config_template.update({
            'use_browser': {
                'label': 'Use Browser?',
                'type': 'boolean_radio',
                'options': {}
            },
            'filter_on_state': {
                'label': 'Filter On State?',
                'type': 'boolean_radio',
                'options': {}
            },
            'defer_base_url': {
                'label': 'Defer Base URL?',
                'type': 'boolean_radio',
                'options': {}
            }
        })

        defer_base_url = self.pump.engine_config.get('defer_base_url')
        filter_on_state = self.pump.engine_config.get('filter_on_state')
        use_browser = self.pump.engine_config.get('use_browser')

        if defer_base_url is not True:
            config_template.update({
                'base_url': {
                    'label': 'Base URL',
                    'type': 'text',
                    'options': {}
                }
            })

        config_template.update({
            'follow_depth': {
                'label': 'Link Follow Depth',
                'type': 'number',
                'options': {}
            }
        })

        try:
            follow_depth = int(self.pump.engine_config.get('follow_depth'))
            assert follow_depth > 0

            config_template.update({
                'follow_selector': {
                    'label': 'Link Follow Selector',
                    'type': 'text',
                    'options': {}
                },
                'follow_attribute': {
                    'label': 'Link Follow Attribute',
                    'type': 'text',
                    'options': {}
                }
            })

        except (Exception,) as e:
            pass

        config_template.update({
            'drop_selector': {
                'label': 'Drop Selector',
                'type': 'text',
                'options': {}
            },
            'drop_limit': {
                'label': 'Drop Limit',
                'type': 'number',
                'options': {}
            }
        })

        if filter_on_state is True:
            config_template.update({
                'state_key_selector': {
                    'label': 'State Key Selector',
                    'type': 'text',
                    'options': {}
                },
                'state_key_attribute': {
                    'label': 'State Key Attribute',
                    'type': 'text',
                    'options': {}
                }
            })

        pipe = self.pump.get_output_pipe('output')

        if pipe is None or pipe.tank_uid is None:
            return config_template

        tank_service = TankService(session)

        tank = tank_service.get_by_uid(pipe.tank_uid)

        if tank is None:
            return config_template

        scrape_mappings = [
            x for x in self.pump.engine_config.keys()
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x] == 'scrape'
        ]

        for scrape_mapping in scrape_mappings:
            arrayspec_name = scrape_mapping[15:]

            config_template.update({
                f'array_selector_{arrayspec_name}': {
                    'label': f'{tank.name} / {arrayspec_name} Selector',
                    'type': 'text',
                    'options': {}
                }
            })

        if use_browser is True:
            config_template.update({
                'initial_wait': {
                    'label': 'Initial Wait (ms)',
                    'type': 'number',
                    'options': {}
                },
                'script_iterations': {
                    'label': 'Script Iterations',
                    'type': 'number',
                    'options': {}
                }

            })

            try:
                script_iterations = int(self.pump.engine_config.get('script_iterations'))
                assert script_iterations > 0

                config_template.update({
                    'script_scroll_end': {
                        'label': 'Script Scroll To End?',
                        'type': 'boolean_radio',
                        'options': {}
                    },
                    'use_script_element': {
                        'label': 'Script Event On Element?',
                        'type': 'boolean_radio',
                        'options': {}
                    }
                })

                use_script_element = self.pump.engine_config.get('use_script_element')

                if use_script_element is True:
                    config_template.update({
                        'script_element_selector': {
                            'label': 'Script Element Selector',
                            'type': 'text',
                            'options': {}
                        },
                        'script_element_event': {
                            'label': 'Script Element Event',
                            'type': 'custom_select',
                            'options': {
                                'choices': {
                                    'click': 'Click',
                                    'context_click': 'Right Click',
                                    'double_click': 'Double Click'
                                }
                            }
                        }
                    })

                config_template.update({
                    'script_wait': {
                        'label': 'Script Wait (ms)',
                        'type': 'number',
                        'options': {}
                    }
                })

            except (TypeError, ValueError, AssertionError):
                pass

        return config_template

    def validate(self, session: Session) -> Dict:
        report = super().validate(session)

        output_pipe = self.pump.get_pipe('output')

        if output_pipe is None or output_pipe.tank_uid is None:
            report['engine_code']['errors'].append(
                'Output pipe is missing or invalid.'
            )
            return report

        tank_service = TankService(session)

        tank = tank_service.get_by_uid(output_pipe.tank_uid)

        if tank is None:
            report['engine_code']['errors'].append(
                'Output pipe is missing or invalid.'
            )
            return report

        try:
            follow_depth = int(self.pump.engine_config.get('follow_depth'))
            assert follow_depth >= 0
            if follow_depth > 0:
                follow_selector = self.pump.engine_config.get('follow_selector')
                if type(follow_selector) is not str or follow_selector.strip() == '':
                    report['engine_config']['follow_selector']['errors'].append('Link follow selector is invalid.')
                follow_attribute = self.pump.engine_config.get('follow_attribute')
                if type(follow_attribute) is not str or follow_attribute.strip() == '':
                    report['engine_config']['follow_attribute']['errors'].append('Link follow attribute is invalid.')

        except(TypeError, ValueError, AssertionError):
            follow_depth = None
            report['engine_config']['follow_depth']['errors'].append(
                'Link follow depth should be a non-negative integer.'
            )

        use_browser = self.pump.engine_config.get('use_browser')
        if use_browser not in [True, False]:
            report['engine_config']['use_browser']['errors'].append('Use browser should be true or false.')
        elif use_browser is True:
            # validate Selenium-related fields here
            try:
                initial_wait = int(self.pump.engine_config.get('initial_wait'))
                assert initial_wait >= 0
            except (TypeError, ValueError, AssertionError):
                report['engine_config']['initial_wait']['errors'].append(
                    'Initial wait should be a non-negative integer.'
                )

            try:
                script_iterations = int(self.pump.engine_config.get('script_iterations'))
                assert script_iterations >= 0

                if script_iterations > 0:
                    script_scroll_end = self.pump.engine_config.get('script_scroll_end')
                    if script_scroll_end not in [True, False]:
                        report['engine_config']['script_scroll_end']['errors'].append(
                            'Scroll to end should be true or false.'
                        )

                    use_script_element = self.pump.engine_config.get('use_script_element')
                    if use_script_element not in [True, False]:
                        report['engine_config']['script_element']['errors'].append(
                            'Event on element should be true or false.'
                        )
                    elif use_script_element is True:
                        script_element_selector = self.pump.engine_config.get('script_element_selector')
                        if type(script_element_selector) is not str or script_element_selector.strip() == '':
                            report['engine_config']['script_element_selector']['errors'].append(
                                'Element selector is invalid.')
                        script_element_event = self.pump.engine_config.get('script_element_event')
                        if type(script_element_event) not in ['click', 'context_click', 'double_click']:
                            report['engine_config']['script_element_event']['errors'].append(
                                'Element event is invalid.')

                    try:
                        script_wait = int(self.pump.engine_config.get('script_wait'))
                        assert script_wait >= 0
                    except (TypeError, ValueError, AssertionError):
                        report['engine_config']['script_wait']['errors'].append(
                            'Script wait should be a non-negative integer.'
                        )

            except (TypeError, ValueError, AssertionError):
                report['engine_config']['script_iterations']['errors'].append(
                    'Script iterations should be a non-negative integer.'
                )

        filter_on_state = self.pump.engine_config.get('filter_on_state')
        if filter_on_state not in [True, False]:
            report['engine_config']['filter_on_state']['errors'].append('Filter on state should be true or false.')
        elif filter_on_state is True:
            state_key_selector = self.pump.engine_config.get('state_key_selector')
            if type(state_key_selector) is not str or state_key_selector.strip() == '':
                report['engine_config']['state_key_selector']['errors'].append('State key selector is invalid.')

            state_key_attribute = self.pump.engine_config.get('state_key_attribute')
            if type(state_key_attribute) is not str or state_key_attribute.strip() == '':
                report['engine_config']['state_key_attribute']['errors'].append('State key attribute is invalid.')


        defer_base_url = self.pump.engine_config.get('defer_base_url')
        if defer_base_url not in [True, False]:
            report['engine_config']['defer_base_url']['errors'].append('Defer base URL should be true or false.')
        elif defer_base_url is False:
            base_url = self.pump.engine_config.get('base_url')
            if type(base_url) is not str or base_url.strip() == '':
                report['engine_config']['base_url']['errors'].append('Base URL is invalid.')

        drop_selector = self.pump.engine_config.get('drop_selector')
        if type(drop_selector) is not str or drop_selector.strip() == '':
            report['engine_config']['drop_selector']['errors'].append('Drop selector is invalid.')

        try:
            drop_limit = int(self.pump.engine_config.get('drop_limit'))
            assert drop_limit > 0

        except(TypeError, ValueError, AssertionError):
            report['engine_config']['drop_limit']['errors'].append('Drop limit should be a positive integer.')

        scrape_mappings = [
            x for x in self.pump.engine_config.keys()
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x] == 'scrape'
        ]

        for scrape_mapping in scrape_mappings:
            arrayspec_name = scrape_mapping[15:]
            arrayspec = tank.get_arrayspec(name=arrayspec_name)
            if arrayspec is None:
                report['engine_config'][scrape_mapping]['errors'].append('Invalid scrape mapping.')
            elif not any([
                x for x in arrayspec.get_face_type().get_modes()
                if x[0] == 'html' and x[3] is True
            ]):
                report['engine_config'][scrape_mapping]['errors'].append(
                    'Face of this array spec does not support loading in HTML mode.'
                )

            selector_field_name = f'array_selector_{arrayspec_name}'
            selector = self.pump.engine_config.get(selector_field_name)
            if type(selector) is not str or selector.strip() == '':
                report['engine_config'][selector_field_name]['errors'].append('Array spec selector is invalid.')



        return report

    def needs_client_post(self) -> bool:
        defer_base_url = self.pump.engine_config.get('defer_base_url')
        if defer_base_url is False:
            return False

        return True

    def get_client_config_template(self) -> Dict:
        if self.pump.engine_config.get('defer_base_url') is True:
            return {
                'base_url': {
                    'label': 'Base URL',
                    'type': 'text',
                    'options': {}
                }
            }
        else:
            return {}

    def can_commit(self, cycle: Cycle) -> bool:
        return super().can_commit(cycle)

    def process(self, cycle: Cycle, session: Session) -> None:
        defer_base_url = self.pump.engine_config.get('defer_base_url')
        if defer_base_url:
            base_url = cycle.client_config.get('base_url')
        else:
            base_url = self.pump.engine_config.get('base_url')

        urls = [base_url]
        scraped_urls = []

        use_browser = self.pump.engine_config.get('use_browser')
        drop_limit = int(self.pump.engine_config.get('drop_limit'))
        depth_limit = int(self.pump.engine_config.get('follow_depth')) + 1
        filter_on_state = self.pump.engine_config.get('filter_on_state')

        if filter_on_state is True:
            cycle.state = {
                'keys': []
            }

        for depth_idx in range(depth_limit):
            if (len(cycle.output_drops)) < drop_limit:
                target_urls = [x for x in urls if x not in scraped_urls]
                new_urls = []
                for url_idx in range(len(target_urls)):
                    if(len(cycle.output_drops)) < drop_limit:
                        if use_browser:
                            new_urls.extend(self.scrape_selenium(target_urls[url_idx], cycle, session))
                        else:
                            new_urls.extend(self.scrape_requests(target_urls[url_idx], cycle, session))

                        scraped_urls.append(target_urls[url_idx])

                urls.extend([x for x in set(new_urls) if x not in scraped_urls])

    # write cycle.output_drops & progress, return urls
    def scrape_requests(self, url: str, cycle: Cycle, session: Session) -> List:
        drop_limit = int(self.pump.engine_config.get('drop_limit'))
        follow_depth = int(self.pump.engine_config.get('follow_depth'))
        follow_selector = self.pump.engine_config.get('follow_selector')
        follow_attribute = self.pump.engine_config.get('follow_attribute')
        drop_selector = self.pump.engine_config.get('drop_selector')
        filter_on_state = self.pump.engine_config.get('filter_on_state')

        if filter_on_state is True:
            state_key_selector = self.pump.engine_config.get('state_key_selector')
            state_key_attribute = self.pump.engine_config.get('state_key_attribute')
            pump_keys = []
            for cycle_uid in self.pump.state.keys():
                if type(self.pump.state[cycle_uid]) is dict \
                        and 'keys' in self.pump.state[cycle_uid] \
                        and type(self.pump.state[cycle_uid]['keys']) is list:
                    pump_keys.extend([x for x in self.pump.state[cycle_uid].get('keys') if x not in pump_keys])

        else:
            state_key_selector = None
            state_key_attribute = None
            pump_keys = []

        follow_urls = []
        output_pipe = self.pump.get_pipe('output')
        output_drops = copy.deepcopy(cycle.output_drops)
        cycle_keys = cycle.state.get('keys')

        try:
            res = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
            if res.ok:
                soup = BeautifulSoup(res.text, features='html5lib')

                if follow_depth > 0:
                    follow_tags = soup.select(follow_selector)
                    follow_urls = [parse.urljoin(url, x.attrs.get(follow_attribute))
                                   for x in follow_tags
                                   if follow_attribute in x.attrs]

                drop_tags = soup.select(drop_selector)

                array_service = ArrayService(session)

                for drop_tag_idx in range(len(drop_tags)):

                    if len(output_drops) < drop_limit:
                        try:
                            if filter_on_state is True:
                                state_key_tags = drop_tags[drop_tag_idx].select(state_key_selector)
                                assert len(state_key_tags) > 0
                                assert state_key_attribute in state_key_tags[0].attrs

                                key = state_key_tags[0].attrs.get(state_key_attribute)

                                assert key not in pump_keys
                                assert key not in cycle_keys

                                cycle_keys.append(key)

                            output_drop = {}

                            for arrayspec in output_pipe.tank.arrayspecs:
                                mapping = self.pump.engine_config.get(
                                    f'mapping_{output_pipe.name}_{arrayspec.name}'
                                )
                                face_type = arrayspec.get_face_type()
                                face = face_type(arrayspec, session)

                                if mapping == 'scrape':
                                    array_selector = self.pump.engine_config.get(
                                        f'array_selector_{arrayspec.name}'
                                    )
                                    array_tags = drop_tags[drop_tag_idx].select(array_selector)
                                    assert len(array_tags) > 0
                                    array = face.load(
                                        {
                                            'html': str(array_tags[0]),
                                            'origin': url
                                        },
                                        'html',
                                        session
                                    )
                                    # don't wait until checks in cycle.complete which would abort entire cycle
                                    assert arrayspec.shape_fits(array.shape)
                                    assert arrayspec.dtype == array.dtype.char

                                    data_hash = array_service.save(array, cycle_uid=cycle.uid)
                                    output_drop[arrayspec.name] = data_hash

                                elif mapping == 'constant':
                                    data_hash = self.pump.engine_config[
                                        f'constant_{output_pipe.name}_{arrayspec.name}']
                                    output_drop[arrayspec.name] = data_hash
                                elif mapping == 'blank':
                                    data_hash = array_service.save(face.load_blank(session), cycle_uid=cycle.uid)
                                    output_drop[arrayspec.name] = data_hash
                                elif mapping == 'random':
                                    data_hash = array_service.save(face.load_random(session), cycle_uid=cycle.uid)
                                    output_drop[arrayspec.name] = data_hash


                            output_drops['output'].append(output_drop)
                            cycle.progress = min((100 * len(output_drops['output']) // drop_limit), 99)
                            session.commit()

                        except (AssertionError, FaceDataError):
                            pass
                            # this is normal and should not spam the log

                    cycle.state = {
                        'keys': cycle_keys
                    }
                    cycle.output_drops = output_drops
                    session.commit()

            else:
                logging.warning(f'url "{url}" returned error code {str(res.status_code)}')

        except (Exception,) as e:
            logging.warning(f'url "{url}" scrape failed: {str(type(e))} / {str(e)}')

        return follow_urls

    def scrape_selenium(self, url: str, cycle: Cycle, session: Session) -> List:
        drop_limit = int(self.pump.engine_config.get('drop_limit'))
        follow_depth = int(self.pump.engine_config.get('follow_depth'))
        follow_selector = self.pump.engine_config.get('follow_selector')
        follow_attribute = self.pump.engine_config.get('follow_attribute')
        drop_selector = self.pump.engine_config.get('drop_selector')
        filter_on_state = self.pump.engine_config.get('filter_on_state')

        if filter_on_state is True:
            state_key_selector = self.pump.engine_config.get('state_key_selector')
            state_key_attribute = self.pump.engine_config.get('state_key_attribute')
            pump_keys = []
            for cycle_uid in self.pump.state.keys():
                if type(self.pump.state[cycle_uid]) is dict \
                        and 'keys' in self.pump.state[cycle_uid] \
                        and type(self.pump.state[cycle_uid]['keys']) is list:
                    pump_keys.extend([x for x in self.pump.state[cycle_uid]['keys'] if x not in pump_keys])

        else:
            state_key_selector = None
            state_key_attribute = None
            pump_keys = []

        initial_wait = int(self.pump.engine_config.get('initial_wait'))
        script_iterations = int(self.pump.engine_config.get('script_iterations'))

        if script_iterations > 0:
            script_scroll_end = self.pump.engine_config.get('script_scroll_end')
            use_script_element = self.pump.engine_config.get('use_script_element')
            if use_script_element is True:
                script_element_selector = self.pump.engine_config.get('script_element_selector')
                script_element_event = self.pump.engine_config.get('script_element_event')
            else:
                script_element_selector = None
                script_element_event = None
            script_wait = int(self.pump.engine_config.get('script_wait'))

        else:
            script_scroll_end = None
            use_script_element = None
            script_element_selector = None
            script_element_event = None
            script_wait = None

        follow_urls = []
        output_pipe = self.pump.get_pipe('output')
        output_drops = copy.deepcopy(cycle.output_drops)
        cycle_keys = cycle.state.get('keys')

        try:
            mode = Config.SELENIUM_MODE
            if mode == 'local':
                options = webdriver.FirefoxOptions()
                options.add_argument('--headless')
                driver = webdriver.Firefox(options=options)
            elif mode == 'remote':
                driver = webdriver.Remote(
                    'http://firefox:4444/wd/hub',
                    desired_capabilities=webdriver.DesiredCapabilities.FIREFOX
                )
            else:
                raise EngineConfigError


            driver.get(url)
            time.sleep(initial_wait / 1000)

            for _ in range(script_iterations):

                if use_script_element is True:
                    try:
                        script_element = driver.find_element_by_css_selector(script_element_selector)
                    except NoSuchElementException:
                        script_element = None
                else:
                    script_element = None

                actions = ActionChains(driver)
                if script_scroll_end is True:
                    driver.execute_script('window.scrollTo(0, document.body.scrollHeight)')
                if script_element is not None:
                    if script_element_event == 'click':
                        actions.click(script_element)
                    elif script_element_event == 'context_click':
                        actions.context_click(script_element)
                    elif script_element_event == 'double_click':
                        actions.double_click(script_element)

                actions.pause(script_wait / 1000).perform()

            if follow_depth > 0:
                follow_elements = driver.find_elements_by_css_selector(follow_selector)
                follow_urls = [parse.urljoin(url, x.get_attribute(follow_attribute))
                               for x in follow_elements
                               if x.get_attribute(follow_attribute) is not None]

            drop_elements = driver.find_elements_by_css_selector(drop_selector)

            array_service = ArrayService(session)

            for drop_element_idx in range(len(drop_elements)):
                if len(output_drops) < drop_limit:
                    try:
                        if filter_on_state is True:
                            state_key_element = drop_elements[drop_element_idx].find_element_by_css_selector(
                                state_key_selector
                            )
                            key = state_key_element.get_attribute(state_key_attribute)

                            assert key is not None
                            assert key not in pump_keys
                            assert key not in cycle_keys

                            cycle_keys.append(key)

                        output_drop = {}

                        for arrayspec in output_pipe.tank.arrayspecs:
                            mapping = self.pump.engine_config.get(
                                f'mapping_{output_pipe.name}_{arrayspec.name}'
                            )
                            face_type = arrayspec.get_face_type()
                            face = face_type(arrayspec, session)

                            if mapping == 'scrape':
                                array_selector = self.pump.engine_config.get(
                                    f'array_selector_{arrayspec.name}'
                                )
                                array_element = drop_elements[drop_element_idx].find_element_by_css_selector(
                                    array_selector
                                )
                                array = face.load(
                                    {
                                        'html': str(array_element.get_attribute('outerHTML')),
                                        'origin': url
                                    },
                                    'html',
                                    session
                                )

                                # don't wait until checks in cycle.complete which would abort entire cycle
                                assert arrayspec.shape_fits(array.shape)
                                assert arrayspec.dtype == array.dtype.char

                                data_hash = array_service.save(array, cycle_uid=cycle.uid)
                                output_drop[arrayspec.name] = data_hash

                            elif mapping == 'constant':
                                data_hash = self.pump.engine_config[
                                    'constant_%s_%s' % (output_pipe.name, arrayspec.name)]
                                output_drop[arrayspec.name] = data_hash
                            elif mapping == 'blank':
                                data_hash = array_service.save(face.load_blank(session), cycle_uid=cycle.uid)
                                output_drop[arrayspec.name] = data_hash
                            elif mapping == 'random':
                                data_hash = array_service.save(face.load_random(session), cycle_uid=cycle.uid)
                                output_drop[arrayspec.name] = data_hash

                        output_drops['output'].append(output_drop)
                        cycle.progress = min((100 * len(output_drops['output']) // drop_limit), 99)
                        session.commit()

                    except (AssertionError, FaceDataError, NoSuchElementException):
                        pass

            cycle.state = {
                'keys': cycle_keys
            }
            cycle.output_drops = output_drops
            session.commit()

        except (Exception,) as e:
            logging.warning('url "%s" scrape failed: %s/%s' % (url, str(type(e)), str(e)))

        finally:
            try:
                driver.quit()
            except (Exception,) as e:
                pass

        return follow_urls

    def get_public_config(self) -> Dict:
        return {
            'defer_base_url': self.pump.engine_config.get('defer_base_url')
        }

    @staticmethod
    def get_code() -> str:
        return 'scrape'

    @staticmethod
    def get_pipes() -> Tuple:
        return [], ['output']

    @staticmethod
    def get_records() -> Tuple:
        return [], []
