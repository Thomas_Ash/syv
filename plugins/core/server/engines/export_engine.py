import logging
import os
from io import BytesIO
import zipfile
from typing import Dict, Tuple

from sqlalchemy.orm import Session
import slugify

from syv.models import Cycle
from syv.services import ArrayService, TankService, RecordService
from .base_engine import BaseEngine


class ExportEngine(BaseEngine):

    def get_config_template(self, session: Session) -> Dict:
        config_template = super().get_config_template(session)

        # no output pipe so generate pseudo-mappings of input here

        input_pipe = self.pump.get_pipe('input')

        if input_pipe is None or input_pipe.tank_uid is None:
            return config_template

        tank_service = TankService(session)

        tank = tank_service.get_by_uid(input_pipe.tank_uid)

        if tank is None:
            return config_template

        modes = [x.get_face_type().get_modes() for x in tank.arrayspecs]
        has_text = [any([y for y in x if y[0] == 'text']) for x in modes]

        for i in range(len(tank.arrayspecs)):

            choices = {'ignore': 'Ignore'}
            if has_text[i]:
                choices.update({
                    f'dir_{j}': f'Directory Level {str(j+1)}'
                    for j in range(len([x for x in has_text if x]))
                })
                choices.update({'filename': 'Filename'})

            if any([x for x in modes[i] if x[0] == 'file']):
                choices.update({'file': 'File'})

            config_template.update({
                f'mapping_{tank.arrayspecs[i].name}': {
                    'label': f'{tank.name} / {tank.arrayspecs[i].name}',
                    'type': 'custom_select',
                    'options': {
                        'choices': choices,
                        'icon_code': 'arrow-right'
                    }
                }
            })

        return config_template

    def validate(self, session: Session) -> Dict:
        report = super().validate(session)

        input_pipe = self.pump.get_pipe('input')

        if input_pipe is None or input_pipe.tank_uid is None:
            report['engine_code']['errors'].append(
                'Input pipe is missing or invalid.'
            )
            return report

        tank_service = TankService(session)

        tank = tank_service.get_by_uid(input_pipe.tank_uid)

        if tank is None:
            report['engine_code']['errors'].append(
                'Input pipe is missing or invalid.'
            )
            return report

        field_names = self.pump.engine_config.keys()

        file_mappings = {
            x: self.pump.engine_config[x] for x in field_names
            if x.startswith('mapping_')
            and self.pump.engine_config[x] == 'file'
        }

        if len(file_mappings) != 1:
            report['engine_code']['errors'].append(
                'File should be mapped to exactly one array spec.'
            )

        for field_name in file_mappings.keys():
            file_arrayspec = tank.get_arrayspec(name=field_name[8:])
            if file_arrayspec is None:
                report['engine_config'][field_name]['errors'].append(
                    'Invalid file mapping.'
                )
            elif not any([
                x for x in file_arrayspec.get_face_type().get_modes()
                if x[0] == 'file' and x[2] is True
            ]):
                report['engine_config'][field_name]['errors'].append(
                    'Face of this arrayspec does not support dumping in file mode.'
                )

        dir_mappings = {
            x: self.pump.engine_config[x] for x in field_names
            if x.startswith('mapping_')
            and self.pump.engine_config[x].startswith('dir_')
        }

        for field_name in dir_mappings:
            dir_arrayspec = tank.get_arrayspec(name=field_name[8:])
            if dir_arrayspec is None:
                report['engine_config'][field_name]['errors'].append(
                    'Invalid directory mapping.'
                )
            elif not any([
                x for x in dir_arrayspec.get_face_type().get_modes()
                if x[0] == 'text' and x[2] is True
            ]):
                report['engine_config'][field_name]['errors'].append(
                    'Face of this arrayspec does not support dumping in text mode.'
                )

            try:
                dir_level = int(dir_mappings[field_name][4:])

                if dir_level > 0:
                    prev_mapping = next(iter([
                        x for x in dir_mappings.keys()
                        if dir_mappings[x] == f'dir_{str(dir_level-1)}'
                    ]), None)
                    if prev_mapping is None:
                        report['engine_config'][field_name]['errors'].append(
                            f'The previous directory level ({dir_level}) should be mapped to an arrayspec.'
                        )

            except (TypeError, ValueError):
                report['engine_config'][field_name]['errors'].append(
                    'Invalid directory mapping.'
                )


        filename_mappings = {
            x: self.pump.engine_config[x] for x in field_names
            if x.startswith('mapping_')
            and self.pump.engine_config[x] == 'filename'
        }

        if len(filename_mappings) > 1:
            report['engine_code']['errors'].append(
                'Filename should not be mapped to more than one array spec.'
            )

        for field_name in filename_mappings.keys():
            filename_arrayspec = tank.get_arrayspec(name=field_name[8:])
            if filename_arrayspec is None:
                report['engine_config'][field_name]['errors'].append(
                    'Invalid filename mapping.'
                )
            elif not any([
                x for x in filename_arrayspec.get_face_type().get_modes()
                if x[0] == 'text' and x[2] is True
            ]):
                report['engine_config'][field_name]['errors'].append(
                    'Face of this arrayspec does not support dumping in text mode.'
                )

        return report

    def needs_client_post(self) -> bool:
        return False

    def can_commit(self, cycle: Cycle) -> bool:
        return True

    def process(self, cycle: Cycle, session: Session) -> None:
        input_pipe = self.pump.get_pipe('input')
        field_names = self.pump.engine_config.keys()

        file_mappings = [
            x for x in field_names
            if x.startswith('mapping_')
            and self.pump.engine_config[x] == 'file'
        ]

        file_arrayspec = input_pipe.tank.get_arrayspec(name=file_mappings[0][8:])
        file_face = file_arrayspec.get_face_type()(file_arrayspec, session)

        dir_mappings = [
            (x, self.pump.engine_config[x]) for x in field_names
            if x.startswith('mapping_')
            and self.pump.engine_config[x].startswith('dir_')
        ]

        dir_arrayspecs = []
        dir_faces = []

        for dir_idx in range(len(dir_mappings)):
            dir_mapping = next(iter([
                x for x in dir_mappings if x[1] == f'dir_{dir_idx}'
            ]))

            dir_arrayspec = input_pipe.tank.get_arrayspec(name=dir_mapping[0][8:])
            dir_face = dir_arrayspec.get_face_type()(dir_arrayspec, session)

            dir_arrayspecs.append(dir_arrayspec)
            dir_faces.append(dir_face)

        filename_mappings = [
            x for x in field_names
            if x.startswith('mapping_')
            and self.pump.engine_config[x] == 'filename'
        ]

        filename_arrayspec = input_pipe.tank.get_arrayspec(name=filename_mappings[0][8:]) \
            if len(filename_mappings) == 1 else None

        filename_face = None if filename_arrayspec is None else \
            filename_arrayspec.get_face_type()(filename_arrayspec, session)

        zip_stream = BytesIO()
        drop_count = len(cycle.input_drops)

        array_service = ArrayService(session)

        with zipfile.ZipFile(zip_stream, mode='w', compression=zipfile.ZIP_DEFLATED) as zip_file:
            for drop_idx in range(drop_count):
                drop = cycle.input_drops[drop_idx]
                path_parts = []
                for dir_idx in range(len(dir_arrayspecs)):
                    dir_arrayinst = drop.get_arrayinst(arrayspec_uid=dir_arrayspecs[dir_idx].uid)
                    dir_array = array_service.get_by_data_hash(dir_arrayinst.array_data_hash)
                    path_parts.append(slugify.slugify(
                        dir_faces[dir_idx].dump(dir_array.data_array, 'text', session)
                    ))

                file_arrayinst = drop.get_arrayinst(arrayspec_uid=file_arrayspec.uid)
                file_array = array_service.get_by_data_hash(file_arrayinst.array_data_hash)
                file_stream = file_face.dump(file_array.data_array, 'file', session)

                if filename_arrayspec is None or filename_face is None:
                    filename = str(drop.uid)
                else:
                    filename_arrayinst = drop.get_arrayinst(arrayspec_uid=filename_arrayspec.uid)
                    filename_array = array_service.get_by_data_hash(filename_arrayinst.array_data_hash)
                    filename = slugify.slugify(filename_face.dump(filename_array.data_array, 'text', session))

                path_parts.append(f'{filename}.{file_face.get_extension()}')

                try:
                    zip_file.writestr(os.path.join(*path_parts), file_stream.getvalue())
                except (Exception,) as e:
                    logging.warning(f'Failed to add drop to zip archive: {str(e)}')

                cycle.progress = min((100 * drop_idx) // drop_count, 99)
                session.commit()

        record_service = RecordService(session)
        record_service.save(zip_stream, 'output.zip', 'output', cycle)
        session.commit()

    @staticmethod
    def get_code() -> str:
        return 'export'

    @staticmethod
    def get_pipes() -> Tuple:
        return ['input'], []

    @staticmethod
    def get_records() -> Tuple:
        return [], ['output']
