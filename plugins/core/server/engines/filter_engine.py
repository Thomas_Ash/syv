import copy
from typing import Dict, Tuple

from sqlalchemy.orm import Session

from syv.models import Cycle
from syv.exceptions import EngineInternalError
from syv.services import ArrayService, TankService
from .base_engine import BaseEngine


class FilterEngine(BaseEngine):

    def get_config_template(self, session: Session) -> Dict:
        config_template = super().get_config_template(session)
        config_template.update(self.get_mappings(session, choices={}))

        config_template.update({
            'drops_per_page': {
                'label': 'Drops Per Page',
                'type': 'number',
                'options': {}
            }
        })

        return config_template


    def validate(self, session: Session) -> Dict:
        report = super().validate(session)

        try:
            drops_per_page = int(self.pump.engine_config.get('drops_per_page'))
            assert drops_per_page > 0
        except (TypeError, ValueError, AssertionError):
            report['engine_config']['drops_per_page']['errors'].append('Drops per page should be a positive integer.')

        tank_service = TankService(session)

        input_pipe = self.pump.get_pipe('input')
        input_tank = tank_service.get_by_uid(input_pipe.tank_uid)

        if input_pipe and input_tank:

            accept_pipe = self.pump.get_pipe('accept')
            accept_tank = tank_service.get_by_uid(accept_pipe.tank_uid)

            if accept_pipe and accept_tank:
                for accept_arrayspec in accept_tank.arrayspecs:
                    mapping_key = f'mapping_{accept_pipe.name}_{accept_arrayspec.name}'
                    mapping_value = self.pump.engine_config[mapping_key]
                    if mapping_value in ['blank', 'random']:
                        continue

                    try:
                        input_arrayspec_uid = int(mapping_value.split('_')[1])
                        input_arrayspec = input_tank.get_arrayspec(uid=input_arrayspec_uid)
                        assert input_arrayspec is not None
                    except (ValueError, IndexError, AssertionError):
                        report['engine_config'][mapping_key]['errors'].append('Invalid array mapping.')
                    
            else:
                report['engine_code']['errors'].append('Accept pipe is missing or invalid.')

            reject_pipe = self.pump.get_pipe('reject')
            reject_tank = tank_service.get_by_uid(reject_pipe.tank_uid)

            if reject_pipe and reject_tank:
                for reject_arrayspec in reject_tank.arrayspecs:
                    mapping_key = f'mapping_{reject_pipe.name}_{reject_arrayspec.name}'
                    mapping_value = self.pump.engine_config[mapping_key]
                    if mapping_value in ['blank', 'random']:
                        continue

                    try:
                        input_arrayspec_uid = int(mapping_value.split('_')[1])
                        input_arrayspec = input_tank.get_arrayspec(uid=input_arrayspec_uid)
                        assert input_arrayspec is not None
                    except (ValueError, IndexError, AssertionError):
                        report['engine_config'][mapping_key]['errors'].append('Invalid array mapping.')

            else:
                report['engine_code']['errors'].append('Reject pipe is missing or invalid.')

        else:
            report['engine_code']['errors'].append('Input pipe is missing or invalid.')

        return report

    def needs_client_post(self) -> bool:
        return True

    def can_commit(self, cycle: Cycle) -> bool:
        if 'accept' not in cycle.client_drops or 'reject' not in cycle.client_drops:
            return False

        for drop in cycle.input_drops:
            accept = next(iter(x for x in cycle.client_drops['accept'] if x.get('mapped_uid') == drop.uid), None)
            reject = next(iter(x for x in cycle.client_drops['reject'] if x.get('mapped_uid') == drop.uid), None)
            if accept is None and reject is None:
                return False

        return True

    def process(self, cycle: Cycle, session: Session) -> None:

        accept_pipe = self.pump.get_pipe('accept')
        reject_pipe = self.pump.get_pipe('reject')
        output_drops = copy.deepcopy(cycle.output_drops)
        drop_count = len(cycle.input_drops)

        array_service = ArrayService(session)

        for drop_idx in range(drop_count):
            input_drop = cycle.input_drops[drop_idx]
            if any([x for x in cycle.client_drops['accept'] if x.get('mapped_uid') == input_drop.uid]):
                output_pipe = accept_pipe
            elif any([x for x in cycle.client_drops['reject'] if x.get('mapped_uid') == input_drop.uid]):
                output_pipe = reject_pipe
            else:
                raise EngineInternalError

            output_drop = {}
            for output_arrayspec in output_pipe.tank.arrayspecs:
                mapping = self.pump.engine_config[f'mapping_{output_pipe.name}_{output_arrayspec.name}']
                if mapping == 'blank':
                    face_type = output_arrayspec.get_face_type()
                    face = face_type(output_arrayspec, session)
                    data_hash = array_service.save(face.load_blank(session), cycle_uid=cycle.uid)
                    output_drop[output_arrayspec.name] = data_hash
                elif mapping == 'random':
                    face_type = output_arrayspec.get_face_type()
                    face = face_type(output_arrayspec, session)
                    data_hash = array_service.save(face.load_random(session), cycle_uid=cycle.uid)
                    output_drop[output_arrayspec.name] = data_hash
                elif mapping.startswith('arrayspec_'):
                    input_arrayspec_uid = int(mapping.split('_')[1])
                    input_arrayinst = input_drop.get_arrayinst(arrayspec_uid=input_arrayspec_uid)
                    output_drop[output_arrayspec.name] = input_arrayinst.array_data_hash

            output_drops[output_pipe.name].append(output_drop)
            cycle.progress = min((100 * drop_idx) // drop_count, 99)
            session.commit()

        cycle.output_drops = output_drops
        session.commit()

    def get_public_config(self) -> Dict:
        input_pipe = self.pump.get_pipe('input')
        drop_count = input_pipe.drop_limit

        try:
            drops_per_page = int(self.pump.engine_config.get('drops_per_page'))
        except ValueError:
            drops_per_page = drop_count

        return {
            'drops_per_page': drops_per_page
        }

    @staticmethod
    def get_code() -> str:
        return 'filter'

    @staticmethod
    def get_pipes() -> Tuple:
        return ['input'], ['accept', 'reject']
