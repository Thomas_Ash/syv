import os
import zipfile
import copy
import logging
from io import BytesIO
from typing import Dict, Tuple

from sqlalchemy.orm import Session

from syv.models import Cycle
from syv.services import ArrayService, TankService
from syv.exceptions import EngineInternalError, FaceDataError
from .base_engine import BaseEngine



class ImportEngine(BaseEngine):

    MAX_DIRECTORY_DEPTH = 5

    def get_config_template(self, session: Session) -> Dict:
        config_template = super().get_config_template(session)

        choices = {
            'file': 'File',
            'filename': 'Filename',
            'constant': 'Constant'
        }

        choices.update({
            f'dir_{x}': f'Directory Level {str(x+1)}'
            for x in range(ImportEngine.MAX_DIRECTORY_DEPTH)
        })

        config_template.update(self.get_mappings(session, choices=choices))

        return config_template

    def validate(self, session: Session) -> Dict:
        report = super().validate(session)

        output_pipe = self.pump.get_pipe('output')

        if output_pipe is None or output_pipe.tank_uid is None:
            report['engine_code']['errors'].append(
                'Output pipe is missing or invalid.'
            )
            return report

        tank_service = TankService(session)

        tank = tank_service.get_by_uid(output_pipe.tank_uid)

        if tank is None:
            report['engine_code']['errors'].append(
                'Output pipe is missing or invalid.'
            )
            return report

        field_names = self.pump.engine_config.keys()

        file_mappings = {
            x: self.pump.engine_config[x] for x in field_names
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x] == 'file'
        }

        if len(file_mappings) == 0:
            report['engine_code']['errors'].append(
                'File should be mapped to at least one array spec.'
            )

        for field_name in file_mappings.keys():
            file_arrayspec = tank.get_arrayspec(name=field_name[15:])
            if file_arrayspec is None:
                report['engine_config'][field_name]['errors'].append(
                    'Invalid file mapping.'
                )
            elif not any([
                x for x in file_arrayspec.get_face_type().get_modes()
                if x[0] == 'file' and x[3] is True
            ]):
                report['engine_config'][field_name]['errors'].append(
                    'Face of this array spec does not support loading in file mode.'
                )

        dir_mappings = {
            x: self.pump.engine_config[x] for x in field_names
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x].startswith('dir_')
        }

        for field_name in dir_mappings:
            dir_arrayspec = tank.get_arrayspec(name=field_name[15:])
            if dir_arrayspec is None:
                report['engine_config'][field_name]['errors'].append(
                    'Invalid directory mapping.'
                )
            elif not any([
                x for x in dir_arrayspec.get_face_type().get_modes()
                if x[0] == 'text' and x[3] is True
            ]):
                report['engine_config'][field_name]['errors'].append(
                    'Face of this arrayspec does not support loading in text mode.'
                )

            try:
                _ = int(dir_mappings[field_name][4:])
            except (TypeError, ValueError):
                report['engine_config'][field_name]['errors'].append(
                    'Invalid directory mapping.'
                )

        filename_mappings = {
            x: self.pump.engine_config[x] for x in field_names
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x] == 'filename'
        }

        for field_name in filename_mappings.keys():
            filename_arrayspec = tank.get_arrayspec(name=field_name[15:])
            if filename_arrayspec is None:
                report['engine_config'][field_name]['errors'].append(
                    'Invalid filename mapping.'
                )
            elif not any([
                x for x in filename_arrayspec.get_face_type().get_modes()
                if x[0] == 'text' and x[3] is True
            ]):
                report['engine_config'][field_name]['errors'].append(
                    'Face of this arrayspec does not support loading in text mode.'
                )

        return report

    def needs_client_post(self) -> bool:
        # this can be False in future, when input file via local path is supported
        return True

    def can_commit(self, cycle: Cycle) -> bool:
        record = cycle.get_record('input')
        return record is not None

    def process(self, cycle: Cycle, session: Session) -> None:

        output_drops = copy.deepcopy(cycle.output_drops)
        record = cycle.get_record('input')

        if record is None or not zipfile.is_zipfile(record.get_path()):
            raise EngineInternalError

        output_pipe = self.pump.get_pipe('output')
        field_names = self.pump.engine_config.keys()

        file_mappings = [
            x for x in field_names
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x] == 'file'
        ]

        file_arrayspecs = [output_pipe.tank.get_arrayspec(name=x[15:]) for x in file_mappings]
        file_faces = [x.get_face_type()(x, session) for x in file_arrayspecs]

        dir_mappings = [
            (x, self.pump.engine_config[x]) for x in field_names
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x].startswith('dir_')
        ]

        dir_arrayspecs = [[]] * ImportEngine.MAX_DIRECTORY_DEPTH
        dir_faces = [[]] * ImportEngine.MAX_DIRECTORY_DEPTH
        dir_count_min = 0

        for dir_idx in range(ImportEngine.MAX_DIRECTORY_DEPTH):
            dir_level_mappings = [
                x for x in dir_mappings if x[1] == f'dir_{dir_idx}'
            ]

            for dir_mapping in dir_level_mappings:
                dir_arrayspec = output_pipe.tank.get_arrayspec(name=dir_mapping[0][15:])
                dir_face = dir_arrayspec.get_face_type()(dir_arrayspec, session)
                dir_arrayspecs[dir_idx].append(dir_arrayspec)
                dir_faces[dir_idx].append(dir_face)
                dir_count_min = max(dir_count_min, dir_idx+1)

        filename_mappings = [
            x for x in field_names
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x] == 'filename'
        ]

        filename_arrayspecs = [output_pipe.tank.get_arrayspec(name=x[15:]) for x in filename_mappings]
        filename_faces = [x.get_face_type()(x, session) for x in filename_arrayspecs]

        constant_hashes = {
            x[15:]: self.pump.engine_config[f'constant_output_{x[15:]}']
            for x in field_names
            if x.startswith('mapping_output_')
            and self.pump.engine_config[x] == 'constant'
        }

        array_service = ArrayService(session)

        with zipfile.ZipFile(record.get_path(), 'r') as zip_file:
            paths = zip_file.namelist()
            path_count = len(paths)

            for path_idx in range(path_count):

                try:
                    path = paths[path_idx]
                    path, filename = os.path.split(path)

                    # open will fail on directory
                    assert len(filename) > 0

                    dir_names = []
                    while path != '':
                        path, dir_name = os.path.split(path)
                        dir_names.insert(0, dir_name)

                    # not enough directories in path would leave null arrays
                    assert dir_count_min <= len(dir_names)

                    dir_names = dir_names[:ImportEngine.MAX_DIRECTORY_DEPTH]

                    output_drop = {}

                    with zip_file.open(paths[path_idx]) as stream:
                        buf = BytesIO()
                        buf.write(stream.read())

                        for arrayspec_idx in range(len(file_arrayspecs)):
                            buf.seek(0)
                            array = file_faces[arrayspec_idx].load(buf, 'file', session)

                            # don't wait until checks in cycle.complete which would abort entire cycle
                            assert file_arrayspecs[arrayspec_idx].shape_fits(array.shape)
                            assert file_arrayspecs[arrayspec_idx].dtype == array.dtype.char

                            data_hash = array_service.save(array, cycle_uid=cycle.uid)
                            output_drop[file_arrayspecs[arrayspec_idx].name] = data_hash

                    for dir_idx in range(len(dir_names)):
                        for arrayspec_idx in range(len(dir_arrayspecs[dir_idx])):
                            array = dir_faces[dir_idx][arrayspec_idx].load(dir_names[dir_idx], 'text', session)

                            # don't wait until checks in cycle.complete which would abort entire cycle
                            assert dir_arrayspecs[dir_idx][arrayspec_idx].shape_fits(array.shape)
                            assert dir_arrayspecs[dir_idx][arrayspec_idx].dtype == array.dtype.char

                            data_hash = array_service.save(array, cycle_uid=cycle.uid)
                            output_drop[dir_arrayspecs[dir_idx][arrayspec_idx].name] = data_hash

                    for arrayspec_idx in range(len(filename_arrayspecs)):
                        array = filename_faces[arrayspec_idx].load(filename.split('.')[0], 'text', session)

                        # don't wait until checks in cycle.complete which would abort entire cycle
                        assert filename_arrayspecs[arrayspec_idx].shape_fits(array.shape)
                        assert filename_arrayspecs[arrayspec_idx].dtype == array.dtype.char

                        data_hash = array_service.save(array, cycle_uid=cycle.uid)
                        output_drop[filename_arrayspecs[arrayspec_idx].name] = data_hash

                    output_drop.update(constant_hashes)

                    output_drops['output'].append(output_drop)
                    cycle.progress = min((100 * path_idx) // path_count, 99)
                    session.commit()

                except (AssertionError,):
                    pass
                    # this is normal and should not spam the log

                except (FaceDataError,):
                    logging.warning('drop import failed: invalid data for face load')

        cycle.output_drops = output_drops
        session.commit()

    @staticmethod
    def get_code() -> str:
        return 'import'

    @staticmethod
    def get_pipes() -> Tuple:
        return [], ['output']

    @staticmethod
    def get_records() -> Tuple:
        return ['input'], []
