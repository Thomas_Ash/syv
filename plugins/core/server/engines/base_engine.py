from typing import Dict, Tuple, Union

from sqlalchemy.orm import Session

from syv.models import Pump, Cycle
from syv.services import UserService, TankService, ArraySpecService, ArrayService
from syv.exceptions import FaceDataError
from syv.utils import get_timespan_minutes


class BaseEngine:

    def __init__(self, pump: Pump) -> None:
        self.pump = pump

    # dict of required config items
    # override but call super() at the top

    def get_config_template(self, session: Session) -> Dict:
        config_template = {
            'commit_timeout': {
                'label': 'Timeout Pre-Commit?',
                'type': 'boolean_radio',
                'options': {}
            }
        }

        commit_timeout = self.pump.engine_config.get('commit_timeout')

        if commit_timeout is True:
            config_template.update({
                'commit_timeout_duration': {
                    'label': 'Pre-Commit Timeout',
                    'type': 'timespan',
                    'options': {}
                }

            })

        config_template.update({
            'expiry_timeout': {
                'label': 'Expire Post-Complete?',
                'type': 'boolean_radio',
                'options': {}
            }
        })

        expiry_timeout = self.pump.engine_config.get('expiry_timeout')

        if expiry_timeout is True:
            config_template.update({
                'expiry_timeout_duration': {
                    'label': 'Expiry Timeout',
                    'type': 'timespan',
                    'options': {}
                }
            })

        if not self.needs_client_post():
            config_template.update({
                'schedule_run': {
                    'label': 'Run On Schedule?',
                    'type': 'boolean_radio',
                    'options': {}
                }
            })

            schedule_run = self.pump.engine_config.get('schedule_run')

            if schedule_run is True:
                config_template.update({
                    'schedule_period': {
                        'label': 'Schedule Period',
                        'type': 'timespan',
                        'options': {}
                    },
                    'schedule_offset': {
                        'label': 'Schedule Offset',
                        'type': 'timespan',
                        'options': {}
                    },
                    'schedule_user_uid': {
                        'label': 'Schedule User',
                        'type': 'user_select',
                        'options': {}
                    }
                })

        return config_template

    # validate current state
    # override but call super() at the top

    def validate(self, session: Session) -> Dict:
        config_template = self.get_config_template(session)

        report = {
            'engine_code': {'errors': []},
            'engine_config': {x: {'errors': []} for x in config_template}
        }

        missing = [x for x in config_template if x not in self.pump.engine_config]

        if len(missing) > 0:
            report['engine_code']['errors'].append(f'{len(missing)} required fields are missing from config.')

        commit_timeout = self.pump.engine_config.get('commit_timeout')

        if commit_timeout is True:
            try:
                commit_timeout_duration = get_timespan_minutes(self.pump.engine_config.get('commit_timeout_duration'))
                assert commit_timeout_duration > 0
            except (TypeError, ValueError, AssertionError, IndexError):
                report['engine_config']['commit_timeout_duration']['errors'].append(
                    'Pre-commit timeout should be a positive integer.'
                )

        expiry_timeout = self.pump.engine_config.get('expiry_timeout')

        if expiry_timeout is True:
            try:
                expiry_timeout_duration = get_timespan_minutes(self.pump.engine_config.get('expiry_timeout_duration'))
                assert expiry_timeout_duration > 0
            except (TypeError, ValueError, AssertionError, IndexError):
                report['engine_config']['expiry_timeout_duration']['errors'].append(
                    'Expiry timeout should be a positive integer.'
                )

        if not self.needs_client_post():
            schedule_run = self.pump.engine_config.get('schedule_run')
            if schedule_run not in [True, False]:
                report['engine_config']['schedule_run']['errors'].append('Run on schedule should be true or false.')
            elif schedule_run is True:
                schedule_period = None
                schedule_offset = None

                try:
                    schedule_period = get_timespan_minutes(self.pump.engine_config.get('schedule_period'))
                    assert schedule_period > 0
                except (TypeError, ValueError, AssertionError, IndexError):
                    report['engine_config']['schedule_period']['errors'].append(
                        'Schedule period should be a positive integer.'
                    )
                try:
                    schedule_offset = get_timespan_minutes(self.pump.engine_config.get('schedule_offset'))
                    assert schedule_offset >= 0
                except (TypeError, ValueError, AssertionError, IndexError):
                    report['engine_config']['schedule_offset']['errors'].append(
                        'Schedule offset should be a non-negative integer.'
                    )

                if schedule_period is not None and schedule_offset is not None and schedule_offset >= schedule_period:
                    report['engine_config']['schedule_offset']['errors'].append(
                        'Schedule offset should be less than schedule period.'
                    )

                user_service = UserService(session)

                try:
                    schedule_user_uid = int(self.pump.engine_config.get('schedule_user_uid'))
                    schedule_user = user_service.get_by_uid(schedule_user_uid)
                    assert schedule_user is not None

                    pump_role_uids = [x.uid for x in self.pump.roles]
                    common_role_uids = [x.uid for x in schedule_user.roles if x.uid in pump_role_uids]
                    if len(common_role_uids) < 1:
                        report['engine_config']['schedule_user_uid']['errors'].append(
                            'Schedule user does not share any roles with this pump.'
                        )

                except (TypeError, ValueError, AssertionError):
                    report['engine_config']['schedule_user_uid']['errors'].append(
                        'Schedule user is invalid.'
                    )

        arrayspec_service = ArraySpecService(session)
        array_service = ArrayService(session)

        for field_key in self.pump.engine_config:

            if field_key.startswith('mapping') and self.pump.engine_config[field_key] == 'constant':

                try:
                    constant_key = f'constant{field_key[7:]}'

                    if constant_key in self.pump.constant_arrays:
                        arrayspec_uid = int(self.pump.constant_arrays[constant_key].get('arrayspec_uid'))
                        arrayspec = arrayspec_service.get_by_uid(arrayspec_uid)
                        assert arrayspec is not None
                        assert arrayspec.face_code == self.pump.constant_arrays[constant_key].get('face_code')

                        face = arrayspec.get_face_type()(arrayspec, session)
                        data_array = face.load(self.pump.constant_arrays[constant_key].get('data'), 'web', session)

                        data_hash = array_service.get_data_hash(data_array)

                        assert data_hash == self.pump.constant_arrays[constant_key].get('data_hash')

                    elif constant_key in self.pump.engine_config:

                        data_hash = self.pump.engine_config[constant_key]
                        data_array = array_service.get_by_data_hash(data_hash)
                        assert data_array is not None

                    else:
                        raise KeyError


                except (KeyError, ValueError, AssertionError, TypeError, FaceDataError):
                    report['engine_config'][field_key]['errors'].append(
                        'Constant mapping value is missing or invalid.'
                    )

                # TODO: should raise error if arrayspec face does not support load in web mode (i.e. image, audio)?

        return report

    # add fields for addition to config template derived from output pipes -> tank -> arrayspecs
    # pumps with output pipes should typically call this in get_config_template

    def get_mappings(self, session: Session, choices: Union[dict, None] = None) -> Dict:

        if choices is None:
            choices = {}

        mappings = {}

        tank_service = TankService(session)

        # iterate through output pipes
        for pipe_name in self.get_pipes()[1]:

            pipe = self.pump.get_output_pipe(pipe_name)

            if pipe is None or pipe.tank_uid is None:
                continue

            tank = tank_service.get_by_uid(pipe.tank_uid)

            if tank is None:
                continue

            for arrayspec in tank.arrayspecs:
                mapping_key = f'mapping_{pipe_name}_{arrayspec.name}'

                mappings[mapping_key] = {
                    'label': f'{tank.name} / {arrayspec.name}',
                    'type': 'mapping_select',
                    'options': {
                        'choices': choices
                    }
                }

                if mapping_key in self.pump.engine_config and self.pump.engine_config[mapping_key] == 'constant':
                    mappings[f'constant_{pipe_name}_{arrayspec.name}'] = {
                        'label': f'{tank.name} / {arrayspec.name} Value',
                        'type': 'constant',
                        'options': {}
                    }

        return mappings

    # bool, true if cycle needs drops and/or config items to be posted before commit
    # override as needed

    def needs_client_post(self) -> bool:
        return False

    # config items client is required to post before commit
    # override as needed

    def get_client_config_template(self) -> Dict:
        return {}

    # true if cycle is ready to commit
    # override as needed but call super() at the top if any client config

    def can_commit(self, cycle: Cycle) -> bool:
        return not any([
            x for x in self.get_client_config_template().keys()
            if x not in cycle.client_config.keys()
        ])

    # processing method, will be called by spooler
    # override as needed

    def process(self, cycle: Cycle, session: Session) -> None:
        pass

    # return whatever pump-level config needs to be serialized with the cycle
    # override as needed

    def get_public_config(self) -> Dict:
        return {}

    # return false to fail immediately post-open if required drops are unavailable
    # override as needed

    def open_success(self, cycle: Cycle) -> bool:
        if len(self.get_pipes()[0]) == 0:
            return True

        return len(cycle.input_drops) > 0

    # unique name
    # override

    @staticmethod
    def get_code() -> str:
        return 'base'

    # 2-tuple (input, output) of arrays of strings
    # override as needed

    @staticmethod
    def get_pipes() -> Tuple:
        return [], []

    # 2-tuple (input, output) of arrays of strings
    # override as needed

    @staticmethod
    def get_records() -> Tuple:
        return [], []
