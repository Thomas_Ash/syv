import copy
from typing import Dict, Tuple

from sqlalchemy.orm import Session

from syv.models import Cycle
from syv.services import ArrayService, TankService
from .base_engine import BaseEngine


class ExtendEngine(BaseEngine):

    def get_config_template(self, session: Session) -> Dict:
        config_template = super().get_config_template(session)
        choices = {
            'ask': 'Ask',
            'constant': 'Constant'
        }

        config_template.update(self.get_mappings(session, choices=choices))

        config_template.update({
            'drops_per_page': {
                'label': 'Drops Per Page',
                'type': 'number',
                'options': {}
            }
        })

        return config_template

    def validate(self, session: Session) -> Dict:
        report = super().validate(session)

        try:
            drops_per_page = int(self.pump.engine_config.get('drops_per_page'))
            assert drops_per_page > 0
        except (TypeError, ValueError, AssertionError):
            report['engine_config']['drops_per_page']['errors'].append('Drops per page should be a positive integer.')

        tank_service = TankService(session)

        input_pipe = self.pump.get_pipe('input')
        input_tank = tank_service.get_by_uid(input_pipe.tank_uid)

        if input_pipe and input_tank:

            output_pipe = self.pump.get_pipe('output')
            output_tank = tank_service.get_by_uid(output_pipe.tank_uid)
            
            if output_pipe and output_tank:
                for output_arrayspec in output_tank.arrayspecs:
                    mapping_key = f'mapping_{output_pipe.name}_{output_arrayspec.name}'
                    mapping_value = self.pump.engine_config[mapping_key]
                    if mapping_value in ['blank', 'random', 'ask', 'constant']:
                        continue

                    try:
                        input_arrayspec_uid = int(mapping_value.split('_')[1])
                        input_arrayspec = input_tank.get_arrayspec(uid=input_arrayspec_uid)
                        assert input_arrayspec is not None
                    except (ValueError, IndexError, AssertionError):
                        report['engine_config'][mapping_key]['errors'].append('Invalid array mapping.')

            else:
                report['engine_code']['errors'].append('Output pipe is missing or invalid.')
                
        else:
            report['engine_code']['errors'].append('Input pipe is missing or invalid.')

        return report

    def needs_client_post(self) -> bool:
        for field in self.pump.engine_config:
            if field.startswith('mapping_') and self.pump.engine_config[field] == 'ask':
                return True

        return False

    def can_commit(self, cycle: Cycle) -> bool:
        if not self.needs_client_post():
            return True

        if 'output' not in cycle.client_drops:
            return False

        input_drop_uids = [x.uid for x in cycle.input_drops]

        if any([x for x in input_drop_uids if not any([
            y for y in cycle.client_drops['output'] if y.get('mapped_uid') == x
        ])]):
            return False

        ask_arrayspec_names = ['_'.join(x.split('_')[2:]) for x in self.pump.engine_config
                               if x.startswith('mapping_output_') and self.pump.engine_config.get(x) == 'ask']

        for drop in cycle.client_drops['output']:
            if 'arrays' not in drop:
                return False

            if any([x for x in ask_arrayspec_names if x not in drop['arrays']]):
                return False

        return True

    def process(self, cycle: Cycle, session: Session) -> None:
        output_pipe = self.pump.get_pipe('output')
        output_drops = copy.deepcopy(cycle.output_drops)
        drop_count = len(cycle.input_drops)

        array_service = ArrayService(session)

        for drop_idx in range(drop_count):
            input_drop = cycle.input_drops[drop_idx]
            output_drop = {}
            for output_arrayspec in output_pipe.tank.arrayspecs:
                mapping = self.pump.engine_config[f'mapping_{output_pipe.name}_{output_arrayspec.name}']
                if mapping == 'ask':
                    client_drop = next(iter(
                        [x for x in cycle.client_drops['output'] if x['mapped_uid'] == input_drop.uid]
                    ), None)
                    output_drop[output_arrayspec.name] = client_drop['arrays'][output_arrayspec.name]
                elif mapping == 'constant':
                    data_hash = self.pump.engine_config[f'constant_{output_pipe.name}_{output_arrayspec.name}']
                    output_drop[output_arrayspec.name] = data_hash
                elif mapping == 'blank':
                    face_type = output_arrayspec.get_face_type()
                    face = face_type(output_arrayspec, session)
                    data_hash = array_service.save(face.load_blank(session), cycle_uid=cycle.uid)
                    output_drop[output_arrayspec.name] = data_hash
                elif mapping == 'random':
                    face_type = output_arrayspec.get_face_type()
                    face = face_type(output_arrayspec, session)
                    data_hash = array_service.save(face.load_random(session), cycle_uid=cycle.uid)
                    output_drop[output_arrayspec.name] = data_hash
                elif mapping.startswith('arrayspec_'):
                    input_arrayspec_uid = int(mapping.split('_')[1])
                    input_arrayinst = input_drop.get_arrayinst(arrayspec_uid=input_arrayspec_uid)
                    output_drop[output_arrayspec.name] = input_arrayinst.array_data_hash

            output_drops['output'].append(output_drop)
            cycle.progress = min((100 * drop_idx) // drop_count, 99)
            session.commit()

        cycle.output_drops = output_drops
        session.commit()

    def get_public_config(self) -> Dict:
        input_pipe = self.pump.get_pipe('input')
        drop_count = input_pipe.drop_limit

        try:
            drops_per_page = int(self.pump.engine_config.get('drops_per_page'))
        except ValueError:
            drops_per_page = drop_count

        return {
            'drops_per_page': drops_per_page
        }

    @staticmethod
    def get_code() -> str:
        return 'extend'

    @staticmethod
    def get_pipes() -> Tuple:
        return ['input'], ['output']
