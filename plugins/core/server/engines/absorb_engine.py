from typing import Dict, Tuple

from sqlalchemy.orm import Session

from syv.models import Cycle
from .base_engine import BaseEngine


class AbsorbEngine(BaseEngine):

    def get_config_template(self, session: Session) -> Dict:
        config_template = super().get_config_template(session)

        config_template.update({
            'drops_per_page': {
                'label': 'Drops Per Page',
                'type': 'number',
                'options': {}
            }
        })

        return config_template

    def validate(self, session: Session) -> Dict:
        report = super().validate(session)

        try:
            drops_per_page = int(self.pump.engine_config.get('drops_per_page'))
            assert drops_per_page > 0
        except (TypeError, ValueError, AssertionError):
            report['engine_config']['drops_per_page']['errors'].append('Drops per page should be a positive integer.')

        return report

    def needs_client_post(self) -> bool:
        return False

    def can_commit(self, cycle: Cycle) -> bool:
        return True

    def process(self, cycle: Cycle, session: Session) -> None:
        pass

    def get_public_config(self) -> Dict:
        input_pipe = self.pump.get_pipe('input')
        drop_count = input_pipe.drop_limit

        try:
            drops_per_page = int(self.pump.engine_config.get('drops_per_page'))
        except ValueError:
            drops_per_page = drop_count

        return {
            'drops_per_page': drops_per_page
        }

    @staticmethod
    def get_code() -> str:
        return 'absorb'

    @staticmethod
    def get_pipes() -> Tuple:
        return ['input'], []
