from io import BytesIO
from typing import Dict, Any, Tuple, List

from sqlalchemy.orm import Session
import numpy as np
from bs4 import BeautifulSoup

from syv.models import ArraySpec
from syv.utils import any_errors
from syv.exceptions import FaceTypeError, FaceDataError
from syv.services import TomeService
from .base_face import BaseFace


class TomeFace(BaseFace):

    def __init__(self, arrayspec: ArraySpec, session: Session) -> None:
        super().__init__(arrayspec, session)

        tome_service = TomeService(session)

        try:
            tome_uid = int(arrayspec.face_config['tome_uid'])
            self.value_tome = tome_service.get_by_uid(tome_uid)
        except (Exception,):
            self.value_tome = None

        self.index_tomes = []

        for dim_idx in range(len(arrayspec.dims)):
            try:
                tome_uid = int(arrayspec.face_config[f'dim_{dim_idx}_tome_uid'])
                self.index_tomes.append(tome_service.get_by_uid(tome_uid))
            except (Exception,):
                self.index_tomes.append(None)

    def get_config_template(self, session: Session) -> Dict:
        config_template = {
            'tome_uid': {
                'label': 'Value Tome',
                'type': 'tome_select',
                'options': {
                    'required': True
                }
            }
        }

        for dim_idx in range(len(self.arrayspec.dims)):
            config_template[f'dim_{dim_idx}_tome_uid'] = {
                'label': f'Dimension {str(dim_idx + 1)} Index Tome',
                'type': 'tome_select',
                'options': {
                    'required': False
                }
            }

        return config_template

    def validate(self, session: Session) -> Dict:
        report = BaseFace.validate(self, session)

        if any_errors(report):
            return report

        if len(self.arrayspec.dims) not in [1, 2]:
            report['face_code']['errors'].append(
                f'Face does not support {len(self.arrayspec.dims)}-dimensional array spec. ' +
                'Should be 1- or 2-dimensional.'
            )

        try:
            assert self.value_tome is not None
        except(KeyError, ValueError, TypeError, AssertionError):
            report['face_config']['tome_uid']['errors'].append('No tome or invalid tome selected.')

        return report

    def load(self, data: Any, mode: str, session: Session) -> np.ndarray:
        if mode == 'web':
            return self.load_web(data, session)
        elif mode == 'text':
            return self.load_text(data, session)
        elif mode == 'file':
            return self.load_file(data, session)
        elif mode == 'html':
            return self.load_html(data, session)
        else:
            return BaseFace.load(self, data, mode, session)

    def load_web(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        integers = data.get('integers')
        if type(integers) is not list:
            raise FaceTypeError(list, type(integers))

        # assert valid row count
        if len(self.arrayspec.dims) == 1:
            if len(integers) > 1:
                raise FaceDataError(dict)
        elif (self.arrayspec.dims[1].get('is_max') and
              len(integers) > int(self.arrayspec.dims[1].get('length'))) or \
                (not self.arrayspec.dims[1].get('is_max') and
                 len(integers) != int(self.arrayspec.dims[1].get('length'))):
            raise FaceDataError(dict)

        array = None
        row_length = None
        for row in integers:
            # every list item should be a list
            if type(row) is not list:
                raise FaceTypeError(list, type(row))

            # every sublist item should be an int
            invalid_values = [x for x in row if type(x) is not int]
            if len(invalid_values) > 0:
                raise FaceTypeError(int, type(invalid_values[0]))

            # ints should all map to some tome entry (or zero for empty string)
            if len(row) > 0 and (max(row) > len(self.value_tome.entries) or min(row) < 0):
                raise FaceDataError(dict)

            if row_length is None:

                # first row, assert valid length and instantiate output array
                if (self.arrayspec.dims[0].get('is_max') and
                    len(row) > int(self.arrayspec.dims[0].get('length'))) or \
                        (not self.arrayspec.dims[0].get('is_max') and
                         len(row) != int(self.arrayspec.dims[0].get('length'))):
                    raise FaceDataError(dict)

                row_length = len(row)
                array = np.array(row).astype(self.arrayspec.dtype)[..., np.newaxis]

            # subsequent row, assert length same as first row
            elif len(row) != row_length:
                raise FaceDataError(dict)
            else:
                # subsequent row is valid, concat to output array
                array = np.concatenate((array, np.array(row).astype(self.arrayspec.dtype)[..., np.newaxis]), 1)

        if len(self.arrayspec.dims) == 1:
            return array[..., 0]
        else:
            return array

    def load_text(self, data: str, session: Session) -> np.ndarray:
        if type(data) is not str:
            raise FaceTypeError(str, type(data))

        count_limit = 1 if len(self.arrayspec.dims) == 1 else int(self.arrayspec.dims[1].get('length'))
        length_limit = int(self.arrayspec.dims[0].get('length'))

        lines = data.split('\n')

        if len(lines) > count_limit:
            lines = lines[:count_limit]
        elif len(lines) < count_limit and (len(self.arrayspec.dims) == 1 or not self.arrayspec.dims[1].get('is_max')):
            lines = lines + [''] * (count_limit - len(lines))

        rows = []

        tome_service = TomeService(session)

        for line in lines:
            row = []
            for token in line.split():
                index = self.value_tome.get_index(token)
                if index is None:
                    # append new entry to tome
                    self.value_tome = tome_service.append_entries(self.value_tome.uid, [token])
                    session.commit()
                    index = self.value_tome.get_index(token)
                    assert index is not None
                row.append(index)
            rows.append(row[:length_limit])

        length = max([len(x) for x in rows]) if self.arrayspec.dims[0].get('is_max') else length_limit

        # zero pad
        rows = [x if len(x) >= length else x + [0] * (length - len(x)) for x in rows]

        if len(self.arrayspec.dims) == 1:
            return np.array(rows[0]).astype(self.arrayspec.dtype)
        else:
            return np.array(rows).astype(self.arrayspec.dtype)

    def load_file(self, data: BytesIO, session: Session) -> np.ndarray:
        if not isinstance(data, BytesIO):
            raise FaceTypeError(BytesIO, type(data))

        return self.load_text(data.read().decode('ascii'), session)

    def load_html(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        html = data.get('html')
        origin = data.get('origin')
        if html is None or origin is None:
            raise FaceDataError(dict)

        soup = BeautifulSoup(html)
        text = '\n'.join([str(x) for x in soup.findAll(text=True)])
        return self.load_text(text, session)

    def load_blank(self, session: Session) -> np.ndarray:
        return np.zeros(self.arrayspec.get_max_shape()).astype(self.arrayspec.dtype)

    def load_random(self, session: Session) -> np.ndarray:
        maximum = min(len(self.value_tome.entries)+1, np.iinfo(self.arrayspec.dtype).max)
        return np.random.randint(1, maximum, size=self.arrayspec.get_random_shape(), dtype=self.arrayspec.dtype)

    def dump(self, array: np.ndarray, mode: str, session: Session) -> Any:
        if type(array) is not np.ndarray:
            raise FaceTypeError(np.ndarray, type(array))

        if mode == 'web':
            return self.dump_web(array, session)
        elif mode == 'text':
            return self.dump_text(array, session)
        elif mode == 'file':
            return self.dump_file(array, session)
        else:
            return BaseFace.dump(self, array, mode, session)

    def dump_web(self, array: np.ndarray, session: Session) -> Dict:
        if len(array.shape) == 1:
            # add dummy dimension to 1D array
            array = array[..., np.newaxis]

        integers = [list([int(x) for x in array[..., i]]) for i in range(array.shape[1])]

        return {
            'integers': integers,
            'value_tome_uid': self.value_tome.uid,
            'index_tome_uids': [x.uid if x is not None else None for x in self.index_tomes]
        }

    def dump_text(self, array: np.ndarray, session: Session) -> str:
        text = ''
        if len(array.shape) == 1:
            # add dummy dimension to 1D array
            array = array[..., np.newaxis]

        for i in range(array.shape[1]):
            row = list(array[..., i])
            text += ' '.join([self.value_tome.get_entry(x) for x in row]) + '\n'

        return text

    def dump_file(self, array: np.ndarray, session: Session) -> BytesIO:
        stream = BytesIO()
        stream.write(self.dump_text(array, session).encode('ascii'))
        return stream

    def get_tome_uids(self) -> List:
        return [self.value_tome.uid] + [x.uid for x in self.index_tomes if x is not None]

    def get_extension(self) -> str:
        return 'txt'

    @staticmethod
    def get_code() -> str:
        return 'tome'

    @staticmethod
    def get_dtypes() -> str:
        return 'BHIL'

    @staticmethod
    def get_modes() -> List[Tuple]:
        return [
            ('raw', True, True, True),
            ('web', True, True, True),
            ('text', False, True, True),
            ('file', False, True, True),
            ('html', False, False, True)
        ]
