from io import BytesIO
import base64
import requests
import re
from urllib import parse
from typing import Dict, Any, Tuple, List

from sqlalchemy.orm import Session
import numpy as np
from pydub import AudioSegment
from bs4 import BeautifulSoup

from syv.utils import any_errors
from syv.models import Array
from syv.exceptions import FaceTypeError, FaceConfigError, FaceDataError
from .base_face import BaseFace


class AudioFace(BaseFace):

    def get_config_template(self, session: Session) -> Dict:
        return {
            'sample': {
                'label': 'Sample Dimension',
                'type': 'dim_select',
                'options': {}
            },
            'channel': {
                'label': 'Channel Dimension',
                'type': 'dim_select',
                'options': {}
            },
            'norm_min': {
                'label': 'Normalization Minimum',
                'type': 'number',
                'options': {
                    'has_dtype_min': True
                }
            },
            'norm_max': {
                'label': 'Normalization Maximum',
                'type': 'number',
                'options': {
                    'has_dtype_max': True
                }
            },
            'sample_rate': {
                'label': 'Sample Rate (Hz)',
                'type': 'number',
                'options': {}
            },
            'format': {
                'label': 'Format',
                'type': 'custom_select',
                'options': {
                    'choices': {
                        'wav': 'WAV',
                        'mp3': 'MP3',
                        'ogg': 'OGG'
                    }
                }
            }
        }

    def validate(self, session: Session) -> Dict:
        report = BaseFace.validate(self, session)

        if any_errors(report):
            return report

        if len(self.arrayspec.dims) != 2:
            report['face_code']['errors'].append(
                f'Face does not support {len(self.arrayspec.dims)}-dimensional array spec. Should be 2-dimensional.'
            )

        for key, name in [('sample', 'Sample'),
                          ('channel', 'Channel')]:
            try:
                idx = int(self.arrayspec.face_config[key])
                assert idx >= 0
                assert idx < len(self.arrayspec.dims)
            except (KeyError, ValueError, TypeError, AssertionError):
                report['face_config'][key]['errors'].append(f'{name} dimension is invalid.')

        try:
            sample_idx = int(self.arrayspec.face_config['sample'])
            channel_idx = int(self.arrayspec.face_config['channel'])

            if sample_idx == channel_idx:
                report['face_config']['sample']['errors'].append('Dimension is already in use.')
                report['face_config']['channel']['errors'].append('Dimension is already in use.')
        except ValueError:
            pass

        for key, name in [('norm_min', 'minimum'),
                          ('norm_max', 'maximum')]:
            try:
                norm = float(self.arrayspec.face_config[key])
            except ValueError:
                report['face_config'][key]['errors'].append(f'Normalization {name} is invalid.')

        try:
            norm_min = float(self.arrayspec.face_config['norm_min'])
            norm_max = float(self.arrayspec.face_config['norm_max'])

            if norm_min >= norm_max:
                report['face_config']['norm_min']['errors'].append(
                    'Normalization minimum should be less than maximum.')
                report['face_config']['norm_max']['errors'].append(
                    'Normalization maximum should be greater than minimum.')

        except ValueError:
            pass

        try:
            sample_rate = int(self.arrayspec.face_config['sample_rate'])
            assert sample_rate > 0
        except (ValueError, TypeError, AssertionError):
            report['face_config']['sample_rate']['errors'].append('Sample rate should be a positive integer.')

        try:
            channel_dim = self.arrayspec.dims[int(self.arrayspec.face_config['channel'])]
            length = int(channel_dim['length'])
            is_max = bool(channel_dim['is_max'])

            if length not in [1, 2] or is_max:
                report['face_config']['channel']['errors'].append(
                    'Length of channel dimension should be exactly 1 or 2.')

        except (KeyError, ValueError, TypeError):
            pass

        try:
            sample_rate = int(self.arrayspec.face_config['sample_rate'])
            assert sample_rate > 0
        except (KeyError, ValueError, TypeError, AssertionError):
            report['face_config']['sample_rate']['errors'].append(
                'Sample rate should be a positive integer.')

        if self.arrayspec.face_config['format'] not in ['wav', 'mp3', 'ogg']:
            report['face_config']['format']['errors'].append(
                'Format is invalid.')

        return report

    def load(self, data: Any, mode: str, session: Session) -> np.ndarray:
        if mode == 'web':
            return self.load_web(data, session)
        elif mode == 'audio':
            return self.load_audio(data, session)
        elif mode == 'file':
            return self.load_file(data, session)
        elif mode == 'html':
            return self.load_html(data, session)
        else:
            return BaseFace.load(self, data, mode, session)

    def load_web(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        b64 = data.get('base64')

        if b64 is None:
            raise FaceDataError(dict)

        trimmed = re.match(r'^data:(.+);base64,([a-zA-Z0-9+/]+)$', b64).group(1)

        if trimmed is None:
            raise FaceDataError(dict)

        image_bytes = base64.b64decode(trimmed)
        stream = BytesIO(image_bytes)
        return self.load_file(stream, session)

    def load_audio(self, data: AudioSegment, session: Session) -> np.ndarray:
        if type(data) is not AudioSegment:
            raise FaceTypeError(AudioSegment, type(data))

        sample_rate = int(self.arrayspec.face_config['sample_rate'])
        channel_count = int(self.arrayspec.dims[int(self.arrayspec.face_config['channel'])]['length'])

        audio = data.set_frame_rate(sample_rate).set_channels(channel_count)

        array = np.array(data.get_array_of_samples()).reshape(audio.frame_count(), channel_count)

        norm_min, norm_max = self.get_norms()

        array = array.transpose(Array.reverse_axes((
            int(self.arrayspec.face_config['sample']),
            int(self.arrayspec.face_config['channel'])
        ))).astype(np.float_) * ((norm_max - norm_min) / (8 ** audio.sample_width - 1)) + norm_min

        return array

    def load_file(self, data: BytesIO, session: Session) -> np.ndarray:
        if not isinstance(data, BytesIO):
            raise FaceTypeError(BytesIO, type(data))

        try:
            audio = AudioSegment.from_file(data)
            return self.load_audio(audio, session)
        except (Exception,):
            raise FaceDataError

    def load_html(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        html = data.get('html')
        origin = data.get('origin')
        if html is None or origin is None:
            raise FaceDataError(dict)

        soup = BeautifulSoup(html, features='html5lib')
        audio_tag = soup.audio
        if audio_tag is None:
            raise FaceDataError(dict)

        source_tag = audio_tag.source
        if source_tag is None or 'src' not in source_tag.attrs:
            raise FaceDataError(dict)

        url = parse.urljoin(origin, source_tag.attrs.get('src'))
        res = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})

        with BytesIO() as stream:
            stream.write(res.content)
            return self.load_file(stream, session)

    def load_blank(self, session: Session) -> np.ndarray:
        norm_min, norm_max = self.get_norms()
        mean = (norm_min + norm_max) / 2

        return np.ones(self.arrayspec.get_max_shape(), dtype=self.arrayspec.dtype) * mean

    def load_random(self, session: Session) -> np.ndarray:
        norm_min, norm_max = self.get_norms()

        if self.arrayspec.dtype in 'bhilBHIL':
            return np.random.randint(norm_min, norm_max,
                                     self.arrayspec.get_random_shape(), dtype=self.arrayspec.dtype)
        elif self.arrayspec.dtype in 'fdg':
            return np.random.random(norm_min, norm_max,
                                    self.arrayspec.get_random_shape(), dtype=self.arrayspec.dtype)
        else:
            raise FaceConfigError

    def dump(self, array: np.ndarray, mode: str, session: Session) -> Any:
        if type(array) is not np.ndarray:
            raise FaceTypeError(np.ndarray, type(array))

        if mode == 'web':
            return self.dump_web(array, session)
        elif mode == 'audio':
            return self.dump_audio(array, session)
        elif mode == 'file':
            return self.dump_file(array, session)
        else:
            return BaseFace.dump(self, array, mode, session)

    def dump_web(self, array: np.ndarray, session: Session) -> Dict:
        stream = self.dump_file(array, session)
        if self.arrayspec.face_config['format'] == 'wav':
            mime = 'audio/wav'
        elif self.arrayspec.face_config['format'] == 'mp3':
            mime = 'audio/mpeg'
        elif self.arrayspec.face_config['format'] == 'ogg':
            mime = 'audio/ogg'
        else:
            raise FaceConfigError

        return {
            'base64': f'data:{mime};base64,{base64.b64encode(stream.read()).decode()}'
        }

    def dump_audio(self, array: np.ndarray, session: Session) -> AudioSegment:
        norm_min, norm_max = self.get_norms()

        array = (array.astype(np.float_) - norm_min) * ((2 ** 32 - 1) / (norm_max - norm_min))

        array = array.transpose(
            int(self.arrayspec.face_config['sample']),
            int(self.arrayspec.face_config['channel'])
        ).astype(np.uint32)

        sample_rate = int(self.arrayspec.face_config['sample_rate'])
        channel_count = int(self.arrayspec.dims[int(self.arrayspec.face_config['channel'])]['length'])

        return AudioSegment(array.tobytes(), sample_width=4, frame_rate=sample_rate, channels=channel_count)

    def dump_file(self, array: np.ndarray, session: Session) -> BytesIO:
        audio = self.dump_audio(array, session)
        stream = audio.export(format=self.arrayspec.face_config['format'])
        return stream

    def get_norms(self) -> Tuple:
        try:
            if self.arrayspec.dtype in 'bhilBHIL':
                norm_min = int(self.arrayspec.face_config['norm_min'])
                norm_max = int(self.arrayspec.face_config['norm_max'])
            elif self.arrayspec.dtype in 'fdg':
                norm_min = float(self.arrayspec.face_config['norm_min'])
                norm_max = float(self.arrayspec.face_config['norm_max'])
            else:
                raise FaceConfigError
        except (KeyError, ValueError, TypeError):
            raise FaceConfigError

        return norm_min, norm_max

    def get_extension(self) -> str:
        return self.arrayspec.face_config['format'].lower()

    @staticmethod
    def get_code() -> str:
        return 'audio'

    @staticmethod
    def get_dtypes() -> str:
        return 'bhilBHILfdg'

    @staticmethod
    def get_modes() -> List[Tuple]:
        return[
            ('raw', True, True, True),
            ('web', True, True, True),
            ('audio', False, True, True),
            ('file', False, True, True),
            ('html', False, False, True)
        ]
