import base64
import string
from typing import Dict, Any, Tuple, List

from sqlalchemy.orm import Session
import syvlib
import numpy as np

from syv.models import ArraySpec
from syv.config import Config
from syv.exceptions import FaceModeError, FaceTypeError, FaceConfigError, FaceDataError


class BaseFace:

    def __init__(self, arrayspec: ArraySpec, session: Session) -> None:
        self.arrayspec = arrayspec

    # dict of required config items

    def get_config_template(self, session: Session) -> Dict:
        return {}

    # return dict of errors with arrayspec dtype/dims vs. config
    # use same keys as get_config_template

    def validate(self, session: Session) -> Dict:
        config_template = self.get_config_template(session)

        report = {
            'face_code': {'errors': []},
            'face_config': {x: {'errors': []} for x in config_template}
        }

        if self.arrayspec.dtype not in self.get_dtypes():
            try:
                dtype = np.dtype(self.arrayspec.dtype)
                report['face_code']['errors'].append(
                    f'Face does not support {dtype.name} data type. ' +
                    f'Supported data types: {", ".join([np.dtype(x).name for x in self.get_dtypes()])}'
                )

            except (Exception,):
                report['face_code']['errors'].append('Data type is invalid.')

        missing = [x for x in config_template if x not in self.arrayspec.face_config]

        if len(missing) > 0:
            report['face_code']['errors'].append(f'{len(missing)} required fields are missing from config.')

        return report

    # returns np.ndarray
    # data can be anything

    def load(self, data: Any, mode: str, session: Session) -> np.ndarray:
        if mode == 'raw':
            return self.load_raw(data, session)
        else:
            raise FaceModeError(self.get_modes(), mode)

    # should not be overridden

    def load_raw(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        if 'data' not in data:
            raise FaceDataError(dict)

        return syvlib.decode(
            base64.b64decode(data.get('data')),
            Config.BYTEORDER
        )

    def load_blank(self, session: Session) -> np.ndarray:
        return np.zeros(self.arrayspec.get_max_shape()).astype(self.arrayspec.dtype)

    def load_random(self, session: Session) -> np.ndarray:
        if self.arrayspec.dtype == '?':
            return np.random.choice([True, False], self.arrayspec.get_random_shape()).astype(self.arrayspec.dtype)

        elif self.arrayspec.dtype in 'bhilBHIL':
            return np.random.randint(np.iinfo(self.arrayspec.dtype).min,
                                     np.iinfo(self.arrayspec.dtype).max,
                                     size=self.arrayspec.get_random_shape(),
                                     dtype=self.arrayspec.dtype)

        elif self.arrayspec.dtype in 'fdg':
            return np.random.random(np.finfo(self.arrayspec.dtype).min,
                                    np.finfo(self.arrayspec.dtype).max,
                                    self.arrayspec.get_random_shape(),
                                    dtype=self.arrayspec.dtype)

        elif self.arrayspec.dtype in 'FDG':
            shape = self.arrayspec.get_random_shape()
            dtype = self.arrayspec.dtype.lower()
            re = np.random.random(np.finfo(dtype).min, np.finfo(dtype).max, size=shape, dtype=dtype)
            im = np.random.random(np.finfo(dtype).min, np.finfo(dtype).max, size=shape, dtype=dtype)
            return np.array(re + im * 1j).astype(self.arrayspec.dtype)

        else:
            raise FaceConfigError

    # can return anything

    def dump(self, array: np.ndarray, mode: str, session: Session) -> Any:
        if type(array) is not np.ndarray:
            raise FaceTypeError(np.ndarray, type(array))

        if mode == 'raw':
            return self.dump_raw(array, session)
        elif mode == 'web':
            return self.dump_web(array, session)
        else:
            raise FaceModeError(self.get_modes(), mode)

    # should not be overridden

    def dump_raw(self, array: np.ndarray, session: Session) -> np.ndarray:
        return {
            'data': base64.b64encode(
                syvlib.encode(
                    array,
                    Config.BYTEORDER
                )
            ).decode()
        }

    def dump_web(self, array: np.ndarray, session: Session) -> Dict:
        # generate whatever vue component binds to
        return {}

    # return uids of any tomes which are referenced by this face

    def get_tome_uids(self) -> List:
        return []

    # returns file extension - None if file mode not supported

    def get_extension(self) -> str:
        return None

    # unique name

    @staticmethod
    def get_code() -> str:
        return 'base'

    # supported dtypes

    @staticmethod
    def get_dtypes() -> str:
        return '?bhilBHILfdgFDG'

    # implemented modes
    # str: name
    # bool: is serializable
    # bool: is dumpable
    # bool: is loadable

    @staticmethod
    def get_modes() -> List[Tuple]:
        return[
            ('raw', True, True, True),
            ('web', True, True, False)
        ]



