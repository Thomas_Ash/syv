from io import BytesIO
from typing import Dict, Any, Tuple, List

from sqlalchemy.orm import Session
import numpy as np

from syv.models import ArraySpec
from syv.utils import any_errors
from syv.exceptions import FaceTypeError, FaceDataError
from syv.services import TomeService
from .base_face import BaseFace


class BinaryFace(BaseFace):

    def __init__(self, arrayspec: ArraySpec, session: Session) -> None:
        super().__init__(arrayspec, session)

        self.index_tomes = []

        tome_service = TomeService(session)

        for dim_idx in range(len(arrayspec.dims)):
            try:
                tome_uid = int(arrayspec.face_config[f'dim_{dim_idx}_tome_uid'])
                self.index_tomes.append(tome_service.get_by_uid(tome_uid))
            except (Exception,):
                self.index_tomes.append(None)

    def get_config_template(self, session: Session) -> Dict:
        config_template = {}

        for dim_idx in range(len(self.arrayspec.dims)):
            config_template[f'dim_{dim_idx}_tome_uid'] = {
                'label': f'Dimension {str(dim_idx + 1)} Index Tome',
                'type': 'tome_select',
                'options': {
                    'required': False
                }
            }

        return config_template

    def validate(self, session):
        report = BaseFace.validate(self, session)

        if any_errors(report):
            return report

        if len(self.arrayspec.dims) not in [1, 2]:
            report['face_code']['errors'].append(
                f'Face does not support {len(self.arrayspec.dims)}-dimensional array spec. ' +
                'Should be 1- or 2-dimensional.'
            )

        return report

    def load(self, data: Any, mode: str, session: Session) -> np.ndarray:
        if mode == 'web':
            return self.load_web(data, session)
        elif mode == 'text':
            return self.load_text(data, session)
        elif mode == 'file':
            return self.load_file(data, session)
        else:
            return BaseFace.load(self, data, mode, session)

    def load_web(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        booleans = data.get('booleans')
        if type(booleans) is not list:
            raise FaceTypeError(list, type(booleans))

        # assert valid row count
        if len(self.arrayspec.dims) == 1:
            if len(booleans) > 1:
                raise FaceDataError(dict)
        elif (self.arrayspec.dims[1].get('is_max') and
              len(booleans) > int(self.arrayspec.dims[1].get('length'))) or \
                (not self.arrayspec.dims[1].get('is_max') and
                 len(booleans) != int(self.arrayspec.dims[1].get('length'))):
            raise FaceDataError(dict)

        array = None
        row_length = None
        for row in booleans:
            # every list item should be a list
            if type(row) is not list:
                raise FaceTypeError(list, type(row))

            # every sublist item should be a bool
            invalid_values = [x for x in row if type(x) is not bool]
            if len(invalid_values) > 0:
                raise FaceTypeError(int, type(invalid_values[0]))

            if row_length is None:

                # first row, assert valid length and instantiate output array
                if (self.arrayspec.dims[0].get('is_max') and
                    len(row) > int(self.arrayspec.dims[0].get('length'))) or \
                        (not self.arrayspec.dims[0].get('is_max') and
                         len(row) != int(self.arrayspec.dims[0].get('length'))):
                    raise FaceDataError(dict)

                row_length = len(row)
                array = np.array(row).astype(self.arrayspec.dtype)[..., np.newaxis]

            # subsequent row, assert length same as first row
            elif len(row) != row_length:
                raise FaceDataError(dict)
            else:
                # subsequent row is valid, concat to output array
                array = np.concatenate((array, np.array(row).astype(self.arrayspec.dtype)[..., np.newaxis]), 1)

        if len(self.arrayspec.dims) == 1:
            return array[..., 0]
        else:
            return array

    def load_text(self, data: str, session: Session) -> np.ndarray:
        if type(data) is not str:
            raise FaceTypeError(str, type(data))

        count_limit = 1 if len(self.arrayspec.dims) == 1 else int(self.arrayspec.dims[1].get('length'))
        length_limit = int(self.arrayspec.dims[0].get('length'))

        lines = data.split('\n')

        if len(lines) > count_limit:
            lines = lines[:count_limit]
        elif len(lines) < count_limit and (len(self.arrayspec.dims) == 1 or not self.arrayspec.dims[1].get('is_max')):
            raise FaceDataError(str)

        rows = []

        for line in lines:
            row = []
            for token in line.split():
                if token.lower == 'true':
                    row.append(True)
                elif token.lower == 'false':
                    row.append(False)
                else:
                    raise FaceDataError(str)
            rows.append(row[:length_limit])

        length = len(rows[0])
        if any([x for x in rows if len(x) < length]):
            raise FaceDataError(str)

        if length < length_limit and not self.arrayspec.dims[0].get('is_max'):
            raise FaceDataError(str)

        if len(self.arrayspec.dims) == 1:
            return np.array(rows[0]).astype(self.arrayspec.dtype)
        else:
            return np.array(rows).astype(self.arrayspec.dtype)

    def load_file(self, data: BytesIO, session: Session) -> np.ndarray:
        if not isinstance(data, BytesIO):
            raise FaceTypeError(BytesIO, type(data))

        return self.load_text(data.read().decode('ascii'), session)

    def load_blank(self, session: Session) -> np.ndarray:
        return np.zeros(self.arrayspec.get_max_shape()).astype(self.arrayspec.dtype)

    def load_random(self, session: Session) -> np.ndarray:
        return np.random.choice([True, False], self.arrayspec.get_random_shape()).astype(self.arrayspec.dtype)

    def dump(self, array: np.ndarray, mode: str, session: Session) -> Any:
        if type(array) is not np.ndarray:
            raise FaceTypeError(np.ndarray, type(array))

        if mode == 'web':
            return self.dump_web(array, session)
        elif mode == 'text':
            return self.dump_text(array, session)
        elif mode == 'file':
            return self.dump_file(array, session)
        else:
            return BaseFace.dump(self, array, mode, session)

    def dump_web(self, array: np.ndarray, session: Session) -> Dict:
        if len(array.shape) == 1:
            # add dummy dimension to 1D array
            array = array[..., np.newaxis]

        booleans = [list([bool(x) for x in array[..., i]]) for i in range(array.shape[1])]

        return {
            'booleans': booleans,
            'index_tome_uids': [x.uid if x is not None else None for x in self.index_tomes]
        }

    def dump_text(self, array: np.ndarray, session: Session) -> str:
        text = ''
        if len(array.shape) == 1:
            # add dummy dimension to 1D array
            array = array[..., np.newaxis]

        for i in range(array.shape[1]):
            row = array[..., i]
            text += ' '.join(['True' if x else 'False' for x in row]) + '\n'

        return text

    def dump_file(self, array: np.ndarray, session: Session) -> BytesIO:
        stream = BytesIO()
        stream.write(self.dump_text(array, session).encode('ascii'))
        return stream

    def get_tome_uids(self) -> List:
        return [x.uid for x in self.index_tomes if x is not None]

    def get_extension(self) -> str:
        return 'txt'

    @staticmethod
    def get_code() -> str:
        return 'binary'

    @staticmethod
    def get_dtypes() -> str:
        return '?'

    @staticmethod
    def get_modes() -> List[Tuple]:
        return[
            ('raw', True, True, True),
            ('web', True, True, True),
            ('text', False, True, True),
            ('file', False, True, True),
        ]
