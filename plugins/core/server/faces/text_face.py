from io import BytesIO
import random
import string
import struct
from typing import Dict, Any, Tuple, List

from sqlalchemy.orm import Session
import numpy as np
from bs4 import BeautifulSoup

from syv.config import Config
from syv.utils import any_errors
from syv.exceptions import FaceTypeError, FaceConfigError, FaceDataError
from .base_face import BaseFace


class TextFace(BaseFace):

    def get_config_template(self, session: Session) -> Dict:
        return {
            'encoding': {
                'label': 'Encoding',
                'type': 'custom_select',
                'options': {
                    'choices': {
                        'ascii': 'ASCII',
                        'utf32': 'UTF-32'
                    }
                }
            }
        }

    def validate(self, session: Session) -> Dict:
        report = BaseFace.validate(self, session)

        if any_errors(report):
            return report

        if len(self.arrayspec.dims) != 1:
            report['face_code']['errors'].append(
                f'Face does not support {len(self.arrayspec.dims)}-dimensional array spec. ' +
                'Should be 1-dimensional.'
            )

        encoding = self.arrayspec.face_config.get('encoding')

        if encoding == 'ascii':
            if self.arrayspec.dtype != 'B':
                report['face_config']['encoding']['errors'].append(
                    'Data type should be uint8 for ASCII encoding.'
                )
        elif encoding == 'utf32':
            if self.arrayspec.dtype != 'I':
                report['face_config']['encoding']['errors'].append(
                    'Data type should be uint32 for UTF-32 encoding.'
                )
        else:
            report['face_config']['encoding']['errors'].append(
                'Encoding is invalid or missing.'
            )

        return report

    def load(self, data: Any, mode: str, session: Session) -> np.ndarray:
        if mode == 'web':
            return self.load_web(data, session)
        elif mode == 'text':
            return self.load_text(data, session)
        elif mode == 'file':
            return self.load_file(data, session)
        elif mode == 'html':
            return self.load_html(data, session)
        else:
            return BaseFace.load(self, data, mode, session)

    def load_web(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        text = data.get('text')
        return self.load_text(text, session)

    def load_text(self, data: str, session: Session) -> np.ndarray:
        if type(data) is not str:
            raise FaceTypeError(str, type(data))

        encoding = self.arrayspec.face_config.get('encoding')

        if encoding == 'ascii':
            array = np.array(list(data.encode('ascii'))).astype(self.arrayspec.dtype)
        elif encoding == 'utf32':
            utf = data.encode('utf-32-be' if Config.BYTEORDER == 'big' else 'utf-32-le')
            ints = list([int.from_bytes(
                utf[idx:idx + 4],
                'big' if Config.BYTEORDER == 'big' else 'little'
            ) for idx in range(0, len(utf), 4)])
            array = np.array(ints).astype(self.arrayspec.dtype)
        else:
            raise FaceConfigError

        if not self.arrayspec.shape_fits(array.shape):
            raise FaceDataError(str)

        return array

    def load_file(self, data: BytesIO, session: Session) -> np.ndarray:
        if not isinstance(data, BytesIO):
            raise FaceTypeError(BytesIO, type(data))

        return self.load_text(data.read().decode('ascii'), session)

    def load_html(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        html = data.get('html')
        origin = data.get('origin')
        if html is None or origin is None:
            raise FaceDataError(dict)

        soup = BeautifulSoup(html, features='html5lib')
        text = '\n'.join([str(x) for x in soup.findAll(text=True)])
        return self.load_text(text, session)

    def load_blank(self, session: Session) -> np.ndarray:
        text = ' ' * int(self.arrayspec.dims[0].get('length'))

        return self.load_text(text, session)

    def load_random(self, session: Session) -> np.ndarray:
        shape = self.arrayspec.get_random_shape()
        text = ''.join(random.SystemRandom().choice(string.ascii_lowercase +
                                                    string.ascii_uppercase +
                                                    string.digits +
                                                    string.punctuation +
                                                    ' ' + '\n') for _ in range(shape[0]))

        return self.load_text(text, session)

    def dump(self, array: np.ndarray, mode: str, session: Session) -> Any:
        if type(array) is not np.ndarray:
            raise FaceTypeError(np.ndarray, type(array))

        if mode == 'web':
            return self.dump_web(array, session)
        elif mode == 'text':
            return self.dump_text(array, session)
        elif mode == 'file':
            return self.dump_file(array, session)
        else:
            return BaseFace.dump(self, array, mode, session)

    def dump_web(self, array: np.ndarray, session: Session) -> Dict:
        return {
            'text': self.dump_text(array, session)
        }

    def dump_text(self, array: np.ndarray, session: Session) -> str:

        encoding = self.arrayspec.face_config.get('encoding')

        if encoding == 'ascii':
            text = bytes(list(array)).decode('ascii')
        elif encoding == 'utf32':
            ints = tuple(list(array))
            fmt = '>' if Config.BYTEORDER == 'big' else '<'
            fmt += 'I' * len(ints)
            text = struct.pack(fmt, *ints).decode(
                'utf-32-be' if Config.BYTEORDER == 'big' else 'utf-32-le'
            )
        else:
            raise FaceConfigError

        return text

    def dump_file(self, array: np.ndarray, session: Session) -> BytesIO:
        stream = BytesIO()
        stream.write(self.dump_text(array, session).encode('ascii'))
        return stream

    def get_extension(self) -> str:
        return 'txt'

    @staticmethod
    def get_code() -> str:
        return 'text'

    @staticmethod
    def get_dtypes() -> str:
        return 'BI'

    @staticmethod
    def get_modes() -> List[Tuple]:
        return [
            ('raw', True, True, True),
            ('web', True, True, True),
            ('text', False, True, True),
            ('file', False, True, True),
            ('html', False, False, True)
        ]
