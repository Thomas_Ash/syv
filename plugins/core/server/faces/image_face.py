from io import BytesIO
import base64
import re
import requests
from urllib import parse
from typing import Dict, Any, Tuple, List

from sqlalchemy.orm import Session
import numpy as np
from PIL import Image
from bs4 import BeautifulSoup

from syv.utils import any_errors
from syv.models import Array
from syv.exceptions import FaceTypeError, FaceConfigError, FaceDataError
from .base_face import BaseFace


class ImageFace(BaseFace):

    def get_config_template(self, session: Session) -> Dict:
        return {
            'horizontal': {
                'label': 'Horizontal Dimension',
                'type': 'dim_select',
                'options': {}
            },
            'vertical': {
                'label': 'Vertical Dimension',
                'type': 'dim_select',
                'options': {}
            },
            'channel': {
                'label': 'Channel Dimension',
                'type': 'dim_select',
                'options': {}
            },
            'norm_min': {
                'label': 'Normalization Minimum',
                'type': 'number',
                'options': {
                    'has_dtype_min': True
                }
            },
            'norm_max': {
                'label': 'Normalization Maximum',
                'type': 'number',
                'options': {
                    'has_dtype_max': True
                }
            },
            'format': {
                'label': 'Format',
                'type': 'custom_select',
                'options': {
                    'choices': {
                        'JPEG': 'JPEG',
                        'PNG': 'PNG'
                    }
                }
            },
            'mode': {
                'label': 'Mode',
                'type': 'custom_select',
                'options': {
                    'choices': {
                        'RGB': 'RGB',
                        'RGBA': 'RGBA',
                        'CMYK': 'CMYK',
                        'L': 'Grayscale'
                    }
                }
            }
        }

    def validate(self, session: Session) -> Dict:
        report = BaseFace.validate(self, session)

        if any_errors(report):
            return report

        if len(self.arrayspec.dims) != 3:
            report['face_code']['errors'].append(
                f'Face does not support {len(self.arrayspec.dims)}-dimensional array spec. Should be 3-dimensional.'
            )

        for key, name in [('horizontal', 'Horizontal'),
                          ('vertical', 'Vertical'),
                          ('channel', 'Channel')]:
            try:
                idx = int(self.arrayspec.face_config[key])
                assert idx >= 0
                assert idx < len(self.arrayspec.dims)
            except (KeyError, ValueError, TypeError, AssertionError):
                report['face_config'][key]['errors'].append(f'{name} dimension is invalid.')

        clash_dims = []

        for first, second in [('horizontal', 'vertical'),
                              ('vertical', 'channel'),
                              ('channel', 'horizontal')]:
            try:
                first_idx = int(self.arrayspec.face_config[first])
                second_idx = int(self.arrayspec.face_config[second])

                if first_idx == second_idx:
                    if first not in clash_dims:
                        clash_dims.append(first)
                    if second not in clash_dims:
                        clash_dims.append(second)

            except ValueError:
                continue

        for clash_dim in clash_dims:
            report['face_config'][clash_dim]['errors'].append('Dimension is already in use.')

        for key, name in [('norm_min', 'minimum'),
                          ('norm_max', 'maximum')]:
            try:
                norm = float(self.arrayspec.face_config[key])
            except ValueError:
                report['face_config'][key]['errors'].append(f'Normalization {name} is invalid.')

        try:
            norm_min = float(self.arrayspec.face_config['norm_min'])
            norm_max = float(self.arrayspec.face_config['norm_max'])

            if norm_min >= norm_max:
                report['face_config']['norm_min']['errors'].append(
                    'Normalization minimum should be less than maximum.')
                report['face_config']['norm_max']['errors'].append(
                    'Normalization maximum should be greater than minimum.')

        except ValueError:
            pass

        if self.arrayspec.face_config['mode'] not in ['RGB', 'RGBA', 'L', 'CMYK']:
            report['face_config']['mode']['errors'].append('Mode is invalid.')
        try:
            channel_dim = self.arrayspec.dims[int(self.arrayspec.face_config['channel'])]
            length = int(channel_dim['length'])
            is_max = bool(channel_dim['is_max'])

            # this works for currently supported modes but could be more explicit?
            expected_length = len(self.arrayspec.face_config['mode'])

            if expected_length > 0 and (length != expected_length or is_max):
                report['face_config']['channel']['errors'].append(
                    f'Length of channel dimension should be exactly {expected_length}.'
                )

        except (KeyError, ValueError, TypeError, IndexError):
            pass

        if self.arrayspec.face_config['format'] not in ['JPEG', 'PNG']:
            report['face_config']['format']['errors'].append('Format is invalid.')

        if (self.arrayspec.face_config['format'] == 'JPEG' and self.arrayspec.face_config['mode'] == 'RGBA') or \
                (self.arrayspec.face_config['format'] == 'PNG' and self.arrayspec.face_config['mode'] == 'CMYK'):
            report['face_config']['format']['errors'].append(
                f'Format does not support {self.arrayspec.face_config["mode"]} mode.'
            )

        return report

    def load(self, data: Any, mode: str, session: Session) -> np.ndarray:
        if mode == 'web':
            return self.load_web(data, session)
        elif mode == 'image':
            return self.load_image(data, session)
        elif mode == 'file':
            return self.load_file(data, session)
        elif mode == 'html':
            return self.load_html(data, session)
        else:
            return BaseFace.load(self, data, mode, session)

    def load_web(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        b64 = data.get('base64')

        if b64 is None:
            raise FaceDataError(dict)

        try:
            trimmed = re.match(r'^data:(.+);base64,([a-zA-Z0-9+/]+)$', b64).group(2)
            image_bytes = base64.b64decode(trimmed)

        except (Exception,):
            raise FaceDataError(dict)

        stream = BytesIO(image_bytes)
        return self.load_file(stream, session)

    def load_image(self, data: Image.Image, session: Session) -> np.ndarray:
        if not isinstance(data, Image.Image):
            raise FaceTypeError(Image.Image, type(data))

        array = np.asarray(data.convert(self.arrayspec.face_config['mode']))

        if self.arrayspec.face_config['mode'] == 'L':
            array = array[..., np.newaxis]

        array = array.transpose(Array.reverse_axes((
            int(self.arrayspec.face_config['vertical']),
            int(self.arrayspec.face_config['horizontal']),
            int(self.arrayspec.face_config['channel'])
        )))

        norm_min, norm_max = self.get_norms()

        array = (array.astype(np.float_) * ((norm_max - norm_min) / 255.0)) + norm_min

        return array.astype(self.arrayspec.dtype)

    def load_file(self, data: BytesIO, session: Session) -> np.ndarray:
        if not isinstance(data, BytesIO):
            raise FaceTypeError(BytesIO, type(data))

        try:
            with Image.open(data) as image:
                return self.load_image(image, session)
        except (Exception,):
            raise FaceDataError(type(data))

    def load_html(self, data: Dict, session: Session) -> np.ndarray:
        if type(data) is not dict:
            raise FaceTypeError(dict, type(data))

        html = data.get('html')
        origin = data.get('origin')

        if html is None or origin is None:
            raise FaceDataError(dict)

        soup = BeautifulSoup(html, features='html5lib')

        img_tag = soup.body.find('img', recursive=False)
        a_tag = soup.body.find('a', recursive=False)

        if img_tag is not None and 'src' in img_tag.attrs:
            url = parse.urljoin(origin, img_tag.attrs.get('src'))
        elif a_tag is not None and 'href' in a_tag.attrs:
            url = parse.urljoin(origin, a_tag.attrs.get('href'))
        else:
            raise FaceDataError(dict)

        res = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})

        with BytesIO() as stream:
            stream.write(res.content)
            return self.load_file(stream, session)

    def load_blank(self, session: Session) -> np.ndarray:
        _, norm_max = self.get_norms()

        return np.ones(self.arrayspec.get_max_shape(), dtype=self.arrayspec.dtype) * norm_max

    def load_random(self, session: Session) -> np.ndarray:
        norm_min, norm_max = self.get_norms()

        if self.arrayspec.dtype in 'bhilBHIL':
            return np.random.randint(norm_min, norm_max, size=self.arrayspec.get_random_shape(),
                                     dtype=self.arrayspec.dtype)

        elif self.arrayspec.dtype in 'fdg':
            return np.random.random(norm_min, norm_max, size=self.arrayspec.get_random_shape(),
                                    dtype=self.arrayspec.dtype)

        else:
            raise FaceConfigError


    def dump(self, array: np.ndarray, mode: str, session: Session) -> Any:
        if type(array) is not np.ndarray:
            raise FaceTypeError(np.ndarray, type(array))

        if mode == 'web':
            return self.dump_web(array, session)
        elif mode == 'image':
            return self.dump_image(array, session)
        elif mode == 'file':
            return self.dump_file(array, session)
        else:
            return BaseFace.dump(self, array, mode, session)

    def dump_web(self, array: np.ndarray, session: Session) -> Dict:
        stream = self.dump_file(array, session)
        stream.seek(0)
        image_bytes = stream.getvalue()
        return {
            'base64': f'data:{Image.MIME[self.arrayspec.face_config["format"]]};' +
                      f'base64,{base64.b64encode(image_bytes).decode()}'
        }

    def dump_image(self, array: np.ndarray, session: Session) -> Image.Image:
        norm_min, norm_max = self.get_norms()

        array = (array.astype(np.float_) - norm_min) * (255.0 / (norm_max - norm_min))
        array = array.transpose((
            int(self.arrayspec.face_config['vertical']),
            int(self.arrayspec.face_config['horizontal']),
            int(self.arrayspec.face_config['channel'])
        )).astype(np.uint8)

        if self.arrayspec.face_config['mode'] == 'L':
            array = array[:, :, 0]

        return Image.fromarray(array, mode=self.arrayspec.face_config['mode'])

    def dump_file(self, array: np.ndarray, session: Session) -> BytesIO:
        image = self.dump_image(array, session)
        stream = BytesIO()
        image.save(stream, self.arrayspec.face_config['format'])

        return stream

    def get_norms(self) -> Tuple:
        try:
            if self.arrayspec.dtype in 'bhilBHIL':
                norm_min = int(self.arrayspec.face_config['norm_min'])
                norm_max = int(self.arrayspec.face_config['norm_max'])
            elif self.arrayspec.dtype in 'fdg':
                norm_min = float(self.arrayspec.face_config['norm_min'])
                norm_max = float(self.arrayspec.face_config['norm_max'])
            else:
                raise FaceConfigError
        except (KeyError, ValueError, TypeError):
            raise FaceConfigError

        return norm_min, norm_max

    def get_extension(self) -> str:
        return self.arrayspec.face_config['format'].lower()

    @staticmethod
    def get_code() -> str:
        return 'image'

    @staticmethod
    def get_dtypes() -> str:
        return 'bhilBHILfdg'

    @staticmethod
    def get_modes() -> List[Tuple]:
        return[
            ('raw', True, True, True),
            ('web', True, True, True),
            ('image', False, True, True),
            ('file', False, True, True),
            ('html', False, False, True)
        ]
