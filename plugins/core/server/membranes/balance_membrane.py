from typing import Tuple, List, Dict

from sqlalchemy import and_
from sqlalchemy.orm import Session, Query, aliased

from syv.services import TankService, DropService
from syv.models import Drop, ArrayInst
from .base_membrane import BaseMembrane


class BalanceMembrane(BaseMembrane):

    def get_config_template(self, session: Session) -> Dict:
        tank_service = TankService(session)
        tanks, count = tank_service.page()

        config_template = {
            'hash_limit': {
                'label': 'Hash Limit',
                'type': 'number',
                'options': {}
            }
        }

        local_tank = tank_service.get_by_uid(self.pipe.tank_uid)

        if local_tank is not None:
            config_template['local_arrayspec_uid'] = {
                'label': 'Local Array Spec',
                'type': 'custom_select',
                'options': {
                    'choices': {
                        x.uid: x.name for x in local_tank.arrayspecs
                    }
                }
            }

        config_template['remote_tank_uid'] = {
            'label': 'Remote Tank',
            'type': 'custom_select',
            'options': {
                'choices': {
                    x.uid: x.name for x in tanks
                }
            }
        }

        try:
            remote_tank_uid = int(self.filter.membrane_config.get('remote_tank_uid'))
            remote_tank = tank_service.get_by_uid(remote_tank_uid)
            assert remote_tank is not None

            config_template['remote_arrayspec_uid'] = {
                'label': 'Remote Array Spec',
                'type': 'custom_select',
                'options': {
                    'choices': {
                        x.uid: x.name for x in remote_tank.arrayspecs
                    }
                }
            }

        except(TypeError, ValueError, AssertionError):
            pass


        return config_template

    def validate(self, session: Session) -> Dict:
        report = super().validate(session)

        try:
            hash_limit = int(self.filter.membrane_config.get('hash_limit'))
            assert hash_limit >= 0
        except(TypeError, ValueError, AssertionError):
            report['membrane_config']['hash_limit']['errors'].append(
                'Hash limit should be a non-negative integer.'
            )

        tank_service = TankService(session)

        try:
            local_tank = tank_service.get_by_uid(self.pipe.tank_uid)
            local_arrayspec_uid = int(self.filter.membrane_config.get('local_arrayspec_uid'))
            local_arrayspec = local_tank.get_arrayspec(uid=local_arrayspec_uid)
            assert local_arrayspec is not None
        except(TypeError, ValueError, AssertionError):
            report['membrane_config']['local_arrayspec_uid']['errors'].append(
                'Local array spec is missing or invalid.'
            )

        tank_service = TankService(session)
        remote_tank = None

        try:
            remote_tank_uid = int(self.filter.membrane_config.get('remote_tank_uid'))
            remote_tank = tank_service.get_by_uid(remote_tank_uid)
            assert remote_tank is not None
        except(TypeError, ValueError, AssertionError):
            report['membrane_config']['remote_tank_uid']['errors'].append(
                'Remote tank is missing or invalid.'
            )

        if remote_tank is not None:
            try:
                remote_arrayspec_uid = int(self.filter.membrane_config.get('remote_arrayspec_uid'))
                remote_arrayspec = remote_tank.get_arrayspec(uid=remote_arrayspec_uid)
                assert remote_arrayspec is not None
            except(TypeError, ValueError, AssertionError):
                report['membrane_config']['remote_arrayspec_uid']['errors'].append(
                    'Remote array spec is missing or invalid.'
                )

        return report

    def process_input_query(self, query: Query, session: Session) -> Query:

        hash_limit = int(self.filter.membrane_config.get('hash_limit'))
        local_arrayspec_uid = int(self.filter.membrane_config.get('local_arrayspec_uid'))
        remote_arrayspec_uid = int(self.filter.membrane_config.get('remote_arrayspec_uid'))

        drop_service = DropService(session)
        stats = drop_service.count_by_arrayspec_and_hash(remote_arrayspec_uid)

        blacklist = [x[0] for x in stats if x[1] >= hash_limit]
        arrayinst_alias = aliased(ArrayInst)

        return query.join(
            arrayinst_alias, and_(
                arrayinst_alias.drop_uid == Drop.uid,
                arrayinst_alias.arrayspec_uid == local_arrayspec_uid
            )
        ).filter(
            arrayinst_alias.array_data_hash.not_in(blacklist)
        )

    def process_input_drops(self, drops: List[Drop], session: Session) -> List[Drop]:

        hash_limit = int(self.filter.membrane_config.get('hash_limit'))
        local_arrayspec_uid = int(self.filter.membrane_config.get('local_arrayspec_uid'))
        remote_arrayspec_uid = int(self.filter.membrane_config.get('remote_arrayspec_uid'))

        drop_service = DropService(session)
        stats = drop_service.count_by_arrayspec_and_hash(remote_arrayspec_uid)
        count_by_hash = {x[0]: x[1] for x in stats}
        filtered_drops = []

        for drop in drops:
            arrayinst = drop.get_arrayinst(arrayspec_uid=local_arrayspec_uid)

            if arrayinst.array_data_hash in count_by_hash:
                if count_by_hash[arrayinst.array_data_hash] < hash_limit:
                    filtered_drops.append(drop)
                    count_by_hash[arrayinst.array_data_hash] += 1

            else:
                filtered_drops.append(drop)
                count_by_hash[arrayinst.array_data_hash] = 1

        return filtered_drops

    def process_output_drops(self, drops: List[Dict], session: Session) -> List[Dict]:
        hash_limit = int(self.filter.membrane_config.get('hash_limit'))
        local_arrayspec_uid = int(self.filter.membrane_config.get('local_arrayspec_uid'))
        local_arrayspec_name = self.pipe.tank.get_arrayspec(uid=local_arrayspec_uid).name
        remote_arrayspec_uid = int(self.filter.membrane_config.get('remote_arrayspec_uid'))

        drop_service = DropService(session)
        stats = drop_service.count_by_arrayspec_and_hash(remote_arrayspec_uid)
        count_by_hash = {x[0]: x[1] for x in stats}
        filtered_drops = []

        for drop in drops:
            data_hash = drop.get(local_arrayspec_name)

            if data_hash in count_by_hash:
                if count_by_hash[data_hash] < hash_limit:
                    filtered_drops.append(drop)
                    count_by_hash[data_hash] += 1

            else:
                filtered_drops.append(drop)
                count_by_hash[data_hash] = 1

        return filtered_drops

    @staticmethod
    def get_code() -> str:
        return 'balance'

    @staticmethod
    def get_directionality() -> Tuple[bool, bool]:
        return True, True
