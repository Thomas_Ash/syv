from typing import Tuple, List, Dict

from sqlalchemy.orm import Session, Query

from syv.models import Filter, Pipe, Drop


class BaseMembrane:

    def __init__(self, _filter: Filter, pipe: Pipe) -> None:
        self.filter = _filter
        self.pipe = pipe

    # dict of required config items
    # override

    def get_config_template(self, session: Session) -> Dict:
        return {}

    # validate current state
    # override but call super() at the top

    def validate(self, session: Session) -> Dict:
        config_template = self.get_config_template(session)

        report = {
            'membrane_code': {'errors': []},
            'membrane_config': {x: {'errors': []} for x in config_template}
        }

        if self.pipe.is_input:
            if not self.get_directionality()[0]:
                report['membrane_code']['errors'].append('Membrane type is not compatible with input pipe.')
        else:
            if not self.get_directionality()[1]:
                report['membrane_code']['errors'].append('Membrane type is not compatible with output pipe.')

        return report

    # add .filter(...) clauses to a SQLAlchemy query
    # override as needed

    def process_input_query(self, query: Query, session: Session) -> Query:
        return query

    # return subset of a list of Drop models
    # override as needed

    def process_input_drops(self, drops: List[Drop], session: Session) -> List[Drop]:
        return drops

    # return subset of a list of array hash dictionaries
    # override as needed

    def process_output_drops(self, drops: List[Dict], session: Session) -> List[Dict]:
        return drops

    # unique name
    # override

    @staticmethod
    def get_code() -> str:
        return 'base'

    # applicability to input (0) and output (1) pipes
    # override

    @staticmethod
    def get_directionality() -> Tuple[bool, bool]:
        return True, True
