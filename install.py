# imports

import sys
import json
import shutil
import yaml
from pathlib import Path
from io import StringIO

# init empty plugin dict and plugin root path

plugins = {}
plugins_root = Path('./plugins')

# get glob pattern

if len(sys.argv) < 2:
  glob = '*'
else:
  glob = sys.argv[1]

# iterate plugin paths and build plugin dict from dir names & manifests

for plugin_path in plugins_root.glob(glob):
  plugin_name = plugin_path.parts[-1]
  print(f'Scanning plugin "{plugin_name}"...')
  manifest_path = Path(plugin_path, 'manifest.json')
  with open(manifest_path, 'r') as manifest_file:
    manifest = json.load(manifest_file)
  plugins[plugin_name] = manifest

# init py and js root paths

py_root = Path('./server')
py_module_root = Path(py_root, 'syv')

js_root = Path('./client/syv')
js_module_root = Path(js_root, 'src/components')

# init buffers for code generation

py_engine_import = StringIO()
py_engine_index = StringIO()
py_face_import = StringIO()
py_face_index = StringIO()
py_membrane_import = StringIO()
py_membrane_index = StringIO()

js_engine_import = StringIO()
js_engine_index = StringIO()
js_face_import = StringIO()
js_face_index = StringIO()

# read minimal requirements.txt into buffer

py_packages = StringIO()
py_syv_packages_path = Path(py_root, 'syv.requirements.txt')
with open(py_syv_packages_path, 'r') as py_syv_packages_file:
  py_packages.write(py_syv_packages_file.read())

# read minimal package.json into dict

js_syv_packages_path = Path(js_root, 'syv.package.json')
with open(js_syv_packages_path, 'r') as js_syv_packages_file:
  js_packages = json.load(js_syv_packages_file)

# read minimal docker-compose.yml into dict

dr_syv_compose_path = Path('./syv.docker-compose.yml')
with open(dr_syv_compose_path, 'r') as dr_syv_compose_file:
  dr_compose = yaml.load(dr_syv_compose_file, Loader=yaml.CLoader)

# iterate plugin dict

for plugin_name in plugins:

  # iterate python packages and write to buffer

  for package_name in plugins[plugin_name]['py_packages']:
    package_version = plugins[plugin_name]['py_packages'][package_name]
    py_packages.write(f'{package_name}{package_version}\n')

  # iterate javascript packages and write to dict

  for package_name in plugins[plugin_name]['js_packages']:
    package_version = plugins[plugin_name]['js_packages'][package_name]
    js_packages['dependencies'][package_name] = package_version

  # iterate docker services and write to dict

  for service_name in plugins[plugin_name]['dr_services']:
    service_config = plugins[plugin_name]['dr_services'][service_name]
    dr_compose['services'][service_name] = service_config

    # add to depends_on of uwsgi service

    dr_compose['services']['uwsgi']['depends_on'].append(service_name)

  # iterate engines

  for engine in plugins[plugin_name]['engines']:

    # get values from manifest

    code = engine['code']
    print(f'Installing engine "{code}" from plugin "{plugin_name}"...')
    py_module = engine['py_module']
    py_class = engine['py_class']
    js_module = engine['js_module']

    # copy python module file

    py_filename = py_module + '.py'
    src_path = Path(plugins_root, plugin_name, 'server', 'engines', py_filename)
    dst_path = Path(py_module_root, 'engines', py_filename)
    shutil.copyfile(src_path, dst_path)

    # copy javascript module file

    js_filename = js_module + '.vue'
    src_path = Path(plugins_root, plugin_name, 'client', 'engines', js_filename)
    dst_path = Path(js_module_root, 'engines', js_filename)
    shutil.copyfile(src_path, dst_path)

    # generate python code

    py_engine_import.write(f'from .{py_module} import {py_class}\n')
    py_engine_index.write(f'        {py_class},\n')

    # generate javascript code

    js_engine_import.write(f'import {js_module} from \'@/components/engines/{js_module}\'\n')
    js_engine_index.write(f'  {code}: {js_module},\n')

  # iterate faces

  for face in plugins[plugin_name]['faces']:

    # get values from manifest

    code = face['code']
    print(f'Installing face "{code}" from plugin "{plugin_name}"...')
    py_module = face['py_module']
    py_class = face['py_class']
    js_module = face['js_module']

    # copy python module file

    py_filename = py_module + '.py'
    src_path = Path(plugins_root, plugin_name, 'server', 'faces', py_filename)
    dst_path = Path(py_module_root, 'faces', py_filename)
    shutil.copyfile(src_path, dst_path)

    # copy javascript module file

    js_filename = js_module + '.vue'
    src_path = Path(plugins_root, plugin_name, 'client', 'faces', js_filename)
    dst_path = Path(js_module_root, 'faces', js_filename)
    shutil.copyfile(src_path, dst_path)

    # generate python code

    py_face_import.write(f'from .{py_module} import {py_class}\n')
    py_face_index.write(f'        {py_class},\n')

    # generate javascript code

    js_face_import.write(f'import {js_module} from \'@/components/faces/{js_module}\'\n')
    js_face_index.write(f'  {code}: {js_module},\n')

  # iterate membranes

  for membrane in plugins[plugin_name]['membranes']:

    # get values from manifest

    code = membrane['code']
    print(f'Installing membrane "{code}" from plugin "{plugin_name}"...')
    py_module = membrane['py_module']
    py_class = membrane['py_class']

    # copy python module file

    py_filename = py_module + '.py'
    src_path = Path(plugins_root, plugin_name, 'server', 'membranes', py_filename)
    dst_path = Path(py_module_root, 'membranes', py_filename)
    shutil.copyfile(src_path, dst_path)

    # generate python code

    py_membrane_import.write(f'from .{py_module} import {py_class}\n')
    py_membrane_index.write(f'        {py_class},\n')

# combine engine python code buffers and write to filesystem

py_engine_path = Path(py_module_root, 'engines', '__init__.py')
print(f'Generating "{py_engine_path}"...')
py_engine_import.seek(0)
py_engine_index.seek(0)

with open(py_engine_path, 'w') as py_engine_file:
  py_engine_file.write('from typing import List, Type\n\n')
  py_engine_file.write(py_engine_import.read())
  py_engine_file.write('\n\ndef get_engine_types() -> List[Type[BaseEngine]]:\n    return [\n')
  py_engine_file.write(py_engine_index.read())
  py_engine_file.write('    ]\n')

# combine engine javascript code buffers and write to filesystem

js_engine_path = Path(js_module_root, 'engines', 'index.js')
print(f'Generating "{js_engine_path}"...')
js_engine_import.seek(0)
js_engine_index.seek(0)

with open(js_engine_path, 'w') as js_engine_file:
  js_engine_file.write(js_engine_import.read())
  js_engine_file.write('\nconst engines = {\n')
  js_engine_file.write(js_engine_index.read())
  js_engine_file.write('}\n\nexport function getEngine(engine_code) {\n')
  js_engine_file.write('  if(engines[engine_code]) {\n')
  js_engine_file.write('    return engines[engine_code]\n  }\n\n')
  js_engine_file.write('  return BaseEngine\n}\n')


# combine face python code buffers and write to filesystem

py_face_path = Path(py_module_root, 'faces', '__init__.py')
print(f'Generating "{py_face_path}"...')
py_face_import.seek(0)
py_face_index.seek(0)

with open(py_face_path, 'w') as py_face_file:
  py_face_file.write('from typing import List, Type\n\n')
  py_face_file.write(py_face_import.read())
  py_face_file.write('\n\ndef get_face_types() -> List[Type[BaseFace]]:\n    return [\n')
  py_face_file.write(py_face_index.read())
  py_face_file.write('    ]\n')

# combine face javascript code buffers and write to filesystem

js_face_path = Path(js_module_root, 'faces', 'index.js')
print(f'Generating "{js_face_path}"...')
js_face_import.seek(0)
js_face_index.seek(0)

with open(js_face_path, 'w') as js_face_file:
  js_face_file.write(js_face_import.read())
  js_face_file.write('\nconst faces = {\n')
  js_face_file.write(js_face_index.read())
  js_face_file.write('}\n\nexport function getFace(face_code) {\n')
  js_face_file.write('  if(faces[face_code]) {\n')
  js_face_file.write('    return faces[face_code]\n  }\n\n')
  js_face_file.write('  return BaseFace\n}\n')

# combine membrane python code buffers and write to filesystem

py_membrane_path = Path(py_module_root, 'membranes', '__init__.py')
print(f'Generating "{py_membrane_path}"...')
py_membrane_import.seek(0)
py_membrane_index.seek(0)

with open(py_membrane_path, 'w') as py_membrane_file:
  py_membrane_file.write('from typing import List, Type\n\n')
  py_membrane_file.write(py_membrane_import.read())
  py_membrane_file.write('\n\ndef get_membrane_types() -> List[Type[BaseMembrane]]:\n    return [\n')
  py_membrane_file.write(py_membrane_index.read())
  py_membrane_file.write('    ]\n')

# write python packages to filesystem

py_packages_path = Path(py_root, 'requirements.txt')
print(f'Generating "{py_packages_path}"...')
py_packages.seek(0)

with open(py_packages_path, 'w') as py_packages_file:
  py_packages_file.write(py_packages.read())

# write javascript packages to filesystem

js_packages_path = Path(js_root, 'package.json')
print(f'Generating "{js_packages_path}"...')

with open(js_packages_path, 'w') as js_packages_file:
  json.dump(js_packages, js_packages_file, indent=2, sort_keys=True)

# write docker compose to filesystem

dr_compose_path = Path('./docker-compose.yml')
print(f'Generating "{dr_compose_path}"...')

with open(dr_compose_path, 'w') as dr_compose_file:
  dr_compose_file.write(yaml.dump(dr_compose, Dumper=yaml.CDumper))

print('Done!')
